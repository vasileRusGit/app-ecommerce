<!DOCTYPE html>

<html style="overflow-x: hidden">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <meta name="token" id="token" value="{{ Auth::check() ? true : false }}">
    <meta name="viewport" content="width=device-width">
    <title>Online Ecommerce Store</title>
    <link rel="stylesheet" href="/css/app.css">
    <link rel="shortcut icon" href="{{ asset('images/ecommerce-favicon.png') }}">
    <link rel="stylesheet" href="/css/sweetalert.css">
    <link rel="stylesheet" href="/css/dropzone.css">
    <link rel="stylesheet" href="/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="/css/gridList.css">
    <link rel="stylesheet" href="/css/shopping-cart.css">
    <link rel="stylesheet" href="/css/tooltip_balloons.css">
    <link rel="stylesheet" href="/css/select2.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="/css/smooth.css">

    <!-- CSS -->
    <link rel="stylesheet" href="/css/swin-zoom/zoom.css">

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.dataTables.min.css">

    
    <!-- Scripts -->
      <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token()
        ]); ?>;
    </script>
    </head>

    <body>
        <div id="app-vue-chat">
            @include('pages.navigation')

            @include('cart.addToCart')

            @include('pages.search-form-icon')

            <div class="sliderPosition">
                @yield('sliderPosition')
            </div>

            <div class="container-fluid">
                @yield('content')
            </div>

            @include('pages.footer')
        </div>

        <script src="/js/jquery.js"></script>
        <script src="/js/jquery.easing.js"></script>
        <script src="/js/app.js"></script>
        @yield('pagescriptModal')
        <script src="/js/custom/productID.js"></script>
        <script src="/js/custom/rating_reviews.js"></script>
        <script src="/js/custom/showHideHeaderOnScroll.js"></script>
        <script src="/js/custom/showIndividualFlyer.js"></script>
        <script src="/js/custom/featuredFlyers.js"></script>
        <script src="/js/select2.js"></script>
        <script src="/js/custom/filterFlyers.js"></script>
        <script src="/js/sweetalert.js"></script>
        <script src="/js/dropzone.js"></script>
        <script src="/js/custom/custom.js"></script>
        <script src="/js/custom/custom-navigation.js"></script>
        <script src="/js/jquery.cookie.js"></script>
        <script src="/js/lodash.js"></script>
        <script src="/js/custom/config.js"></script>
        <script src="/js/custom/favorites.js"></script>
        <script src="/js/custom/gridList.js"></script>
        <script src="/js/custom/addToCart.js"></script>
        <script src="/js/jquery.flexslider.js"></script>
        <script src="/js/carousel.js"></script>
        <script src="/js/scrollDown.js"></script>
        <script src="/js/custom/checkoutPayment.js"></script>
        <script src="/js/sha256.js"></script>
        <script src="/js//custom/productQtyProductPage.js"></script>
        

        {{-- datatable --}}
        <script src="/js/jquery.dataTables.min.js"></script>
        <script src="/js/dataTables.bootstrap4.min.js"></script>


        <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap4.min.js"></script>
        <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>


        <script src="/js/custom.js"></script>
        {{-- cart --}}
        <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

        <!-- INDIVIDUAL PRODUCT SLIDER -->
        <link rel='stylesheet' id='zoom-css'  href='/css/swin-zoom/zoom.css' type='text/css' media='all' />
        <script type='text/javascript' src='/js/swin-zoom/jquery.nicescroll.min.js'></script>
        <script type='text/javascript' src='/js/swin-zoom/jquery.slider.js'></script>
        <script type='text/javascript' src='/js/swin-zoom/jquery.mousewheel.js'></script>
        <script type='text/javascript' src='/js/swin-zoom/touch.js'></script>
        <script type='text/javascript' src='/js/swin-zoom/zoom.js'></script>
        <script src="/js/smooth.js"></script>


        <script>
            Dropzone.options.upload = {
                url: "/flyers/create/photos",
                paramName: "file",
                maxFilesize: 20,
                autoProccessQueue: false,
                uploadMultiple: true,
                addRemoveLinks: true,
                parallelUploads: 10,
                accept: function (file, done) {
                }
            };

            $("#searchFormFrom").select2({
                placeholder: "Select an option",
                allowClear: true,
            });
            $("#searchFormTo").select2({
                placeholder: {
                    text: "Select an option"
                },
                allowClear: true
            });
            $('.js-example-basic-single').select2({

            });

            // SIMILAR FLYERS SLIDER
            $('#Carousel').carousel({
                interval: 5000
            });


            // Product preview sliderPosition
            $('#carousel').flexslider({
                animation: "slide",
                controlNav: false,
                animationLoop: false,
                slideshowSpeed: 4000,
                itemWidth: 210,
                itemMargin: 5,
                asNavFor: '#slider'
            });
            $('.flexslider').flexslider({
                animation: "slide",
                controlNav: false,
                animationLoop: true,
                slideshowSpeed: 4000,
                sync: "#carousel"
            });


            $('.sp-wrap').smoothproducts();

            jQuery('.views-gallery a').click(function(e) {
                e.preventDefault();

                var $this = jQuery(this),
                largeImage  = $this.attr('href');
                console.log(largeImage);
                smallImage  = $this.data('easyzoom-source');

                if (!$this.parent().hasClass('thumbnail-active')) {
                    jQuery('a#zoom1').swinxyzoom('load', smallImage,  largeImage);
                    jQuery('.lightbox-btn').attr('href', largeImage);

                    jQuery('.views-gallery .slide.thumbnail-active').removeClass('thumbnail-active');
                    $this.parent().toggleClass('thumbnail-active');
                }

            });

            // dataTables.bootstrap4.min.css
            $('#datatableShowInvoices').DataTable({
                rowReorder: {
                    selector: 'td:nth-child(2)'
                },
                responsive: true
            } );


        </script>

        @include('flash')


    </body>
    </html>