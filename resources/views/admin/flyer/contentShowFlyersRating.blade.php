<section class="content-header">
    <h1>Show flyers rating</h1>
    <ol class="breadcrumb">
        <li><a href="/admin/flyers/show"><i class="fa fa-folder-open"></i> Flyers</a></li>
    </ol>
</section>

<!-- Main content -->
<section class="datatableRow panelAdminD">
    <table id="datatableShowFlyers" class="table table-bordered table-hover" width="100%">
        <thead>
            <tr>
                <th class="visible-desktop" style="width:20px;">#</th>
                <th class="visible-desktop" style="width:70px;">Product ID</th>
                <th >Flyer Id</th>
                <th class="visible-desktop">User Name</th>
                <th class="visible-desktop">Created_at</th>
                <th>Rating</th>
                <th style="width:120px">Action</th>
            </tr>
        </thead>

        <tbody>
            @foreach($ratings as $rate)
            <tr>
                <form action="/admin/flyers/rating" id="updateFlyersRatingForm" method="get">
                    {{csrf_field()}}
                    <td>{{$rate->id}}</td>
                    <td>{{$rate->productID}}</td>
                    <td>{{$rate->flyer_id}}</td>
                    <td>{{$rate->name}}</td>
                    <td>{{$rate->created_at}}</td>
                    <td>
                        <select id="adminRatedFlyer" name="adminRatedFlyer" class="form-control">
                            @for($i = 0; $i<=5; $i++)
                            @if($rate->rating == $i)
                            <option selected value="{{$rate->rating}}">{{$rate->rating}}</option>
                            @else
                            <option value="{{$i}}">{{$i}}</option>
                            @endif
                            @endfor
                        </select>
                    </td>
                    <td>
                        <input type="hidden" name="id" value="{{$rate->id}}" />
                        <button type="submit" id="updateFlyersRatingButton" class="btn btn-warning">Update</button>
                    </form>
                    {{ Form::open(['method' => 'DELETE','id' => 'submitDeleteFlyerRatingForm', 'class' => 'adminDeleteFlyerRatingFormOpen','route' => ['admin.flyer.rating.destroy', $rate->id]]) }}
                    <button type="submit" class="btn btn-danger">Delete</button>
                    {{ Form::close() }}
                </td>
                
            </tr>
            @endforeach
        </tbody>
    </table>
</section>