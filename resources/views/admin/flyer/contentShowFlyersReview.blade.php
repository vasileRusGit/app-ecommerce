<section class="content-header">
    <h1>Show flyers reviews</h1>
    <ol class="breadcrumb">
        <li><a href="/admin/flyers/show"><i class="fa fa-folder-open"></i> Flyers</a></li>
    </ol>
</section>

<!-- Main content -->
<section class="datatableRow panelAdminD">
    <table id="datatableShowFlyers" class="table table-bordered table-hover" width="100%">
        <thead>
            <tr>
                <th style="width:20px;">#</th>
                <th class="visible-desktop">Product ID</th>
                <th >Flyer ID</th>
                <th class="visible-desktop">Flyer Name</th>
                <th class="visible-desktop">User Name</th>
                <th style="width:20px">Action</th>
            </tr>
        </thead>

        <tbody>
            <?php $count = 1; ?>
            @foreach($reviews as $review)
            <tr>
                {{csrf_field()}}
                <td>{{$review->id}}</td>
                <td>{{$review->productID}}</td>
                <td>{{$review->flyer_id}}</td>
                <td>{{$review->street}}</td>
                <td>{{$review->name}}</td>
                
                <td>
                    <div class="position-edit-delete-icons">
                        &nbsp<a href="/admin/review/{{$review->id}}/edit"><i class="ion-edit"></i></a>&nbsp&nbsp&nbsp&nbsp&nbsp|
                        <a>
                            {{ Form::open(['method' => 'DELETE','id' => 'submitDeleteForm', 'class' => 'adminDeleteFlyerFormOpen', 'route' => ['admin.review.destroy', $review->id]]) }}
                            {{ Form::button('<i class="ion-ios-trash"></i>',['onclick' => 'deleteFlyer()','type' => 'submit','class' => 'adminDeleteFlyerButton', 'id' => 'adminDeleteFlyer'])}}
                            {{ Form::close() }}</a>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </section>