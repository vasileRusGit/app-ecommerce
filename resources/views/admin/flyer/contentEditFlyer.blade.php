@inject('countries' , 'App\Http\Utilities\Country')

<section class="content-header">
    <h1>Edit flyer</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Flyers</a></li>
    </ol>
</section>

<!-- Main content -->
<section class="datatableRow">

    @if(count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
            <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
    @endif


     <div class="row editAdminFlyer">
        <div class="col-md-4 flyerPhotos panelAdminFFlyer" style="    margin-right: 5%;">
            @foreach($flyer->photos->chunk(4) as $set)
            <div class="row">
                @foreach($set as $photo)

                <div class="col-md-3 gallery_images">
                    <form style="width:200%!important" method="POST" action="/photos/{{$photo->id}}">
                        {{csrf_field()}}
                        <input type="hidden" name="_method" value="DELETE">
                        <button type="submit" id="removePictureButton"><i class="ion-close-round"></i></button>
                    </form>
                    <a href="/{{$photo->path}}" data-lightbox="image-1">
                        <img src="/{{$photo->path}}" alt="">
                    </a>
                </div>
                @endforeach
            </div>
            @endforeach
        </div>

        {{ Form::open(['method' => 'PUT', 'id' => 'adminEditFlyerForm', 'route' => ['admin.flyer.update', $flyer->id]]) }}

        {{csrf_field()}}

        <div class="col-md-6 panelAdminFFlyer" style="    margin-left: 15px;">
            <div class="class-form streetForm">
                <label for="street">Street:</label>
                <input type="text" name="street" id="street" class="form-control" value="{{$flyer->street}}"/>
            </div>

            <div class="class-form cityForm">
                <label for="city">City:</label>
                <input type="text" name="city" id="city" class="form-control" value="{{$flyer->city}}"/>
            </div>

            <div class="class-form zipForm">
                <label for="zip">Zip/Postal Code:</label>
                <input type="text" name="zip" id="zip" class="form-control" value="{{$flyer->zip}}"/>
            </div>

            <div class="class-form">
                <label for="country">Country:</label>
                <select id="country" name="country" class="form-control">
                    @foreach($countries::all() as $code => $country)
                    <option value="{{$code}}">{{$country}}</option>
                    @endforeach
                </select>
            </div>

            <div class="class-form stateForm">
                <label for="state">State:</label>
                <input type="text" name="state" id="state" class="form-control" value="{{$flyer->state}}"/>
            </div>

            <div class="class-form priceForm">
                <label for="price">Sale Price:</label>
                <input type="text" name="price" id="price" class="form-control"
                value="{{str_replace(',', '', str_replace('$', '',$flyer->price))}}"/>
            </div>

            <div class="class-form">
                <label for="productID">Product ID:</label>
                <input type="text" name="productID" id="productID" class="form-control" value="{{str_replace(',', '', str_replace('$', '',$flyer->productID))}}" readonly/>
            </div>


        <div class="col-md-8" style="margin-left: -15px;    padding-right: 0;">
            <div class="class-form" style="padding-top: 10px;">
                <label for="featured">Set Flyer As Featured :</label>
                <input type="checkbox" id="featured" {{$flyer->featured == 'on' ? 'checked' : ''}} name="featured"/><label id="featuredCheckbox" for="featured"></label>
            </div>
            <div class="class-form" style="    width: 154%;">
                <label for="description">Home Description:</label>
                <textarea name="description" id="description" class="form-control"
                rows="10">{{$flyer->description}}</textarea>
            </div>
        </div>


        {{ Form::close() }}

        <div class="col-md-8">
            <div class="class-form">
                <form style=" width: 159%; margin-left: -15px;" action="/admin/flyer/update/adminPhoto/{{$flyer->id}}" enctype="multipart/form-data" method="post"
                  id="adminUpdateDropzone" class="dropzone">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
              </form>
          </div>
      </div>

      <div class="col-md-5" style="margin-left: -15px;">
        <div class="col-md-3">
            <div class="class-form">
                {{ Form::submit('Update', ['class' => 'btn btn-warning', 'id' => 'updateAdminFlyerSubmitButton']) }}
            </div>
        </div>
    </div>

</div>
</section>
