<section class="content-header">
    <h1>Show featured flyers</h1>
    <ol class="breadcrumb">
        <li><a href="/admin/flyers/show"><i class="fa fa-folder-open"></i> Flyers</a></li>
    </ol>
</section>

<!-- Main content -->
<section class="datatableRow panelAdminD adminFeaturedFlyers">
    <table id="datatableShowFlyers" class="admin table table-bordered table-striped table-hover" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th class="visible-desktop">#</th>
            <th>Product ID</th>
            <th class="visible-desktop">Street</th>
            <th class="visible-desktop">City</th>
            <th class="visible-desktop">Zip</th>
            <th >Featured</th>
            <th style="width: 50px;">Action</th>
        </tr>
        </thead>

        <tbody>
        @foreach($flyers as $flyer)
            <tr>
                <form action="/admin/featured/flyers" id="updateFeaturedFlyersForm" method="get">
                    {{csrf_field()}}
                    <td id="featuredFlyerOnMainPageId">{{$flyer->id}}</td>
                    <td class="responsiveTableIndicator">{{$flyer->productID}}</td>
                    <td>{{$flyer->street}}</td>
                    <td>{{$flyer->city}}</td>
                    <td>{{$flyer->zip}}</td>
                    <td>
                        <input type="checkbox" id="adminFeatured-{{$flyer->id}}"
                               {{$flyer->admin_featured == 'on' ? 'checked' : ''}} name="adminFeatured"/><label
                                id="adminFeaturedCheckbox" for="adminFeatured-{{$flyer->id}}"></label>
                    </td>
                    <td>
                        <input type="hidden" name="id" value="{{$flyer->id}}" />
                        <button type="submit" id="updateFeaturedFlyersButton" class="btn btn-warning">Update</button>
                    </td>
                </form>
            </tr>
        @endforeach
        </tbody>
    </table>
</section>