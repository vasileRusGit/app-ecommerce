@inject('countries' , 'App\Http\Utilities\Country')

<section class="content-header">
    <h1>Add new flyer</h1>
    <ol class="breadcrumb">
        <li><a href="/admin/flyers/show"><i class="fa fa-plus"></i> Flyers</a></li>
        <li class="active">Add new</li>
    </ol>
</section>

<span id="productIdFromDatabase" style="display: table-column;">{{$lastProductId}}</span>

<!-- Main content -->
<section class="datatableRow ">
    <div class="row panelAdminFFlyer">
        <form method="post" enctype="multipart/form-data" class="myForm" id="createFlyerForm" action="/admin/flyers/show">
        {{csrf_field()}}

        <div class="col-md-4">
            <div class="class-form streetForm">
                <label for="street">Street:</label>
                <input type="text" name="street" id="street" class="form-control" value=""
                required/>
            </div>

            <div class="class-form cityForm">
                <label for="city">City:</label>
                <input type="text" name="city" id="city" class="form-control" value=""/>
            </div>

            <div class="class-form zipForm">
                <label for="zip">Zip/Postal Code:</label>
                <input type="text" name="zip" id="zip" class="form-control" value=""/>
            </div>

            <div class="class-form">
                <label for="country">Country:</label>
                <select id="country" name="country" class="form-control">
                    @foreach($countries::all() as $code => $country)
                    <option value="{{$code}}">{{$country}}</option>
                    @endforeach
                </select>
            </div>

            <div class="class-form stateForm">
                <label for="state">State:</label>
                <input type="text" name="state" id="state" class="form-control" value=""/>
            </div>

            <div class="class-form priceForm">
                <label for="price">Sale Price:</label>
                <input type="text" name="price" id="price" class="form-control" value=""/>
            </div>

            <div class="class-form">
                <label for="productID">Product ID:</label>
                <input type="text" name="productID" id="productID" class="form-control" value="" readonly/>
            </div>
        </div>

        <div class="col-md-12"></div>

        <div class="col-md-4 description">
            <div class="class-form" style="padding-top: 10px;">
                <label for="featured">Set Flyer As Featured :</label>
                <input type="checkbox" id="featured" name="featured"/><label id="featuredCheckbox" for="featured"></label>
            </div>
            <div class="class-form">
                <label for="description">Home Description:</label>
                <textarea name="description" id="description" class="form-control"
                rows="10"></textarea>
            </div>
        </div>
       
    </form>

    <div class="col-md-4">
        <div class="class-form" style="margin-top: 0px; width: 107%;">
            <form action="/admin/flyer/create/adminPhotos" enctype="multipart/form-data" method="post"
            id="adminNewDropzone" class="dropzone">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
        </form>
    </div>
</div>

<div class="col-md-12">
    <div class="form-group">
        <button type="submit" name="adminFlyerCreate" id="createAdminFlyerSubmitButton" class="btn btn-primary">
            Create flyer
        </button>
    </div>
</div>
</div>
</section><!-- /.content -->
