<section class="content-header">
    <h1>Show flyers</h1>
    <ol class="breadcrumb">
        <li><a href="/admin/flyers/show"><i class="fa fa-folder-open"></i> Flyers</a></li>
    </ol>
</section>

<!-- Main content -->
<section class="datatableRow panelAdminD">
    <table id="datatableShowFlyers" class="admin display table table-bordered table-striped table-hover" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th style="width: 20px!important;">#</th>
                <th style="width: 0px!important;">Product ID</th>
                <th class="visible-desktop">Street</th>
                <th class="visible-desktop">City</th>
                <th class="visible-desktop">Zip</th>
                <th class="visible-desktop">Country</th>
                <th class="visible-desktop">Price</th>
                <th class="visible-desktop">Description</th>
                <th style="width: 50px!important;">Action</th>
            </tr>
        </thead>

        <tbody>
        <?php $count = 0; ?>
            @foreach($flyers as $flyer)
            <?php $count++; ?>
            <tr>
                <td>{{$count}}</td>
                <td>{{$flyer->productID}}</td>
                <td>{{$flyer->street}}</td>
                <td>{{$flyer->city}}</td>
                <td>{{$flyer->zip}}</td>
                <td>{{$flyer->country}}</td>
                <td>{{$flyer->price}}</td>
                <td>{{$flyer->description}}</td>
                <td>
                    <div class="position-edit-delete-icons">
                        &nbsp<a href="/admin/flyer/{{$flyer->id}}/edit"><i class="ion-edit"></i></a>&nbsp&nbsp&nbsp&nbsp&nbsp|
                        <a>
                            {{ Form::open(['method' => 'DELETE','id' => 'submitDeleteForm', 'class' => 'adminDeleteFlyerFormOpen', 'route' => ['admin.flyer.destroy', $flyer->id]]) }}
                            {{ Form::button('<i class="ion-ios-trash"></i>',['onclick' => 'deleteFlyer()','type' => 'submit','class' => 'adminDeleteFlyerButton', 'id' => 'adminDeleteReplyReview'])}}
                            {{ Form::close() }}</a>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </section>