<section class="content-header">
	<h1>Edit review</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Review</a></li>
	</ol>
</section>

<!-- Main content -->
<section class="datatableRow">

	@if(count($errors) > 0)
	<div class="alert alert-danger">
		<ul>
			@foreach($errors->all() as $error)
			<li>{{$error}}</li>
			@endforeach
		</ul>
	</div>
	@endif


	<div class="row adminReviewEdit">
		<div class="col-md-12" style=" margin-bottom: -50px;">
			<div class="col-md-6 flyerReview">
				<div class="panel panel-primary">
					<div class="panel-heading">Flyer Review</div>
					<div class="panel-body">
						{{ Form::open(['method' => 'PUT', 'id' => 'adminEditReviewForm', 'route' => ['admin.review.update', $review->id]]) }}

						{{csrf_field()}}
						<div class="row adminReviewTextarea">
							<textarea id="reviewTextareaText" name="reviewTextareaText">{{$review->review}}</textarea>
						</div>
						<div class="row" style="margin-right: 0;float: right;">
							<button class="btn btn-primary">Update</button>
						</div>
						{{ Form::close() }}
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="pieSection">
					<div class="pieID pie">

					</div>
					<ul class="pieID legend" style="width: 250px;">
						<li>
							<em>Number of replies</em>
							<span id="numberOfRepliesToReivew">{{count($replyReviews)}}</span>
						</li>
						<li>
							<em>Number of rates</em>
							<span id="numberOfRatesToReivew">{{count($ratingReviews)}}</span>
						</li>
					</ul>
				</div>
			</div>
		</div>


		{{-- ADD REPLY REVIEW TO FLYER MODAL --}}
		<div id="replyReviewAdminModal" name="replyReviewAdminModal" class="modal">
			<div class="modal-dialog animated">
				<div class="modal-content">
					<form class="form-horizontal" id="replyReviewAdminModalForm" method="GET"
					action="/admin/reply/review/add/{{$review->id}}">
					{{csrf_field()}}
					<div class="modal-header">
						<strong>Add new Reply</strong>
					</div>

					<div class="modal-body">
						<div class="form-group">
							<textarea type="text" name="replyReviewTextareaAdminModal" id="reviewTextarea"
							class="form-control"></textarea>
						</div>
					</div>

					<div class="modal-footer">
						<button class="btn btn-default" id="replyReviewModalCancelButton" type="button">Cancel
						</button>
						<button class="btn btn-primary {{$review->review_id}}" type="submit">Save
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>


	{{-- RATING REVIEW TO FLYER MODAL --}}
	<div id="ratingReviewAdminModal" name="rateReviewAdminModal" class="modal">
		<div class="modal-dialog animated">
			<div class="modal-content">
				<form class="form-horizontal" id="rateReviewAdminModalForm" method="GET"
				action="/admin/rate/review/add/{{$review->id}}">
				{{csrf_field()}}
				<div class="modal-header">
					<strong>Add new rating</strong>
				</div>

				<div class="modal-body">
					<div class="form-group">
						<select id="reviewRatingAdminModal" name="reviewRatingAdminModal" class="form-control">
							@for($i = 0; $i<=5; $i++)
							<option value="{{$i}}">{{$i}}</option>
							@endfor
						</select>
					</div>
				</div>

				<div class="modal-footer">
					<button class="btn btn-default" id="rateReviewModalCancelButton" type="button">Cancel</button>
					<button class="btn btn-primary {{$review->review_id}}" type="submit">Save
					</button>
				</div>
			</form>
		</div>
	</div>
</div>


<div class="col-md-12">
	<div class="col-md-6 reviewReplyFlyer">
		<div class="panel panel-primary">
			<div class="panel-heading">Review Replies
				<i data-toggle="modal" id="addReplyReviewAdminModal" data-target="#replyReviewAdminModal" style="float:right; margin-top:10px"
				class="fa fa-plus-circle" aria-hidden="true"></i>
			</div>

			<table id="datatableShowFlyers" class="table table-bordered table-hover" width="100%">
				<thead>
					<tr>
						<th  style="width:20px;">#</th>
						<th >User Name</th>
						<th class="visible-desktop">Reply to review</th>
						<th style="width:70px">Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach($replyReviews as $reply)

					<tr>
						<th>{{$reply->id}}</th>
						<th>{{$reply->name}}</th>
						<th name="replyReviewTextareaText">{{$reply->reply_review}}</th>
						<th>
							<button data-toggle="modal" id="editReviewRepliesButton-{{ $reply->id }}" data-target="#editReplyReviewAdminModal" type="button"  class="btn btn-warning">Edit</button>
							{{ Form::open(['method' => 'DELETE','id' => 'submitDeleteReplyReviewForm', 'class' => 'adminDeleteReplyReviewFormOpen', 'route' => ['admin.reply.review.destroy', $reply->id]]) }}
							<button type="submit" class="btn btn-danger">Delete</button>
							{{ Form::close() }}
						</th>
						{{-- EDIT REPLY REVIEW TO FLYER MODAL --}}
						<div id="editReplyReviewAdminModal-{{ $reply->id }}" name="editReviewAdminModal" class="modal">
							<div class="modal-dialog animated">
								<div class="modal-content">
								<form class="form-horizontal" id="editReviewAdminModalForm" method="GET" action="/admin/reply/review/edit/{{$reply->id}}">
									{{csrf_field()}}
									<div class="modal-header">
										<strong>Edit your Reply</strong>
									</div>

									<div class="modal-body">
										<div class="form-group">
											<textarea style="min-height:160px" type="text" name="editReplyReviewTextareaAdminModal" id="editReplyReviewTextareaAdminModal"
											class="form-control">{{$reply->reply_review}}</textarea>
										</div>
									</div>

									<div class="modal-footer">
										<button class="btn btn-default" id="editReplyReviewModalCancelButton" type="button">Cancel
										</button>
										<button class="btn btn-primary {{$review->review_id}}" type="submit">Save
										</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-md-6 reviewRatingsFlyer">
		<div class="panel panel-primary">
			<div class="panel-heading">Review Ratings
				<i data-toggle="modal" id="addNewReviewRatingToModal" data-target="#ratingReviewAdminModal"
				style="float:right; margin-top:10px" class="fa fa-plus-circle" aria-hidden="true"></i>
			</div>
			<table id="datatableShowFlyers" class="table table-bordered table-hover" width="100%">
				<thead>
					<tr>
						<th class="visible-desktop" style="width:20px;">#</th>
						<th >User Name</th>
						<th >Rating to review</th>
						<th style="width:70px">Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach($ratingReviews as $rating)
					<tr>
						{{ Form::open(['method' => 'PUT', 'id' => 'adminEditRatingReviewForm', 'route' => ['admin.rating.review.update', $rating->id]]) }}
						{{csrf_field()}}
						<td>{{$rating->id}}</td>
						<td>{{$rating->name}}</td>
						<td style="text-align: center;">
							<select id="editReviewRatingAdminModal" name="editReviewRatingAdminModal" class="form-control">
								@for($i = 0; $i<=5; $i++)
								@if($rating->rating_review == $i)
								<option selected value="{{$rating->rating_review}}">{{$rating->rating_review}}</option>
								@else
								<option value="{{$i}}">{{$i}}</option>
								@endif
								@endfor
							</select>
						</td>
						<td>
							<button type="submit" class="btn btn-warning" id="updateReviewRatingFlyer">Update</button>
							{{ Form::close() }}
							{{ Form::open(['method' => 'DELETE','id' => 'submitDeleteRatingReviewForm', 'class' => 'adminDeleteRatingReviewFormOpen', 'route' => ['admin.rating.review.destroy', $rating->id]]) }}
							<button type="submit" class="btn btn-danger">Delete</button>
							{{ Form::close() }}
						</td>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>

</div>
</section>
