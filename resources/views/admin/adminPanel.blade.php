@extends('layout')

@section('content')
    <h1>Admin panel</h1>

    <table class="table panelAdmin">
        <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Email</th>
            <th>Guest</th>
            <th>User</th>
            <th>Employee</th>
            <th>Admin</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <form action="/admin" method="get">
                    {{csrf_field()}}
                    <th scope="row">{{$user->id}}</th>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}<input type="hidden" name="email" value="{{$user->email}}"></td>

                    <td><input type="checkbox" {{$user->hasRole('guest') ? 'checked' : ''}} name="role_guest"></td>
                    <td><input type="checkbox" {{$user->hasRole('user') ? 'checked' : ''}} name="role_user"></td>
                    <td><input type="checkbox" {{$user->hasRole('employee') ? 'checked' : ''}} name="role_employee"></td>
                    <td><input type="checkbox" {{$user->hasRole('admin') ? 'checked' : ''}} name="role_admin"></td>
                    <td><button type="submit" class="btn btn-primary">Assign Role</button></td>
                </form>
            </tr>
        @endforeach
        </tbody>
    </table>
@stop