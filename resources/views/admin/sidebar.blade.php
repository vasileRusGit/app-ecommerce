<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">

    <!-- Sidebar user panel (optional) -->
    <div class="user-panel">
        <div class="pull-left image" style="min-height: 40px;">
            @if($user->image_path == null)
            <img src="/images/no-user-image.png" id="imageHolderUserAccountSidebar"/>
            @else
            <img src="/{{ $user->image_path }}" id="imageHolderUserAccountSidebar" />
            @endif
        </div>
        <div class="pull-left info">
            <p>{{$user->name}}</p>
            <!-- Status -->
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
    </div>

    <!-- search form (Optional) -->
    <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
            <input type="text" name="q" class="form-control" placeholder="Search..."/>
            <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
            </span>
        </div>
    </form>
    <!-- /.search form -->

    <!-- Sidebar Menu -->
    <ul class="sidebar-menu">
        <li><a href="/admin"><i class="dash"></i><span><i class="fa fa-dashboard"></i>&nbsp&nbsp&nbsp Dashboard</span></a></li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-folder-open"></i>
            <span>FLYERS</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/admin/flyer/create"><i class="newFlyer"></i><span><i class="fa fa-plus"></i>&nbsp&nbsp&nbsp&nbsp Add new</span></a></li>
            <li><a href="/admin/flyers/show"><i class="showFlyer"></i><span><i class="fa fa-folder-open"></i>&nbsp&nbsp Manage flyers</span></a></li>
            <li><a href="/admin/featured/flyers/show"><i class="featuredFlyer"></i><span><i class="fa fa-eye"></i>&nbsp&nbsp Manage featured flyers</span>
            </a></li>
          </ul>
        </li>




        @if(\Auth::user()->hasRole('admin'))
        <li class="treeview">
          <a href="#">
            <i class="fa fa-star"></i>
            <span>FLYER RATING & REVIEW</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/admin/flyers/rating/show"><i class="ratingFlyers"></i><span><i class="fa fa-star"></i>&nbsp&nbsp Manage flyer ratings</span></a></li>
        <li><a href="/admin/flyers/review/show"><i class="rewviewFlyers"></i><span><i class="fa fa-comments"></i>&nbsp&nbsp Manage flyer reviews</span></a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-file-text"></i>
            <span>INVOICES</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/admin/invoice/create"><i class="invoiceCreate"></i><span><i class="fa fa-plus" aria-hidden="true"></i>&nbsp&nbsp Add new invoice</span></a></li>
        <li><a href="/admin/invoice/show"><i class="invoiceManage"></i><span><i class="fa fa-book" aria-hidden="true"></i>&nbsp&nbsp&nbsp Manage invoices</span></a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-users"></i>
            <span>USERS</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/admin/user/create"><i class="userCreate"></i><span><i class="fa fa-user-plus"></i>&nbsp&nbsp Add new user</span></a></li>
            <li><a href="/admin/users/profiles"><i class="userProfiles"></i><span><i class="fa fa-user"></i>&nbsp&nbsp&nbsp Users profiles</span></a></li>
            <li><a href="/admin/users/permission"><i class="userPermissions"></i><span><i class="fa fa-users"></i>&nbsp&nbsp&nbsp Users permisions</span></a></li>
          </ul>
        </li>
        @endif
        
        
{{--         @if(\Auth::user()->hasRole('admin'))
        <li class="header">USERS</li>
        <li><a href="/admin/user/create"><i class="userCreate"></i><span><i class="fa fa-user-plus"></i>&nbsp&nbsp Add new user</span></a></li>
        <li><a href="/admin/users/profiles"><i class="userProfiles"></i><span><i class="fa fa-user"></i>&nbsp&nbsp&nbsp Manage users profiles</span></a></li>
        <li><a href="/admin/users/permission"><i class="userPermissions"></i><span><i class="fa fa-users"></i>&nbsp&nbsp&nbsp Manage users permisions</span></a></li>
        @endif --}}
    </ul><!-- /.sidebar-menu -->
</section><!-- /.sidebar -->