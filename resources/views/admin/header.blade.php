<!-- Logo -->
<a href="/admin" class="logo" id="userName"><b>Admin</b> Pannel</a>

<!-- Header Navbar -->
<nav class="navbar navbar-static-top" role="navigation">
	<!-- Sidebar toggle button-->
	<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
	<!-- Navbar Right Menu -->
	<div class="navbar-custom-menu" style="margin-top: -35px;">
		<ul class="nav navbar-nav">
			<!-- Messages: style can be found in dropdown.less-->
			<li class="dropdown messages-menu" style="margin-top: 20px;">
				<!-- Menu toggle button -->
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
					<i class="fa fa-envelope-o"></i>
					<span class="label label-success">4</span>
				</a>
				<ul class="dropdown-menu messages">
					<li class="header">You have 4 messages</li>
					<li>
						<!-- inner menu: contains the messages -->
						<ul class="menu">
							<li><!-- start message -->
								<a href="#">
									<div class="pull-left">
										<!-- User Image -->
										<i class="ion-person"></i>
										{{-- <!-- <img src="{{ asset("/bower_components/admin-lte/dist/img/user2-160x160.jpg") }}" class="img-circle" alt="User Image"/> --> --}}
									</div>
									<!-- Message title and timestamp -->
									<h4>
										Support Team
										<small><i class="fa fa-clock-o"></i> 5 mins</small>
									</h4>
									<!-- The message -->
									<p>Why not buy a new awesome theme?</p>
								</a>
							</li><!-- end message -->
						</ul><!-- /.menu -->
					</li>
					<li class="footer"><a href="#">See All Messages</a></li>
				</ul>
			</li><!-- /.messages-menu -->

			<!-- Notifications Menu -->
			<li class="dropdown notifications-menu" style="margin-top: 20px;">
				<!-- Menu toggle button -->
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
					<i class="fa fa-bell-o"></i>
					<span class="label label-warning">10</span>
				</a>
				<ul class="dropdown-menu notifications">
					<li class="header">You have 10 notifications</li>
					<li>
						<!-- Inner Menu: contains the notifications -->
						<ul class="menu">
							<li><!-- start notification -->
								<a href="#">
									<i class="fa fa-users text-aqua"></i> 5 new members joined today
								</a>
							</li><!-- end notification -->
						</ul>
					</li>
					<li class="footer"><a href="#">View all</a></li>
				</ul>
			</li>
			<!-- Tasks Menu -->
			<li class="dropdown tasks-menu" style="margin-top: 20px;">
				<!-- Menu Toggle Button -->
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
					<i class="fa fa-flag-o"></i>
					<span class="label label-danger">{{count($flyer)}}</span>
				</a>
				<ul class="dropdown-menu tasks">
					<li class="header">You have {{count($flyer)}} flyers</li>
					<li>
						<!-- Inner menu: contains the tasks -->
						<ul class="menu">
							<li><!-- Task item -->
								<a href="#">
									<!-- Task title and progress text -->
									<h3>
										Design some buttons
										<small class="pull-right">50%</small>
									</h3>
									<!-- The progress bar -->
									<div class="progress xs">
										<!-- Change the css width attribute to simulate progress -->
										<div class="progress-bar progress-bar-aqua" style="width: 50%" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">
											<span class="sr-only">50% Complete</span>
										</div>
									</div>
								</a>
							</li><!-- end task item -->
						</ul>
					</li>
					<li class="footer">
						<a href="#">View all tasks</a>
					</li>
				</ul>
			</li>
			<!-- User Account Menu -->
			<li class="dropdown user user-menu">
				<!-- Menu Toggle Button -->
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" style="padding-top: 25px; padding-bottom: 10px;">
					<!-- The user image in the navbar-->
					@if($user->image_path == null)
						<img src="/images/no-user-image.png" id="imageHolderUserAccountHeader"/>
					@else
						<img src="/{{ $user->image_path }}" id="imageHolderUserAccountHeader" />
					@endif
					<span class="hidden-xs">{{$user->name}}</span>
				</a>
				<ul class="dropdown-menu usermenu">
					<!-- The user image in the menu -->
					<li class="user-header" style="margin-top: -2px;">
						@if($user->image_path == null)
						<img src="/images/no-user-image.png" id="imageHolderUserAccount"/>
						@else
						<img src="/{{ $user->image_path }}" id="imageHolderUserAccount" />
						@endif
							
						<p>
							{{$user->name}} - Web Developer
							<small>Member since {{date_format($user->created_at, "d-M-Y")}}</small>
							<a href="/">Visit Website</a>
						</p>
					</li>
					<!-- Menu Footer-->
					<li class="user-footer">
						<div class="pull-left">
							<a href="/admin/user/{{$user->id}}/edit" class="btn btn-default btn-flat">Edit Account</a>
						</div>
						<div class="pull-right">
							{{-- <a href="/logout" class="btn btn-default btn-flat">Sign out</a> --}}
							<a href="{{ url('/logout') }}" class="btn btn-default btn-flat"
                                   onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ url('/logout') }}" method="POST"
                                      style="display: none;">
                                    {{ csrf_field() }}
                                </form>
						</div>
					</li>
				</ul>
			</li>
		</ul>
	</div>
</nav>