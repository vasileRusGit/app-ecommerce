<section class="content-header">
	<h1>Users profiles</h1>
	<ol class="breadcrumb">
		<li><a href="/admin/users/profiles"><i class="fa fa-user"></i> Users</a></li>
	</ol>
</section>

<!-- Main content -->
<section class="datatableRow panelAdminD">

	{{-- {{dump($pass)}} --}}

	@if(count($errors) > 0)
	<div class="alert alert-danger">
		<ul>
			@foreach($errors->all() as $error)
			<li>{{$error}}</li>
			@endforeach
		</ul>
	</div>
	@endif

	<table id="datatableShowUsers" class="table table-bordered table-hover">
		<thead>
			<tr>
				<th style="width:20px;">#</th>
				<th>Name</th>
				<th class="visible-desktop">Email</th>
				<th style="width:20px;">Action</th>
			</tr>
		</thead>
		<tbody>
			@foreach($users as $user)
			<tr>
				<td scope="row">{{$user->id}}</td>
				<td>{{$user->name}}</td>
				<td>{{$user->email}}</td>
				<td>
					<div class="position-edit-delete-icons">
						&nbsp<a href="/admin/user/{{$user->id}}/edit"><i class="ion-edit"></i></a>&nbsp&nbsp&nbsp&nbsp&nbsp
						<a>
						@if((\Auth::user()->id) !== $user->id)
						|
							{{ Form::open(['method' => 'DELETE','id' => 'submitDeleteForm', 'class' => 'adminDeleteUserFormOpen', 'route' => ['admin.user.destroy', $user->id]]) }}
							{{ Form::button('<i class="ion-ios-trash"></i>',['type' => 'submit','class' => 'adminDeleteUserButton', 'id' => 'adminDeleteUser'])}}
							{{ Form::close() }}</a>
							@endif
						</div>
					</tr>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>

</section><!-- /.content