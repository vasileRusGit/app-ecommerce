<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Users permision</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-users"></i> Users permision</a></li>
    </ol>
</section>

<!-- Main content -->
<section class="datatableRow panelAdminD">

<table id="datatableShowUsers" class="table table-bordered table-hover" width="100%">
	<thead class="thead-inverse">
		<tr>
			<th class="visible-desktop" style="width:40px;">#</th>
			<th >Name</th>
			<th class="visible-desktop">Email</th>
			<th class="visible-desktop">Role-User</th>
			<th class="visible-desktop">Role-Employee</th>
			<th >Role-Admin</th>
			<th  style="width:60px;">Action</th>
		</tr>
	</thead>
	<tbody>
		@foreach($users as $user)
		<tr>
			<form action="/admin/users/permission/{{ $user->id }}" method="get">
				{{csrf_field()}}
				<td scope="row">{{$user->id}}</td>
				<td>{{$user->name}}</td>
				<td>{{$user->email}}<input type="hidden" name="email" value="{{$user->email}}"></td>

				<td style="    text-align: center;"><input type="checkbox" id="checkbox-{{$user->id}}-1" {{$user->hasRole('user') ? 'checked' : ''}} name="role_user"/><label for="checkbox-{{$user->id}}-1"></label></td>
				<td style="    text-align: center;"><input type="checkbox" id="checkbox-{{$user->id}}-2" {{$user->hasRole('employee') ? 'checked' : ''}} name="role_employee"/><label for="checkbox-{{$user->id}}-2"></label></td>
				<td style="    text-align: center;"><input type="checkbox" id="checkbox-{{$user->id}}-3" {{$user->hasRole('admin') ? 'checked' : ''}} name="role_admin"/><label for="checkbox-{{$user->id}}-3"></label></td>
				<td><button type="submit" class="btn btn-primary">Assign Role</button></td>
			</form>
		</tr>
		@endforeach
	</tbody>
</table>

</section><!-- /.content -->