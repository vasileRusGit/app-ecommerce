@inject('countries' , 'App\Http\Utilities\Country')

<section class="content-header">
	<h1>Edit User</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> User</a></li>
	</ol>
</section>


<!-- Main content -->
<section class="datatableRow">
	<div class="row">
		{{-- use account informations --}}
		<div class="col-md-4 panelAdminEditUser">
			{{ Form::open(['method' => 'PUT', 'id' => 'adminEditUserForm','route' => ['admin.user.update', $userAccount->user_id], 'files' => true]) }}
			{{csrf_field()}}

			<div class="col-md-4">
				<div class="adminEditAccount">User Account Information</div>
				<div class="class-form usernameForm">
					<label for="name">User Name:</label>
					<input type="text" name="name" id="name" class="form-control" value="{{$userAccount->name}}"
					required/>
				</div>

				<div class="class-form emailForm">
					<label for="email">User Email:</label>
					<input type="email" name="email" id="email" class="form-control" value="{{$userAccount->email}}" required/>
				</div>

				<div class="class-form passwordForm" id="passwordInputForm">
					<label for="password">Password:</label>
					<input type="password" name="password" id="password" class="form-control"/>
				</div>

				<div class="class-form repeatPasswordForm" id="repeatPasswordInputForm">
					<label for="repeat-password">Repeat Password:</label>
					<input type="password" name="repeat-password" id="repeat-password" class="form-control"/>
				</div>
				<div class="class-form">
					<label>User Image:</label>
					<label for="custom-file-upload" class="userImage">
						<span class="filupp-file-name js-value">Browse Files</span>
						<input type="file" name="editUserImage" value="1" id="custom-file-upload"/>
					</label>
					<span id="imageHolder" 
					@if($user->image_path == null)
					style="background-image: url('/images/no-user-image.png'); background-size: 110px 110px;"
					@else
					style="background-image: url('/{{$userAccount->image_path}}')!important; background-size: 110px 110px;"
					@endif
					></span>
				</div>
			</div>
			{{ Form::close() }}
			<div class="col-md-9" style="margin-top:20px; margin-left: -15px;">
				<div class="col-md-3">
					<div class="class-form">
						{{ Form::submit('Update Account', ['class' => 'btn btn-warning', 'id' => 'updateAdminUserSubmitButton']) }}
					</div>
				</div>
			</div>
		</div>


		{{-- user deliverry informations --}}
		<div class="col-md-4 panelAdminEditUser">
			{{ Form::open(['method' => 'PUT', 'id' => 'adminEditDeliveryUserForm','route' => ['admin.user.delivery.update', $userAccount->user_id]]) }}
			{{csrf_field()}}

			{{-- delivery --}}
			<div class="col-md-4">
				<div class="adminEditAccount">Delivery Address Information</div>
				<div class="class-form usernameForm">
					<label for="country">Country :</label>
					<input type="text" name="country" id="country" class="form-control" value="{{$userAccount->country}}"/>
				</div>

				<div class="class-form">
					<label for="state">State :</label>
					<input type="text" name="state" id="state" class="form-control" value="{{$userAccount->state}}"/>
				</div>

				<div class="class-form ">
					<label for="address">Address:</label>
					<input type="text" name="address" id="address" class="form-control" value="{{$userAccount->address}}"/>
				</div>

				<div class="class-form">
					<label for="zip">Zip :</label>
					<input type="number" name="zip" id="zip" class="form-control" value="{{$userAccount->zip}}"/>
				</div>

				<div class="class-form">
					<label for="phone">Phone :</label>
					<input type="number" name="phone" id="phone" class="form-control" value="{{$userAccount->phone}}"/>
				</div>

			</div>

			<div class="col-md-9" style="margin-top:20px; margin-left: -15px;">
				<div class="col-md-3">
					<div class="class-form">
						{{ Form::submit('Update Delivery Address', ['class' => 'btn btn-warning updateDeliveryAddress']) }}
					</div>
				</div>
			</div>

			{{ Form::close() }}
		</div>


		{{-- user credit card informations --}}
		<div class="col-md-4 panelAdminEditUser">
			{{ Form::open(['method' => 'PUT', 'id' => 'adminEditUserForm','route' => ['admin.user.card.update', $userAccount->user_id]]) }}
			{{csrf_field()}}

			<div class="col-md-4">
				<div class="adminEditAccount">User Credit-Card Information</div>
				<label for="card_number">Card Number:</label>
				<div class="col-md-12 creditCardNumbers" style="    margin-left: -15px;    width: 107%;">
					<div class="col-md-3" style="padding-left: 0px; padding-right: 5px;">
						<input id="cardNumber1" type="text" maxlength="4" oninput="this.value=this.value.replace(/[^0-9]/g,'');" class="form-control" value="{{ $userAccount->card_number_1 }}" name="card_number_1">
					</div>
					<div class="col-md-3" style="padding-left: 5px; padding-right: 5px;">
						<input id="cardNumber2" type="text" maxlength="4" oninput="this.value=this.value.replace(/[^0-9]/g,'');" class="form-control" value="{{ $userAccount->card_number_2 }}" name="card_number_2" >
					</div>
					<div class="col-md-3" style="padding-left: 5px; padding-right: 5px;">
						<input id="cardNumber3" type="text" maxlength="4" oninput="this.value=this.value.replace(/[^0-9]/g,'');" class="form-control" value="{{ $userAccount->card_number_3 }}" name="card_number_3">
					</div>
					<div class="col-md-3" style="padding-left: 5px; padding-right: 0;">
						<input id="cardNumber4" type="text" maxlength="4" oninput="this.value=this.value.replace(/[^0-9]/g,'');" class="form-control" value="{{ $userAccount->card_number_4 }}" name="card_number_4" >
					</div>
				</div>


				<div class="class-form">
					<label for="card_holder">Card Holder :</label>
					<input type="text" name="card_holder" id="card_holder" class="form-control" value="{{$userAccount->card_holder}}"/>
				</div>

				<div class="form-group" style="margin-top: 20px">
					<label for="card_expiration_month" class="col-md-4 control-label" style=" margin-left: -15px; margin-right: -35px;">Expiration Month: </label>
					<div class="col-md-3">
						<input id="card_expiration_month" type="text" class="form-control" value="{{ $userAccount->card_expiration_month }}" name="card_expiration_month" maxlength="2" oninput="this.value=this.value.replace(/[^0-9]/g,'');" style="max-width:80px">
					</div>
					<label for="cardExpirationYear" class="col-md-4 control-label" style="margin-left: 2%; padding-left: 0;width: 25%;">Expiration Year: </label>
					<div class="col-md-3">
						<input id="card_expiration_year" type="text" class="form-control" value="{{ $userAccount->card_expiration_year }}" name="card_expiration_year" maxlength="4" oninput="this.value=this.value.replace(/[^0-9]/g,'');" style="max-width:80px">
					</div>
				</div>

				<div class="class-form">
					<label for="card_ccv">CCV :</label>
					<input type="number" name="card_ccv" id="card_ccv" class="form-control" value="{{$userAccount->card_ccv}}"/>
				</div>

				<div class="col-md-9" style="margin-top:20px; margin-left: -30px;">
					<div class="col-md-3">
						<div class="class-form">
							{{ Form::submit('Update Credit Card', ['class' => 'btn btn-warning updateCreditCard']) }}
						</div>
					</div>
				</div>
				{{ Form::close() }}
				
			</div>

		</div>
	</section>

