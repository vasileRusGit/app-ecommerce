<section class="content-header">
	<h1>Add new user</h1>
	<ol class="breadcrumb">
		<li><a href="/admin/users/profiles"><i class="fa fa-user-plus"></i> Users</a></li>
		<li class="active">Add new</li>
	</ol>
</section>

<!-- Main content -->
<section class="datatableRow">

	<div class="row panelAdminF">
		{{ Form::open(['method' => 'PUT', 'id'=> 'adminCreateUserForm', 'route' => ['admin.user.create'], 'files' => true]) }}
		{{csrf_field()}}
		<div class="col-md-4">
			<div class="class-form usernameForm">
				<label for="name">Username:</label>
				<input type="text" name="name" id="name" class="form-control" value="{{ !empty($oldName) ? $oldName : null}}"/>
			</div>
			<div class="class-form emailForm">
				<label for="email">Email:</label>
				<input type="email" name="email" id="email" class="form-control" value="{{ !empty($oldEmail) ? $oldEmail : null}}"/>
			</div>
			<div class="class-form passwordForm" id="passwordInputForm">
				<label for="password">Password:</label>
				<input type="password" name="password" id="password" class="form-control" value="" />
			</div>
			<div class="class-form repeatPasswordForm">
				<label for="repeat-password">Repeat password:</label>
				<input type="password" name="repeat-password" id="repeat-password" class="form-control" value=""/>
			</div>
			<div class="class-form">
				<label>User Image:</label>
				<label for="custom-file-upload" class="userImage">
					<span class="filupp-file-name js-value">Browse Files</span>
					<input type="file" name="userImage" value="1" id="custom-file-upload"/>
				</label>
				<span id="imageHolder"></span>
			</div>
		</div>
		{{ Form::close() }}

		<div class="col-md-9" style="margin-top:20px; margin-left: -15px;">
				<div class="col-md-3">
					{{ Form::submit('Create', ['class' => 'btn btn-primary', 'id' => 'createAdminUserSubmitButton']) }}
				</div>
		</div>
	</div>

</section><!-- /.content -->