<section class="content-header">
    <h1>
        Add new invoice
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="/admin/invoice/show">
                <i class="fa fa-plus">
                </i>
                Invoice
            </a>
        </li>
        <li class="active">
            Add new
        </li>
    </ol>
</section>
<!-- Main content -->
<section class="datatableRow">
    <div class="row addNewInvoice panelAdminF">
        <form action="/admin/invoice/store" class="myForm" id="createFlyerForm" method="post">
            {{csrf_field()}}
            <div class="col-md-4">
                <div class="class-form usernameForm">
                    <label for="user-name">
                        User:
                    </label>
                    <select class="form-control" id="user-name" name="user-name">
                        @foreach($allUsers as $user)
                        <option value="{{$user->name}}">
                            {{$user->name}}
                        </option>
                        @endforeach
                    </select>
                </div>
                <div class="class-form">
                    <label for="invoiceid">
                        Invoice ID:
                    </label>
                    <input class="form-control" readonly id="invoiceid" name="invoiceid" type="text" value="{{ $invoiceId }}"/>
                </div>
                <div class="class-form passwordForm" id="passwordInputForm">
                    <label for="payment-method">
                        Payment Method:
                    </label>
                    <select class="form-control" id="payment-method" name="payment-method">
                        <option value="Credit Card">
                            Credit Card
                        </option>
                        <option value="Ramburs">
                            Ramburs
                        </option>
                    </select>
                </div>
                <div class="class-form repeatPasswordForm">
                    <label for="payment-sum">
                        Payment Sum:
                    </label>
                    <input class="form-control" required="true" id="payment-sum" name="payment-sum" type="number" value=""/>
                </div>
            </div>
            <div class="col-md-12" style="margin-top: 20px">
                <div class="form-group">
                    <button class="btn btn-primary" id="createAdminInvoiceSubmitButton" name="adminInvoiceCreate" type="submit">
                        Create invoice
                    </button>
                </div>
            </div>
        </form>
    </div>
</section>