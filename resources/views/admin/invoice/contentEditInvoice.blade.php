<section class="content-header">
    <h1>
        Edit invoice
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="/admin/invoice/show">
                <i class="fa fa-plus">
                </i>
                Invoice
            </a>
        </li>
        <li class="active">
            Edit invoice
        </li>
    </ol>
</section>
<!-- Main content -->
<section class="datatableRow">
    <div class="row panelAdminF">
        <form action="/admin/invoice/{{ $invoice->invoice_id }}/update" class="myForm" id="createFlyerForm" method="post">
            {{csrf_field()}}
            <div class="col-md-4">
                <div class="class-form usernameForm">
                    <label for="user-name">
                        User:{{ $invoice->name }}
                    </label>
                    <select class="form-control" id="user-name" name="user-name">
                        @foreach($allUsers as $customer)
                        @if($customer->name == $invoice->name)
                          <option value="{{$customer->name}}" selected>
                            {{$customer->name}}
                          </option>
                        @else
                        <option value="{{$customer->name}}">
                          {{$customer->name}}
                          </option>
                        @endif
                        @endforeach
                    </select>
                </div>
                <div class="class-form">
                    <label for="invoiceid">
                        Invoice ID:
                    </label>
                    <input class="form-control" readonly id="invoiceid" name="invoiceid" type="text" value="{{ $invoice->invoice_id }}"/>
                </div>
                <div class="class-form passwordForm" id="passwordInputForm">
                    <label for="payment-method">
                        Payment Method:
                    </label>
                    <select class="form-control" id="payment-method" name="payment-method">
                        @if($invoice->payment_method == 'Credit Card')
                        <option value="Credit Card" selected>
                            Credit Card
                        </option>
                        <option value="Ramburs">
                            Ramburs
                        </option>
                        @else
                        <option value="Credit Card">
                            Credit Card
                        </option>
                        <option value="Ramburs" selected>
                            Ramburs
                        </option>
                        @endif
                    </select>
                </div>
                <div class="class-form repeatPasswordForm">
                    <label for="payment-sum">
                        Payment Sum:
                    </label>
                    <input class="form-control" required="true" id="payment-sum" name="payment-sum" type="number" value="{{ $invoice->payment_sum }}"/>
                </div>
            </div>
            <div class="col-md-12" style="margin-top: 20px">
                <div class="form-group">
                    <button class="btn btn-primary" id="updateAdminInvoiceSubmitButton" name="adminInvoiceUpdate" type="submit">
                        Update invoice
                    </button>
                </div>
            </div>
        </form>
    </div>
</section>