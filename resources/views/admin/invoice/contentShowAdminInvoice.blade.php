<section class="content-header">
    <h1>Manage Invoices</h1>
    <ol class="breadcrumb">
        <li>
            <a href="/admin/invoice/manage"><i class="fa fa-folder-open"></i>Invoices</a>
        </li>
    </ol>
</section>
<!-- Main content -->
<section class="datatableRow panelAdminD">
    <table class="table table-bordered table-hover hidden-phone hidden-tablet panelAdmin" id="datatableShowInvoices" width="100%!important">
        <thead>
            <tr>
            <th style="text-align:center">#</th>
               <th >Invoice ID</th>
               <th class="visible-desktop">Customer Name</th>
               <th class="visible-desktop">Customer Email</th>
               <th class="visible-desktop">Date</th>
               <th class="visible-desktop">Payment</th>
               <th class="visible-desktop" style="width: 120px">Payment Method</th>
               <th style="width:30px">Action</th>
           </tr>
       </thead>
       <tbody>
        <?php $count = 0;?>
        @foreach($invoices as $invoice)
        <?php $count++; ?>
        <tr>
            <td text-align:center">{{$count}}</td>
            <td class="test">{{$invoice->invoice_id}}</td>
            <td >{{$invoice->name}}</td>
            <td>{{$invoice->email}}</td>
            <td>{{ date("F j, Y g:i (A)", strtotime($invoice->updated_at))}}</td>
            <td>$ {{number_format($invoice->payment_sum, 2)}}</td>
            <td>{{$invoice->payment_method}}</td>
            <td>
                <div class="position-edit-delete-icons">
                    &nbsp<a href="/admin/invoice/{{$invoice->invoice_id}}/edit"><i class="ion-edit"></i></a>&nbsp&nbsp&nbsp&nbsp&nbsp|
                    <a>
                        {{ Form::open(['method' => 'DELETE','id' => 'submitDeleteForm', 'class' => 'adminDeleteInvoiceFormOpen', 'route' => ['admin.invoice.destroy', $invoice->id]]) }}
                        {{ Form::button('<i class="ion-ios-trash"></i>',['type' => 'submit','class' => 'adminDeleteFlyerButton', 'id' => 'adminDeleteReplyReview'])}}
                        {{ Form::close() }}</a>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</section>