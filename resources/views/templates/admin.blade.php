<!DOCTYPE html>

<html>
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Admin Panel</title>
    <link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/admin/adminLte.css">
    <link rel="stylesheet" href="/css/admin/skin-blue.css">
</head>

<body class="skin-blue">
    <div class="wrapper">

        {{-- <!-- Main Header -->
        <header class="main-header">
            @include('admin/header')
        </header>


        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">
            @include('admin/sidebar')
        </aside>


        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            @if(Request::path() == "admin")
                @include('admin/contentDashboard')
            @endif
            @if(Request::path() == "admin/flyer/create")
                @include('admin/contentAddNewFlyer')

            @endif
            @if(Request::path() == "admin/flyers")
                @include('admin/contentShowFlyers')
            @endif
        </div><!-- /.content-wrapper --> --}}


        <!-- Main Footer -->
{{--         <footer class="main-footer">
            @include('admin/footer')
        </footer> --}}

    </div><!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->
    <script src="/js/app.js"></script>
    <script src="/js/adminLte.js"></script>
    <script src="/js/adminPanelCustom.js"></script>
</body>
</html>