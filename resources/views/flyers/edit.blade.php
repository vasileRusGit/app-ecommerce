@inject('countries' , 'App\Http\Utilities\Country')

@extends('layout')

@section('content')
<h1 id="edit-flyer">Edit Flyer</h1>
<hr>

@if(count($errors) > 0)
<div class="alert alert-danger">
    <ul>
        @foreach($errors->all() as $error)
        <li>{{$error}}</li>
        @endforeach
    </ul>
</div>
@endif




<div class="row editPhotoContainer" style="margin-top: 20px;">
    <div class="col-md-4 flyerPhotos">
        @foreach($flyer->photos->chunk(4) as $set)
        <div class="row">
            @foreach($set as $photo)

            <div class="col-md-3 gallery_images">
                <form method="POST" action="/photos/{{$photo->id}}">
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="DELETE">
                    <button type="submit" id="removePictureButton"><i class="ion-close-round"></i></button>
                </form>
                <a href="/{{$photo->path}}" data-lightbox="image-1">
                    <img src="/{{$photo->path}}" alt="">
                </a>
            </div>
            @endforeach
        </div>
        @endforeach
    </div>


    {{ Form::open(['method' => 'PUT','id' => 'updateDeleteForm', 'route' => ['flyers.update', $flyer->id]]) }}

    {{csrf_field()}}

    <div class="col-md-6">
        <div class="class-form">
            <label for="street">Street:</label>
            <input type="text" name="street" id="street" class="form-control" value="{{$flyer->street}}"
            required/>
        </div>

        <div class="class-form">
            <label for="city">City:</label>
            <input type="text" name="city" id="city" class="form-control" value="{{$flyer->city}}" required/>
        </div>

        <div class="class-form">
            <label for="zip">Zip/Postal Code:</label>
            <input type="text" name="zip" id="zip" class="form-control" value="{{$flyer->zip}}" required/>
        </div>

        <div class="class-form">
            <label for="country">Country:</label>
            <select id="country" name="country" class="form-control" required>
                @foreach($countries::all() as $code => $country)
                <option value="{{$code}}">{{$country}}</option>
                @endforeach
            </select>
        </div>

        <div class="class-form">
            <label for="state">State:</label>
            <input type="text" name="state" id="state" class="form-control" value="{{$flyer->state}}" required/>
        </div>

        <div class="class-form">
            <label for="price">Sale Price:</label>
            <input type="text" name="price" id="price" class="form-control" value="{{str_replace(',', '', str_replace('$', '',$flyer->price))}}" required/>
        </div>

        <div class="class-form">
            <label for="productID">Product ID:</label>
            <input type="text" name="productID" id="productID" class="form-control" value="{{str_replace(',', '', str_replace('$', '',$flyer->productID))}}" readonly/>
        </div>
    </div>

    <div class="col-md-8">
        <div class="class-form" style="padding-top: 10px;">
            <label for="featured">Set Flyer As Featured :</label>
            <input type="checkbox" id="featured" {{$flyer->featured == 'on' ? 'checked' : ''}} name="featured"/><label id="featuredCheckbox" for="featured"></label>
        </div>
        <div class="class-form">
            <label for="description">Home Description:</label>
            <textarea name="description" id="description" class="form-control" rows="10">{{$flyer->description}}</textarea>
        </div>
    </div>


    {{ Form::close() }}

    <div class="col-md-8" style="margin-top:20px">
        <div class="class-form">
            <form action="/flyers/{{$flyer->id}}/edit/photos" enctype="multipart/form-data" method="post"
                id="userNewFlyerDropzone" class="dropzone">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </form>
        </div>
    </div>
    <div class="col-md-5 updateDeleteButtons">
        <div class="col-md-2">
            <div class="class-form">
                {{ Form::submit('Update', ['class' => 'btn btn-warning', 'id' => 'updateFlyerSubmitButton']) }}
            </div>
        </div>


        {{ Form::open(['method' => 'DELETE', 'route' => ['flyers.destroy', $flyer->id]]) }}
        <div class="col-md-2">
            <div class="class-form">
                {{ Form::submit('Delete', ['class' => 'btn btn-danger', 'id' => 'deleteFlyerButton']) }}
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>

@stop