@extends('layout')

@inject('countries' , 'App\Http\Utilities\Country')


@section('content')
<h1 id="create-flyer">Selling your home ?</h1>
<hr>


@if(count($errors) > 0)
<div class="alert alert-danger">
	<ul>
		@foreach($errors->all() as $error)
		<li>{{$error}}</li>
		@endforeach
	</ul>
</div>
@endif

<span id="productIdFromDatabase" style="display: table-column;">{{$lastProductId}}</span>

<div class="row createNewProduct">
	<form method="post" enctype="multipart/form-data" id="addNewFlyerForm" action="/flyers">

		{{csrf_field()}}

		<div class="col-md-6">
			<div class="class-form">
				<label for="street">Street:</label>
				<input type="text" name="street" id="street" class="form-control" value="{{old('street')}}" required/>
			</div>

			<div class="class-form">
				<label for="city">City:</label>
				<input type="text" name="city" id="city" class="form-control" value="{{old('city')}}" required/>
			</div>

			<div class="class-form">
				<label for="zip">Zip/Postal Code:</label>
				<input type="text" name="zip" id="zip" class="form-control" value="{{old('zip')}}" required/>
			</div>

			<div class="class-form">
				<label for="country">Country:</label>
				<select id="country" name="country" class="form-control" required>
					@foreach($countries::all() as $code => $country)
					<option value="{{$code}}">{{$country}}</option>
					@endforeach
				</select>
			</div>

			<div class="class-form">
				<label for="state">State:</label>
				<input type="text" name="state" id="state" class="form-control" value="{{old('state')}}" required/>
			</div>

			<div class="class-form">
				<label for="price">Sale Price:</label>
				<input type="text" name="price" id="price" class="form-control" value="{{old('price')}}" required/>
			</div>
			<div class="class-form">
				<label for="productID">Product ID:</label>
				<input type="text" name="productID" id="productID" class="form-control" value="{{old('productID')}}" readonly/>
			</div>
		</div>

		<div class="col-md-8">
			<div class="class-form" style="padding-top: 10px;">
				<label for="featured">Set Flyer As Featured :</label>
				<input type="checkbox" id="featured" name="featured"/>
				<label id="featuredCheckbox" for="featured"></label>
			</div>
			<div class="class-form">
				<label for="description">Home Description:</label>
				<textarea name="description" id="description" class="form-control" rows="10" value="{{old('description')}}"></textarea>
			</div>
		</div>
	</form>


	<div class="col-md-8" style="margin-top: 20px;">
		<div class="class-form">
			<form action="/flyers/create/photos" enctype="multipart/form-data" method="post"
			id="userNewFlyerDropzone" class="dropzone">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
		</form>
	</div>
</div>

<div class="col-md-12" style="margin-top:20px">
	<div class="form-group">
		<button type="submit" name="flyerCreate" id="createFlyerSubmitButton" class="btn btn-primary">Create Flyer</button>
	</div>
</div>

</div>

@stop