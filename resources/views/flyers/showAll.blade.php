@extends('layout')

@section('content')
{{-- grid list buttons and filter dropdown --}}
<div class="row gridListButtonsAndFilterDropDownRow">
  <div class="col-md-10">
    <div class="buttons">
      <div class="list-button"><span class="active"></span><span class="active"></span></div>
      <div class="grid-button"><span></span><span></span><span></span><span></span></div>
    </div>
  </div>

  <div class="col-md-2" id="filterDropdown">
    <select class="js-example-basic-single">
      <option selected value="cele-mai-recente">Cele mai recente</option>
      <option value="cele-mai-ieftine">De la cele mai ieftine</option>
      <option value="cele-mai-scumpe">De la cele mai scumpe</option>
      <option value="cei-mai-multi-km">De la cei mai multi KM</option>
      <option value="cei-mai-putini-km">De la cei mai putini KM</option>
    </select>
  </div>
</div>
{{-- end grid list buttons and filter dropdown --}}

@if($cookievalue == 'list')
<?php $numberOfChuncksPerRow = 1; ?>
@endif
@if($cookievalue == 'grid')
<?php $numberOfChuncksPerRow = 4; ?>
@endif

<div class="preload"><img src="/images/loading.gif"></div>


<div class="row content showAllFlyers allFlyersRow" id="grid">
  @foreach($allFlyers->chunk($numberOfChuncksPerRow) as  $flyers)
  <div class="gridListView">
    @foreach($flyers as $flyer)

    <?php 
    $ratings = 0;
    foreach($flyer->rating as $rating){
      if($rating->rating !== 'undefined'){
        $ratings = $rating->rating;
      }
    }
    $reviewRatings = 0;
    foreach($flyer->review_ratings as $rev_rate){
      if($rev_rate->rating_review !== 'undefined'){
        $reviewRatings += $rev_rate->rating_review;
      }
    }
    $numberOfReview_ratings = count($flyer->review_ratings);
    if($ratings !== 0){
      $numberOfReview_ratings += 1;
    }
    $productRating = 0;
    if($numberOfReview_ratings !== 0){
      $productRating = round(($ratings + $reviewRatings) / $numberOfReview_ratings);
    }
    ?>

    {{-- QUICKVIEW MODAL --}}
    <div id="quickviewModal-{{$flyer->id}}" name="quickviewModal" class="modal">
      <div class="modal-dialog animated quickViewModal">
        <div class="modal-content" id="quickviewModal">
          <form class="form-horizontal" id="quickviewModal" method="GET"
          action="/quickviewModal">
          {{csrf_field()}}
          <div class="modal-header quickviewCloseButton">
            <a id="closeQuickviewModalIcon"><i class="fa fa-close"  aria-hidden="true"></i></a>
          </div>

          <div class="modal-body">
            <input type="hidden" value="{{$flyer->id}}" name="quickViewModalFlyerId" />
            {{-- product image preview --}}
            <div class="col-sm-8 col-lg-8 imagePreview">
              @if(count($flyer->photos) > 0)
              <div class="views-gallery">
                <ul class="slider">
                  @foreach($flyer->photos->chunk(4) as $set)
                  @foreach($set as $photo)
                  <li class="slide">
                    <a class="image" data-easyzoom-source="/{{$photo->path}}" href="/{{$photo->path}}">
                      <img class="" height="90" src="/{{$photo->path}}" width="90"/>
                    </a>
                  </li>
                  @endforeach
                  @endforeach
                </ul>
              </div>
              <div class="Zimage">
                <div>
                  <a class="zoom" href="/{{$flyer->photos[0]->path}}" id="zoom1" itemprop="image">
                    <img class="" src="/{{$flyer->photos[0]->path}}"/>
                  </a>
                </div>
                <div class="clear">
                </div>
              </div>
              @else
              <div class="row no-image">
                <div class="col-sm-2 col-md-2"></div>
                <div class="col-sm-8 col-md-8">
                  <img src="/images/no-image.png"/>
                </div>
                <div class="col-sm-2 col-md-2"></div>
              </div>
              @endif
            </div>
            {{-- end picture preview --}}

            {{-- product propertiews --}}
            <div class="col-sm-4 col-lg-4 productTitle">
              <div class="product" id="product-spec">
                <a href="/{{$flyer->zip}}/{{str_replace(' ', '-', $flyer->street)}}"><h1 class="titleForPencil">{!! $flyer->street !!}</h1></a>
                <div id="productDetails">

                  <div class="col-md-12" id="ratingReviewArea">
                    <div class="ratingReview row">
                      @if($productRating == 5)
                      <input type="radio" class="selectedRatingReview" id="review_star5" name="ratingReview" value="5"/>
                      <label for="review_star5"></label>
                      <input type="radio" id="review_star4" name="ratingReview" value="4"/>
                      <label for="review_star4"></label>
                      <input type="radio" id="review_star3" name="ratingReview" value="3"/>
                      <label for="review_star3"></label>
                      <input type="radio" id="review_star2" name="ratingReview" value="2"/>
                      <label for="review_star2"></label>
                      <input type="radio" id="review_star1" name="ratingReview" value="1"/>
                      <label for="review_star1"></label>

                      @elseif($productRating == 4)
                      <input type="radio" id="review_star5" name="ratingReview" value="5"/>
                      <label for="review_star5"></label>
                      <input type="radio" class="selectedRatingReview" id="review_star4" name="ratingReview" value="4"/>
                      <label for="review_star4"></label>
                      <input type="radio" id="review_star3" name="ratingReview" value="3"/>
                      <label for="review_star3"></label>
                      <input type="radio" id="review_star2" name="ratingReview" value="2"/>
                      <label for="review_star2"></label>
                      <input type="radio" id="review_star1" name="ratingReview" value="1"/>
                      <label for="review_star1"></label>

                      @elseif($productRating == 3)
                      <input type="radio" id="review_star5" name="ratingReview" value="5"/>
                      <label for="review_star5"></label>
                      <input type="radio" id="review_star4" name="ratingReview" value="4"/>
                      <label for="review_star4"></label>
                      <input type="radio" class="selectedRatingReview" id="review_star3" name="ratingReview" value="3"/>
                      <label for="review_star3"></label>
                      <input type="radio" id="review_star2" name="ratingReview" value="2"/>
                      <label for="review_star2"></label>
                      <input type="radio" id="review_star1" name="ratingReview" value="1"/>
                      <label for="review_star1"></label>

                      @elseif($productRating == 2)
                      <input type="radio" id="review_star5" name="ratingReview" value="5"/>
                      <label for="review_star5"></label>
                      <input type="radio" id="review_star4" name="ratingReview" value="4"/>
                      <label for="review_star4"></label>
                      <input type="radio" id="review_star3" name="ratingReview" value="3"/>
                      <label for="review_star3"></label>
                      <input type="radio" class="selectedRatingReview" id="review_star2" name="ratingReview" value="2"/>
                      <label for="review_star2"></label>
                      <input type="radio" id="review_star1" name="ratingReview" value="1"/>
                      <label for="review_star1"></label>

                      @elseif($productRating == 1)
                      <input type="radio" id="review_star5" name="ratingReview" value="5"/>
                      <label for="review_star5"></label>
                      <input type="radio" id="review_star4" name="ratingReview" value="4"/>
                      <label for="review_star4"></label>
                      <input type="radio" id="review_star3" name="ratingReview" value="3"/>
                      <label for="review_star3"></label>
                      <input type="radio" id="review_star2" name="ratingReview" value="2"/>
                      <label for="review_star2"></label>
                      <input type="radio" class="selectedRatingReview" id="review_star1" name="ratingReview" value="1"/>
                      <label for="review_star1"></label>

                      @else
                      <input type="radio" id="review_star5" name="ratingReview" value="5"/>
                      <label for="review_star5"></label>
                      <input type="radio" id="review_star4" name="ratingReview" value="4"/>
                      <label for="review_star4"></label>
                      <input type="radio" id="review_star3" name="ratingReview" value="3"/>
                      <label for="review_star3"></label>
                      <input type="radio" id="review_star2" name="ratingReview" value="2"/>
                      <label for="review_star2"></label>
                      <input type="radio" id="review_star1" name="ratingReview" value="1"/>
                      <label for="review_star1"></label>
                      @endif
                    </div>

                    <div class="numberOfReviewsModal">
                      @if($numberOfReview_ratings > 1)
                      {{ $numberOfReview_ratings }} reviews
                      @else
                      {{ $numberOfReview_ratings }} review
                      @endif
                    </div>

                  </div>


                  <h2>Mint Green</h2>
                  <h3 class="rrp">$150</h3>
                  <h3 class="sale">{{ $flyer->price }}</h3>
                  <div class="row productQtyAllProductPage">
                    <div class="input-group">
                      <span class="input-group-btn">
                          <button type="button" class="btn btn-default btn-number" data-type="minus" data-field="quant[1]">
                              <span class="glyphicon glyphicon-minus"></span>
                          </button>
                      </span>
                      <input type="text" name="quant[1]" class="form-control input-number {{ $flyer->id }}"  value="1">
                      <span class="input-group-btn">
                          <button type="button" class="btn btn-default btn-number" data-type="plus" data-field="quant[1]">
                              <span class="glyphicon glyphicon-plus"></span>
                          </button>
                      </span>
                  </div>
                </div>
                  <div class="row">
                    <div class="col-lg-12">
                      <button class="btn btn-success addToCartButton" onclick="event.preventDefault();" @click="addToCartVueAllFlyers({{ $flyer->id }})">
                        <i class="fa fa-shopping-cart"  aria-hidden="true"></i>&nbsp;&nbsp; Add to Cart
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {{-- end product propertiews --}}
          </div>
          <div class="modal-footer" style="border:none;">
          </div>
        </form>
      </div>
    </div>
  </div>


  <div class="column" id="articles">
    <div class="post-module hove {{ $flyer->id }}">

      {{-- thumbnail --}}
      <div class="thumbnail">
        <a href="/{{$flyer->zip}}/{{str_replace(' ', '-', $flyer->street)}}">
          @if(count($flyer->photos) > 0)
          @foreach($flyer->photos as $photo)
          <img src="/{{$photo->path}}" />
          @endforeach
          @else
          <img src="/images/no-image.png" />
          @endif
        </a>
      </div>
      {{-- end thumbnail --}}

      {{-- post content --}}
      <div class="post-content">
        <div class="row quickviewAndFavoritePanel">

          <div class="row productDetails">
           <h2 class="category">{{ $flyer->productID }}</h2>
           <h1 class="title"><a href="/{{$flyer->zip}}/{{str_replace(' ', '-', $flyer->street)}}">{{ $flyer->street }}</a></h1>

           <div class="row ratingReview" id="ratingReviewArea">
            @if($productRating == 5)
            <input type="radio" class="selectedRatingReview" id="review_star5" name="ratingReview" value="5"/>
            <label for="review_star5"></label>
            <input type="radio" id="review_star4" name="ratingReview" value="4"/>
            <label for="review_star4"></label>
            <input type="radio" id="review_star3" name="ratingReview" value="3"/>
            <label for="review_star3"></label>
            <input type="radio" id="review_star2" name="ratingReview" value="2"/>
            <label for="review_star2"></label>
            <input type="radio" id="review_star1" name="ratingReview" value="1"/>
            <label for="review_star1"></label>

            @elseif($productRating == 4)
            <input type="radio" id="review_star5" name="ratingReview" value="5"/>
            <label for="review_star5"></label>
            <input type="radio" class="selectedRatingReview" id="review_star4" name="ratingReview" value="4"/>
            <label for="review_star4"></label>
            <input type="radio" id="review_star3" name="ratingReview" value="3"/>
            <label for="review_star3"></label>
            <input type="radio" id="review_star2" name="ratingReview" value="2"/>
            <label for="review_star2"></label>
            <input type="radio" id="review_star1" name="ratingReview" value="1"/>
            <label for="review_star1"></label>

            @elseif($productRating == 3)
            <input type="radio" id="review_star5" name="ratingReview" value="5"/>
            <label for="review_star5"></label>
            <input type="radio" id="review_star4" name="ratingReview" value="4"/>
            <label for="review_star4"></label>
            <input type="radio" class="selectedRatingReview" id="review_star3" name="ratingReview" value="3"/>
            <label for="review_star3"></label>
            <input type="radio" id="review_star2" name="ratingReview" value="2"/>
            <label for="review_star2"></label>
            <input type="radio" id="review_star1" name="ratingReview" value="1"/>
            <label for="review_star1"></label>

            @elseif($productRating == 2)
            <input type="radio" id="review_star5" name="ratingReview" value="5"/>
            <label for="review_star5"></label>
            <input type="radio" id="review_star4" name="ratingReview" value="4"/>
            <label for="review_star4"></label>
            <input type="radio" id="review_star3" name="ratingReview" value="3"/>
            <label for="review_star3"></label>
            <input type="radio" class="selectedRatingReview" id="review_star2" name="ratingReview" value="2"/>
            <label for="review_star2"></label>
            <input type="radio" id="review_star1" name="ratingReview" value="1"/>
            <label for="review_star1"></label>

            @elseif($productRating == 1)
            <input type="radio" id="review_star5" name="ratingReview" value="5"/>
            <label for="review_star5"></label>
            <input type="radio" id="review_star4" name="ratingReview" value="4"/>
            <label for="review_star4"></label>
            <input type="radio" id="review_star3" name="ratingReview" value="3"/>
            <label for="review_star3"></label>
            <input type="radio" id="review_star2" name="ratingReview" value="2"/>
            <label for="review_star2"></label>
            <input type="radio" class="selectedRatingReview" id="review_star1" name="ratingReview" value="1"/>
            <label for="review_star1"></label>

            @else
            <input type="radio" id="review_star5" name="ratingReview" value="5"/>
            <label for="review_star5"></label>
            <input type="radio" id="review_star4" name="ratingReview" value="4"/>
            <label for="review_star4"></label>
            <input type="radio" id="review_star3" name="ratingReview" value="3"/>
            <label for="review_star3"></label>
            <input type="radio" id="review_star2" name="ratingReview" value="2"/>
            <label for="review_star2"></label>
            <input type="radio" id="review_star1" name="ratingReview" value="1"/>
            <label for="review_star1"></label>
            @endif
          </div>
          <div class="numberOfReviews">
            @if($numberOfReview_ratings > 1)
            {{ $numberOfReview_ratings }} reviews
            @else
            {{ $numberOfReview_ratings }} review
            @endif
          </div>

          <input type="hidden" id="productIdAllFlyers" value="{{ $flyer->productID }}" />
          <div class="sub_title">{{ $flyer->price }}</div>
          <div class="descript">{{ $flyer->description }}</div>

          <div class="row quickViewAndFavoriteIcons">
            <div class="positioningIcons hideQuickviewAndFavoritePanel">
              <div class="qiuickView {{$flyer->id}}" data-balloon="Quickview" data-balloon-pos="up">
                <i class="fa fa-eye" data-toggle="modal" data-target="#quickviewModal-{{$flyer->id}}"></i>
              </div>
              
              @if(count($favoriteFlyers) > 0)
              @if($favoriteFlyers !== null)
              @foreach($favoriteFlyers as $favId)
              <?php $favTest[] = $favId; ?>
              @endforeach
              @if(in_array($flyer->id, $favTest))
              <div class="favorite-icon-position" data-balloon-pos="up" data-balloon="Remove from Favorite">
                <div class="favorite">
                  <i data-flyer="{{$flyer->id}}" class="favorite-icon isFavorite ion-android-star" style='font-size: 28px;'></i>
                </div>
                @else
                <div class="favorite-icon-position" data-balloon-pos="up" data-balloon="Add To Favorite">
                  <div class="favorite">
                    <i data-flyer="{{$flyer->id}}" class="favorite-icon ion-android-star-outline" style='font-size: 28px;'></i>
                  </div>
                  @endif
                  @endif
                  @else
                  <div class="favorite-icon-position" data-balloon-pos="up" data-balloon="Add To Favorite">
                    <div class="favorite">
                      <i data-flyer="{{$flyer->id}}" class="favorite-icon ion-android-star-outline" style='font-size: 28px;'></i>
                    </div>
                    @endif
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-lg-12 addToCartColumn">
                  <button class="btn btn-success addToCartButton" @click="addToCartVueAllFlyers({{ $flyer->id }})">
                    <i class="fa fa-shopping-cart"  aria-hidden="true"></i>&nbsp;&nbsp; Add to Cart
                  </button>
                </div>
              </div>
            </div>

          </div>
        </div>
        {{-- \end post content --}}

      </div>
    </div>
    @endforeach
  </div>
  @endforeach
  
<div class="showAllFlyersPagination">
  {{$pagination->links()}}
</div>

</div>
</div>
@stop