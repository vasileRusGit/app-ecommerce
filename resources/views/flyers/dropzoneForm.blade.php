@if(empty($flyer))
    <form action="/flyers" method="post" id="addPhotoForm" class="dropzone">
        {{csrf_field()}}
    </form>
    <button type="submit" id="submit-all" class="btn btn-success">Add photos</button>
@else
    <form action="/{{$flyer->zip}}/{{$flyer->street}}/photos" method="post" id="addPhotoForm" class="dropzone">
        {{csrf_field()}}
    </form>
    <button type="submit" id="submit-all" class="btn btn-success">Add photos</button>
@endif