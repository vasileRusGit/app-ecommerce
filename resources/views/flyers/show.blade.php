@extends('layout')

@section('content')
<div class="row individualFlyer">
    {{-- product image preview --}}
    <div class="col-sm-8 col-lg-8 imagePreview">
        @if(count($flyer->photos) > 0)
        <div class="views-gallery">
            <ul class="slider">
                @foreach($flyer->photos->chunk(4) as $set)
                @foreach($set as $photo)
                <li class="slide">
                    <a class="image" data-easyzoom-source="/{{$photo->path}}" href="/{{$photo->path}}">
                        <img class="" height="90" src="/{{$photo->path}}" width="90"/>
                    </a>
                </li>
                @endforeach
                @endforeach
            </ul>
        </div>
        <div class="Zimage">
            <div>
                <a class="zoom" href="/{{$flyer->photos[0]->path}}" id="zoom1" itemprop="image">
                    <img class="" src="/{{$flyer->photos[0]->path}}"/>
                </a>
            </div>
            <div class="clear">
            </div>
        </div>
        @else
        <div class="row no-image">
            <div class="col-sm-2 col-md-2">
            </div>
            <div class="col-sm-8 col-md-8">
                <img src="/images/no-image.png"/>
            </div>
            <div class="col-sm-2 col-md-2">
            </div>
        </div>
        @endif
    </div>
    {{-- end picture preview --}}

    {{-- product propertiews --}}
    <div class="col-sm-4 col-lg-4 productTitle">
        <div class="product" id="product-spec">
            <h1 class="titleForPencil">
                {!! $flyer->street !!}
                @if($isGuest === true)
                <a href="/flyers/{{$flyer->id}}/edit">
                </a>
                @else
                @if(!$isGuest || $isSignedIn)
                @if($user->owns($flyer) || $user->hasRole('admin'))
                <a href="/flyers/{{$flyer->id}}/edit">
                    <i class="fa fa-pencil-square-o editProduct" aria-hidden="true"></i>
                </a>
                @endif
                @endif
                @endif
            </h1>
            <div id="productDetails">
                <span id="ratingNumberFromDatabase" style="display: table-column;">
                    {{$calculateRating}}
                </span>
                <div class="col-md-12" id="ratingArea">
                    <div class="col-md-6">
                        <div class="rating row">
                            <input id="star5" name="rating" type="radio" value="5"/>
                            <label for="star5">
                            </label>
                            <input id="star4" name="rating" type="radio" value="4"/>
                            <label for="star4">
                            </label>
                            <input id="star3" name="rating" type="radio" value="3"/>
                            <label for="star3">
                            </label>
                            <input id="star2" name="rating" type="radio" value="2"/>
                            <label for="star2">
                            </label>
                            <input id="star1" name="rating" type="radio" value="1"/>
                            <label for="star1">
                            </label>
                        </div>
                    </div>
                    <div class="col-md-6" id="numberOfReviews">
                        <span>{{$numberOfUsersThatRateProduct}} reviews</span>
                    </div>
                </div>
                <h2 id="subtitle">
                    Mint Green
                </h2>
                <input id="productId" type="hidden" value="{{ $flyer->productID }}"/>
                <h3 class="rrp">
                    $150
                </h3>
                <h3 class="sale">
                    {!! $flyer->price !!}
                </h3>
                <div class="row productQtyProductPage">
                    <div class="input-group">
                      <span class="input-group-btn">
                          <button type="button" class="btn btn-default btn-number" data-type="minus" data-field="quant[1]">
                              <span class="glyphicon glyphicon-minus"></span>
                          </button>
                      </span>
                      <input type="text" name="quant[1]" class="form-control input-number" value="1">
                      <span class="input-group-btn">
                          <button type="button" class="btn btn-default btn-number" data-type="plus" data-field="quant[1]">
                              <span class="glyphicon glyphicon-plus"></span>
                          </button>
                      </span>
                  </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <button @click="addToCartVue" class="btn btn-success addToCartButton">
                            <i aria-hidden="true" class="fa fa-shopping-cart">
                            </i>
                            &nbsp;Add to Cart
                        </button>
                    </div>
                </div>
                <input type="hidden" id="productIdAllFlyers" value="{{ $flyer->productID }}" />
                <hr style="margin-top:50px;    margin-bottom: 10px;">
                    <div class="row" id="writeReviewPen">
                        <i class="titleEdit">
                        </i>
                        <h2 id="writeReview">
                            write a review
                        </h2>
                    </div>
                    @if(count($favoriteFlyers) > 0)
                @foreach($favoriteFlyers as $favoriteFlyersId)
                @if ($favoriteFlyersId == null)
                    <?php $isFavorite = 'false'; ?>
                    @else
                @if (in_array($flyer->id, $favoriteFlyersId))
                    <?php $isFavorite = 'true'; ?>
                    @else
                    <?php $isFavorite = 'false'; ?>
                    @endif
                @endif
                @endforeach

                @if($isFavorite == 'true')
                    <div class="row favorite">
                        <i class="favorite-icon isFavorite ion-android-star-outline" data-flyer="{{$flyer->id}}">
                            <h2 id="addToWishList">
                                Add to wish list
                            </h2>
                        </i>
                    </div>
                    @else
                    <div class="row favorite addToWishList">
                        <i class="favorite-icon ion-android-star-outline" data-flyer="{{$flyer->id}}">
                            <h2 id="addToWishList">
                                Add to wish list
                            </h2>
                        </i>
                    </div>
                    @endif
                @else
                    <div class="row favorite">
                        <i class="favorite-icon ion-android-star-outline" data-flyer="{{$flyer->id}}">
                            <h2 id="addToWishList">
                                Add to wish list
                            </h2>
                        </i>
                    </div>
                    @endif
                </hr>
            </div>
        </div>
    </div>

{{-- end product propertiews --}}

{{-- ADD REVIEW TO FLYER MODAL --}}
<div class="modal" id="addReviewModal">
    <div class="modal-dialog animated">
        <div class="modal-content">
            <form action="/addReviewToFlyerModal" class="form-horizontal" id="addReviewModalForm" method="get">
                {{csrf_field()}}
                <div class="modal-header">
                    <strong>
                        Write your review below
                    </strong>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <input id="flyerIdShowPage" name="flyerIdShowPage" style="display:none" value="{{$flyer->id}}"/>
                        <textarea class="form-control" id="reviewTextarea" name="reviewTextarea" type="text">
                        </textarea>
                    </div>
                    <div class="col-md-12" id="ratingAreaModal">
                        <div class="ratingReviewModal row">
                            <input id="ratingReviewModalStar5" name="ratingReviewModal" type="radio" value="5"/>
                            <label for="ratingReviewModalStar5">
                            </label>
                            <input id="ratingReviewModalStar4" name="ratingReviewModal" type="radio" value="4"/>
                            <label for="ratingReviewModalStar4">
                            </label>
                            <input id="ratingReviewModalStar3" name="ratingReviewModal" type="radio" value="3"/>
                            <label for="ratingReviewModalStar3">
                            </label>
                            <input id="ratingReviewModalStar2" name="ratingReviewModal" type="radio" value="2"/>
                            <label for="ratingReviewModalStar2">
                            </label>
                            <input id="ratingReviewModalStar1" name="ratingReviewModal" type="radio" value="1"/>
                            <label for="ratingReviewModalStar1">
                            </label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" onclick="addReviewModal.close();" type="button">
                        Cancel
                    </button>
                    <button class="btn btn-primary" id="addReviewModalButton" onclick="addReviewModal.close();" type="submit">
                        Save
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

{{-- end write review flyer --}}


{{-- product description --}}
<hr>
    <div class="row description-area">
        <h4 class="clearfix">
            Description
        </h4>
        <p>
            With a highly breathable upper and casual silouette, the Nike Roshe Run is definately a perfect model for
        summer. For Spring /Summer 2013 a fresh Mint Green colorway's version of this 'yet classic' pair is now
        available to order.
        </p>
    </div>
    {{-- end product description --}}


    {{-- similar flyers --}}
    <div class="row similar-flyers">
        <h4 id="similar-flyers-heeading">
            Similar Products
        </h4>
        <div class="container-fluid-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="carousel slide" id="Carousel">
                        <ol class="carousel-indicators" id="similarFlyersIndicators">
                            <li class="active" data-slide-to="0" data-target="#Carousel">
                            </li>
                            @if(count($flyerPhotos) > 4)
                            <li data-slide-to="1" data-target="#Carousel">
                            </li>
                            @endif
                            @if(count($flyerPhotos) > 8)
                            <li data-slide-to="2" data-target="#Carousel">
                            </li>
                            @endif
                        </ol>
                        <?php $numberOfSimilarProperties = $flyerPhotos; ?>
                        {{--// convert object in array--}}
                        @foreach($similarFlyerId as $similarFlyer)
                        <?php $similarFlyers[] = $similarFlyer; ?>
                        @endforeach
                        <!-- Carousel items -->
                        <div class="carousel-inner">
                            @if(count($numberOfSimilarProperties) > 0)
                            <?php $count = 1; ?>
                            <div class="item active">
                                <div class="row">
                                    @foreach($flyerPhotos as $flyerPhoto)
                                    @if($count == 5) @break; @endif
                                    <?php $count++; $randomNumber = rand(0, count($flyerPhotos) - 1);?>
                                    <div class="col-md-3" id="similarFlyers">
                                        <div class="post-module hove">
                                            <div class="thumbnail">
                                                @if(is_null($flyerPhotos[$randomNumber]))
                                                <a href="/{{$similarFlyers[$randomNumber]->zip}}/{{str_replace(' ', '-', $similarFlyers[$randomNumber]->street)}}">
                                                    <img alt="sample90" src="/images/no-image.png"/>
                                                </a>
                                                @else
                                                <a href="/{{$similarFlyers[$randomNumber]->zip}}/{{str_replace(' ', '-', $similarFlyers[$randomNumber]->street)}}">
                                                    <img src="/images/photos/{{$flyerPhotos[$randomNumber]->name}}"/>
                                                </a>
                                                @endif
                                            </div>
                                            <div class="post-content">
                                                <h2 class="category">
                                                    {!! $similarFlyers[$randomNumber]->productID !!}
                                                </h2>
                                                <h1 class="title">
                                                    <a href="/{{$similarFlyers[$randomNumber]->zip}}/{{$similarFlyers[$randomNumber]->street}}">
                                                        {{ $similarFlyers[$randomNumber]->street }}
                                                    </a>
                                                </h1>
                                                <div class="sub_title">
                                                    ${{ $similarFlyers[$randomNumber]->price }}
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <button @click="addToCartVueAllFlyers({{ $flyer->id }})" class="btn btn-success addToCartButton" onclick="event.preventDefault();">
                                                            <i aria-hidden="true" class="fa fa-shopping-cart">
                                                            </i>
                                                            Add to Cart
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php unset($similarFlyers[$randomNumber]); $similarFlyers = array_values($similarFlyers); ?>
                                    <?php unset($flyerPhotos[$randomNumber]); $flyerPhotos = array_values($flyerPhotos); ?>
                                    @endforeach
                                </div>
                            </div>
                            @endif

                            @if(count($numberOfSimilarProperties) > 4)
                            <?php $count = 1; ?>
                            <div class="item">
                                <div class="row">
                                    @foreach($flyerPhotos as $flyerPhoto)
                                    @if($count == 5) @break; @endif
                                    <?php $count++; $randomNumber = rand(0, count($flyerPhotos) - 1); ?>
                                    <div class="col-md-3" id="similarFlyers">
                                        <div class="post-module hove">
                                            <div class="thumbnail">
                                                @if(is_null($flyerPhotos[$randomNumber]))
                                                <a href="/{{$similarFlyers[$randomNumber]->zip}}/{{str_replace(' ', '-', $similarFlyers[$randomNumber]->street)}}">
                                                    <img alt="sample90" src="/images/no-image.png"/>
                                                </a>
                                                @else
                                                <a href="/{{$similarFlyers[$randomNumber]->zip}}/{{str_replace(' ', '-', $similarFlyers[$randomNumber]->street)}}">
                                                    <img src="/images/photos/{{$flyerPhotos[$randomNumber]->name}}"/>
                                                </a>
                                                @endif
                                            </div>
                                            <div class="post-content">
                                                <h2 class="category">
                                                    {!! $similarFlyers[$randomNumber]->productID !!}
                                                </h2>
                                                <h1 class="title">
                                                    <a href="/{{$similarFlyers[$randomNumber]->zip}}/{{$similarFlyers[$randomNumber]->street}}">
                                                        {{ $similarFlyers[$randomNumber]->street }}
                                                    </a>
                                                </h1>
                                                <div class="sub_title">
                                                    {{ $similarFlyers[$randomNumber]->price }}
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <button @click="addToCartVueAllFlyers({{ $flyer->id }})" class="btn btn-success addToCartButton" onclick="event.preventDefault();">
                                                            <i aria-hidden="true" class="fa fa-shopping-cart">
                                                            </i>
                                                            Add to Cart
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php unset($similarFlyers[$randomNumber]); $similarFlyers = array_values($similarFlyers); ?>
                                    <?php unset($flyerPhotos[$randomNumber]); $flyerPhotos = array_values($flyerPhotos); ?>
                                    @endforeach
                                </div>
                            </div>
                            @endif

                            @if(count($numberOfSimilarProperties) > 8)
                            <?php $count = 1; ?>
                            <div class="item">
                                <div class="row">
                                    @foreach($flyerPhotos as $flyerPhoto)
                                    @if($count == 5) @break; @endif
                                    <?php $count++; $randomNumber = rand(0, count($flyerPhotos) - 1); ?>
                                    <div class="col-md-3" id="similarFlyers">
                                        <div class="post-module hove">
                                            <div class="thumbnail">
                                                @if(is_null($flyerPhotos[$randomNumber]))
                                                <a href="/{{$similarFlyers[$randomNumber]->zip}}/{{str_replace(' ', '-', $similarFlyers[$randomNumber]->street)}}">
                                                    <img alt="sample90" src="/images/no-image.png"/>
                                                </a>
                                                @else
                                                <a href="/{{$similarFlyers[$randomNumber]->zip}}/{{str_replace(' ', '-', $similarFlyers[$randomNumber]->street)}}">
                                                    <img src="/images/photos/{{$flyerPhotos[$randomNumber]->name}}"/>
                                                </a>
                                                @endif
                                            </div>
                                            <div class="post-content">
                                                <h2 class="category">
                                                    {!! $similarFlyers[$randomNumber]->productID !!}
                                                </h2>
                                                <h1 class="title">
                                                    <a href="/{{$similarFlyers[$randomNumber]->zip}}/{{$similarFlyers[$randomNumber]->street}}">
                                                        {{ $similarFlyers[$randomNumber]->street }}
                                                    </a>
                                                </h1>
                                                <div class="sub_title">
                                                    {{ $similarFlyers[$randomNumber]->price }}
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <button @click="addToCartVueAllFlyers({{ $flyer->id }})" class="btn btn-success addToCartButton" onclick="event.preventDefault();">
                                                            <i aria-hidden="true" class="fa fa-shopping-cart">
                                                            </i>
                                                            Add to Cart
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php unset($similarFlyers[$randomNumber]); $similarFlyers = array_values($similarFlyers); ?>
                                    <?php unset($flyerPhotos[$randomNumber]); $flyerPhotos = array_values($flyerPhotos); ?>
                                    @endforeach
                                </div>
                            </div>
                            @endif
                        </div>
                        <a class="left carousel-control" data-slide="prev" href="#Carousel">
                            ‹
                        </a>
                        <a class="right carousel-control" data-slide="next" href="#Carousel">
                            ›
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- end similar flyers --}}

    {{-- REVIEW FLYER AREA --}}
    <div class="row reviewFlyers">
        @if(count($reviewsAtachetToFlyer) > 0)
        <h4 id="flyer-review-heeading">
            Product Reviews
        </h4>
        @endif
        @foreach($reviewsAtachetToFlyer as $review)

        {{-- EDIT REVIEW TO FLYER MODAL --}}
        <div class="modal" id="editReviewModal-{{$review->id}}" name="editReviewModal">
            <div class="modal-dialog animated editReviewModal">
                <div class="modal-content">
                    <form action="/editReviewToFlyerModal" class="form-horizontal" id="editReviewModalForm" method="GET">
                        {{csrf_field()}}
                        <div class="modal-header">
                            <strong>
                                Edit your review below
                            </strong>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <input id="flyerIdShowPageForEditReview" name="flyerIdShowPageForEditReview" style="display:none" value="{{$flyer->id}}"/>
                                <input id="editReviewId" name="editReviewId" style="display:none" value="{{$review->id}}"/>
                                <textarea class="form-control" id="reviewTextarea" name="editReviewTextarea" type="text">{{ trim($review->review)}}</textarea>
                            </div>
                            <div class="col-md-12" id="ratingAreaModal">
                                <div class="ratingEditReviewModal {{$review->id}} row" id="review-{{$review->id}}">
                                    @if($review->rating_review == 5)
                                    <input class="selectedRatingReview" id="edit_review_star5" name="editRatingReview" type="radio" value="5"/>
                                    <label for="review_star5">
                                    </label>
                                    <input id="edit_review_star4" name="editRatingReview" type="radio" value="4"/>
                                    <label for="review_star4">
                                    </label>
                                    <input id="edit_review_star3" name="editRatingReview" type="radio" value="3"/>
                                    <label for="review_star3">
                                    </label>
                                    <input id="edit_review_star2" name="editRatingReview" type="radio" value="2"/>
                                    <label for="review_star2">
                                    </label>
                                    <input id="edit_review_star1" name="editRatingReview" type="radio" value="1"/>
                                    <label for="review_star1">
                                    </label>
                                    @elseif($review->rating_review == 4)
                                    <input id="edit_review_star5" name="editRatingReview" type="radio" value="5"/>
                                    <label for="review_star5">
                                    </label>
                                    <input class="selectedRatingReview" id="edit_review_star4" name="editRatingReview" type="radio" value="4"/>
                                    <label for="review_star4">
                                    </label>
                                    <input id="edit_review_star3" name="editRatingReview" type="radio" value="3"/>
                                    <label for="review_star3">
                                    </label>
                                    <input id="edit_review_star2" name="editRatingReview" type="radio" value="2"/>
                                    <label for="review_star2">
                                    </label>
                                    <input id="edit_review_star1" name="editRatingReview" type="radio" value="1"/>
                                    <label for="review_star1">
                                    </label>
                                    @elseif($review->rating_review == 3)
                                    <input id="edit_review_star5" name="editRatingReview" type="radio" value="5"/>
                                    <label for="review_star5">
                                    </label>
                                    <input id="edit_review_star4" name="ratingReview" type="radio" value="4"/>
                                    <label for="review_star4">
                                    </label>
                                    <input class="selectedRatingReview" id="edit_review_star3" name="editRatingReview" type="radio" value="3"/>
                                    <label for="review_star3">
                                    </label>
                                    <input id="edit_review_star2" name="editRatingReview" type="radio" value="2"/>
                                    <label for="review_star2">
                                    </label>
                                    <input id="edit_review_star1" name="editRatingReview" type="radio" value="1"/>
                                    <label for="review_star1">
                                    </label>
                                    @elseif($review->rating_review == 2)
                                    <input id="edit_review_star5" name="editRatingReview" type="radio" value="5"/>
                                    <label for="review_star5">
                                    </label>
                                    <input id="edit_review_star4" name="editRatingReview" type="radio" value="4"/>
                                    <label for="review_star4">
                                    </label>
                                    <input id="edit_review_star3" name="editRatingReview" type="radio" value="3"/>
                                    <label for="review_star3">
                                    </label>
                                    <input class="selectedRatingReview" id="edit_review_star2" name="editRatingReview" type="radio" value="2"/>
                                    <label for="review_star2">
                                    </label>
                                    <input id="edit_review_star1" name="editRatingReview" type="radio" value="1"/>
                                    <label for="review_star1">
                                    </label>
                                    @elseif($review->rating_review == 1)
                                    <input id="edit_review_star5" name="editRatingReview" type="radio" value="5"/>
                                    <label for="review_star5">
                                    </label>
                                    <input id="edit_review_star4" name="editRatingReview" type="radio" value="4"/>
                                    <label for="review_star4">
                                    </label>
                                    <input id="edit_review_star3" name="editRatingReview" type="radio" value="3"/>
                                    <label for="review_star3">
                                    </label>
                                    <input id="edit_review_star2" name="editRatingReview" type="radio" value="2"/>
                                    <label for="review_star2">
                                    </label>
                                    <input class="selectedRatingReview" id="edit_review_star1" name="editRatingReview" type="radio" value="1"/>
                                    <label for="review_star1">
                                    </label>
                                    @else
                                    <input id="edit_review_star5" name="ratingReview" type="radio" value="5"/>
                                    <label for="review_star5">
                                    </label>
                                    <input id="edit_review_star4" name="ratingReview" type="radio" value="4"/>
                                    <label for="review_star4">
                                    </label>
                                    <input id="edit_review_star3" name="ratingReview" type="radio" value="3"/>
                                    <label for="review_star3">
                                    </label>
                                    <input id="edit_review_star2" name="ratingReview" type="radio" value="2"/>
                                    <label for="review_star2">
                                    </label>
                                    <input id="edit_review_star1" name="ratingReview" type="radio" value="1"/>
                                    <label for="review_star1">
                                    </label>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-default" type="button">
                                Cancel
                            </button>
                            <button class="btn btn-primary {{$review->id}}" id="editReviewModalButton" type="submit">
                                Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- end edit review modal --}}


{{-- REPLY REVIEW TO FLYER MODAL --}}
    <div class="modal" id="replyReviewModal-{{$review->id}}" name="replyReviewModal">
        <div class="modal-dialog animated replyToReviewModal">
            <div class="modal-content">
                <form action="/replyReviewToFlyerModal" class="form-horizontal" id="replyReviewModalForm" method="GET">
                    {{csrf_field()}}
                    <div class="modal-header">
                        <strong>
                            Reply to review below
                        </strong>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <input id="replyReviewId" name="replyReviewId" style="display:none" value="{{$review->id}}"/>
                            <textarea class="form-control" id="reviewTextarea" name="replyReviewTextarea" type="text"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-default" type="button">
                            Cancel
                        </button>
                        <button class="btn btn-primary {{$review->id}}" id="replyReviewModalButton" type="submit">
                            Save
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- end reply review --}}


{{-- SHOW REVIEWS AREA  --}}
    <div class="row reviewArea">
        <div class="client">
            <span class="image-container">
                @if($review->image_path == null)
                <img height="80" src="/images/no-user-image.png" width="80">
                    @else
                    <img height="80" src="/{{$review->image_path}}" width="80">
                        @endif
                    </img>
                </img>
            </span>
            <div class="row ratingNameAndDate">
                <div class="col-md-12">
                    <div class="col-md-9">
                        <h2 class="heading">
                            {{$review->name}}
                        </h2>
                    </div>
                    <div class="col-md-3">
                        @if(Auth::check())
                        <span id="reviewDate">
                            {{Carbon\Carbon::parse($review->created_at)->format('d-m-Y')}}
                        </span>
                        @if(($user->id == $review->user_id) || ($user->hasRole('admin')))
                        <a href="#">
                            <i aria-hidden="true" class="fa fa-pencil-square-o {{$review->id}}" data-target="#editReviewModal-{{$review->id}}" data-toggle="modal" id="editReviewPen" name="reviewnumber">
                            </i>
                        </a>
                        @endif
                    @else
                        <span id="reviewDate">
                            {{Carbon\Carbon::parse($review->created_at)->format('d-m-Y')}}
                        </span>
                        @endif
                        <div class="col-md-12" id="ratingReviewArea">
                            <div class="ratingReview reviewIds-{{$review->id}} row">
                                @if($review->rating_review == 5)
                                <input class="selectedRatingReview" id="review_star5" name="ratingReview" type="radio" value="5"/>
                                <label for="review_star5">
                                </label>
                                <input id="review_star4" name="ratingReview" type="radio" value="4"/>
                                <label for="review_star4">
                                </label>
                                <input id="review_star3" name="ratingReview" type="radio" value="3"/>
                                <label for="review_star3">
                                </label>
                                <input id="review_star2" name="ratingReview" type="radio" value="2"/>
                                <label for="review_star2">
                                </label>
                                <input id="review_star1" name="ratingReview" type="radio" value="1"/>
                                <label for="review_star1">
                                </label>
                                @elseif($review->rating_review == 4)
                                <input id="review_star5" name="ratingReview" type="radio" value="5"/>
                                <label for="review_star5">
                                </label>
                                <input class="selectedRatingReview" id="review_star4" name="ratingReview" type="radio" value="4"/>
                                <label for="review_star4">
                                </label>
                                <input id="review_star3" name="ratingReview" type="radio" value="3"/>
                                <label for="review_star3">
                                </label>
                                <input id="review_star2" name="ratingReview" type="radio" value="2"/>
                                <label for="review_star2">
                                </label>
                                <input id="review_star1" name="ratingReview" type="radio" value="1"/>
                                <label for="review_star1">
                                </label>
                                @elseif($review->rating_review == 3)
                                <input id="review_star5" name="ratingReview" type="radio" value="5"/>
                                <label for="review_star5">
                                </label>
                                <input id="review_star4" name="ratingReview" type="radio" value="4"/>
                                <label for="review_star4">
                                </label>
                                <input class="selectedRatingReview" id="review_star3" name="ratingReview" type="radio" value="3"/>
                                <label for="review_star3">
                                </label>
                                <input id="review_star2" name="ratingReview" type="radio" value="2"/>
                                <label for="review_star2">
                                </label>
                                <input id="review_star1" name="ratingReview" type="radio" value="1"/>
                                <label for="review_star1">
                                </label>
                                @elseif($review->rating_review == 2)
                                <input id="review_star5" name="ratingReview" type="radio" value="5"/>
                                <label for="review_star5">
                                </label>
                                <input id="review_star4" name="ratingReview" type="radio" value="4"/>
                                <label for="review_star4">
                                </label>
                                <input id="review_star3" name="ratingReview" type="radio" value="3"/>
                                <label for="review_star3">
                                </label>
                                <input class="selectedRatingReview" id="review_star2" name="ratingReview" type="radio" value="2"/>
                                <label for="review_star2">
                                </label>
                                <input id="review_star1" name="ratingReview" type="radio" value="1"/>
                                <label for="review_star1">
                                </label>
                                @elseif($review->rating_review == 1)
                                <input id="review_star5" name="ratingReview" type="radio" value="5"/>
                                <label for="review_star5">
                                </label>
                                <input id="review_star4" name="ratingReview" type="radio" value="4"/>
                                <label for="review_star4">
                                </label>
                                <input id="review_star3" name="ratingReview" type="radio" value="3"/>
                                <label for="review_star3">
                                </label>
                                <input id="review_star2" name="ratingReview" type="radio" value="2"/>
                                <label for="review_star2">
                                </label>
                                <input class="selectedRatingReview" id="review_star1" name="ratingReview" type="radio" value="1"/>
                                <label for="review_star1">
                                </label>
                                @else
                                <input id="review_star5" name="ratingReview" type="radio" value="5"/>
                                <label for="review_star5">
                                </label>
                                <input id="review_star4" name="ratingReview" type="radio" value="4"/>
                                <label for="review_star4">
                                </label>
                                <input id="review_star3" name="ratingReview" type="radio" value="3"/>
                                <label for="review_star3">
                                </label>
                                <input id="review_star2" name="ratingReview" type="radio" value="2"/>
                                <label for="review_star2">
                                </label>
                                <input id="review_star1" name="ratingReview" type="radio" value="1"/>
                                <label for="review_star1">
                                </label>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <p class="info">
                {{$review->review}}
            </p>
        </div>
        <div class="replyReview">
            @if(Auth::check())
            <a data-target="#replyReviewModal-{{$review->id}}" data-toggle="modal" href="#" id="replyToReview">
                <i aria-hidden="true" class="fa fa-reply {{$review->id}}">
                </i>
                reply
            </a>
            @endif
        </div>
        {{-- REPLY REVIEW AREA --}}
    @foreach($replyToReviews as $replyReview)
    {{-- EDIT REPLY TO REVIEW TO FLYER MODAL --}}
        <div class="modal" id="editReplyReviewModal-{{$replyReview->id}}" name="editReplyReviewToFlyerModal">
            <div class="modal-dialog animated editReplyToReviewModal">
                <div class="modal-content">
                    <form action="/editReplyReviewToFlyerModal" class="form-horizontal" id="editReplyReviewModalForm" method="GET">
                        {{csrf_field()}}
                        <div class="modal-header">
                            <strong>
                                Edit your reply to this review below
                            </strong>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <input id="editReplyReviewId" name="editReplyReviewId" style="display:none" value="{{$replyReview->id}}"/>
                                <input id="editReplyUserId" name="editReplyUserId" style="display:none" value="{{$replyReview->user_id}}"/>
                                <textarea class="form-control" id="reviewTextarea" name="editReplyReviewTextarea" type="text">{{ trim($replyReview->reply_review) }}</textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-default" type="button">
                                Cancel
                            </button>
                            <button class="btn btn-primary" id="editReviewModalButton" type="submit">
                                Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        {{-- end edit reply to review modal --}}

    @if($replyReview->review_id == $review->id)
        <div class="reviewArea" id="replyReviewArea" style="width: 90%; margin-left: 10%">
            <div class="client">
                <span class="image-container">
                    @if($replyReview->image_path == null)
                    <img height="80" src="/images/no-user-image.png" width="80">
                        @else
                        <img height="80" src="/{{$replyReview->image_path}}" width="80">
                            @endif
                        </img>
                    </img>
                </span>
                <div class="row ratingNameAndDate">
                    <div class="col-md-12">
                        <div class="col-md-8">
                            <h2 class="heading">
                                {{$replyReview->name}}
                            </h2>
                        </div>
                        <div class="col-md-4">
                            @if(Auth::check())
                            <span id="replyReviewDate">
                                {{Carbon\Carbon::parse($replyReview->created_at)->format('d-m-Y')}}
                            </span>
                            @if(($user->id == $replyReview->user_id) || ($user->hasRole('admin')))
                            <a href="#">
                                <i aria-hidden="true" class="fa fa-pencil-square-o {{$replyReview->id}}" data-target="#editReplyReviewModal-{{$replyReview->id}}" data-toggle="modal" id="editReplayReviewPen" name="reviewnumber">
                                </i>
                            </a>
                            @endif
                        @else
                            <span id="reviewDate">
                                {{Carbon\Carbon::parse($replyReview->created_at)->format('d-m-Y')}}
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <p class="info">
                    {{$replyReview->reply_review}}
                </p>
            </div>
        </div>
        @endif
    @endforeach
    @endforeach
    </div>
</div>
    {{-- end review area --}}



@stop


@section('pagescriptModal')
    <script src="/js/rmodal.js">
    </script>
    <script src="/js/custom/customModal.js">
    </script>
    @stop
</hr>