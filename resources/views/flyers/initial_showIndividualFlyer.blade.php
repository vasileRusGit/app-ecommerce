@extends('layout')

@section('content')
<div class="row individualFlyer">
    <div class="col-md-12">
        <div class="col-md-8 gallery_images productImageCss" id="productImage">
            @if(count($flyer->photos) > 0)
            {{-- PRODUCT PREVIEW SLIDER --}}
            <div id="jssor_1" class="jssor_1_container">
                <!-- Loading Screen -->
                <div data-u="loading"
                style="position:absolute;top:0px;left:0px;background-color:rgba(0,0,0,0.7);">
                <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
                <div style="position:absolute;display:block;background:url('/images/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
            </div>
            <div id="imageContainer" data-u="slides">

                @foreach($flyer->photos->chunk(4) as $set)
                @foreach($set as $photo)
                <div>
                    <img id="jssorSliderImage" data-u="image" src="/{{$photo->path}}"/>
                    <img data-u="thumb" src="/{{$photo->path}}"/>
                </div>
                @endforeach
                @endforeach

            </div>
            <!-- Thumbnail Navigator -->
            <div data-u="thumbnavigator" class="jssort01" data-autocenter="1">
                <!-- Thumbnail Item Skin Begin -->
                <div data-u="slides" style="cursor: default;">
                    <div data-u="prototype" class="p">
                        <div class="w">
                            <div data-u="thumbnailtemplate" class="t"></div>
                        </div>
                        <div class="c"></div>
                    </div>
                </div>
                <!-- Thumbnail Item Skin End -->
            </div>
            <!-- Arrow Navigator -->
            <span data-u="arrowleft" class="jssora05l"></span>
            <span data-u="arrowright" class="jssora05r"></span>
        </div>
        @else
        <img src="/images/no-image.png"/>
        @endif
    </div>


    {{-- PRODUCT PREVIEW DETAILS --}}
    <div class="col-md-4" id="productName">
        <div id="product-spec" class="wrapper" class="product">
            <h1 class="titleForPencil">
                {!! $flyer->street !!}

                @if($isGuest === true)
                <a href="/flyers/{{$flyer->id}}/edit"></a>
                @else
                @if(!$isGuest || $isSignedIn)
                @if($user->owns($flyer) || $user->hasRole('admin'))
                <a href="/flyers/{{$flyer->id}}/edit"><i class="titleEdit"></i></a>
                @endif
                @endif
                @endif
            </h1>
            <div id="productDetails">
                <span id="ratingNumberFromDatabase"
                style="display: table-column;">{{$ratingNumberFromDatabase}}</span>

                <div class="col-md-12" id="ratingArea">
                    <div class="col-md-6">
                        <div class="rating row">
                            <input type="radio" id="star5" name="rating" value="5"/><label for="star5"></label>
                            <input type="radio" id="star4" name="rating" value="4"/><label for="star4"></label>
                            <input type="radio" id="star3" name="rating" value="3"/><label for="star3"></label>
                            <input type="radio" id="star2" name="rating" value="2"/><label for="star2"></label>
                            <input type="radio" id="star1" name="rating" value="1"/><label for="star1"></label>
                        </div>
                    </div>
                    <div class="col-md-6" id="numberOfReviews">
                        <span>{{$numberOfUsersThatRateProduct}} review</span>
                    </div>
                </div>


                <h2>Mint Green</h2>
                <h3 class="rrp">150</h3>
                <h3 class="sale">114.99</h3>
                <div class="buttonAddToCart">
                    <a class="addtocart">
                     <div class="add"><i class="fa fa-shopping-cart shippingCart"  aria-hidden="true"></i>&nbsp&nbspAdd to Cart</div>
                 </a>
             </div>

             <hr style="margin-top:50px;    margin-bottom: 10px;">
             <div class="row" id="writeReviewPen">
                <i class="titleEdit"></i>
                <h2 id="writeReview">write a review</h2>
            </div>

            @if(count($favoriteFlyers) > 0)
            @foreach($favoriteFlyers as $favoriteFlyersId)
            @if ($favoriteFlyersId == null)
            <?php $isFavorite = 'false'; ?>
            @else
            @if (in_array($flyer->id, $favoriteFlyersId))
            <?php $isFavorite = 'true'; ?>
            @else
            <?php $isFavorite = 'false'; ?>
            @endif
            @endif
            @endforeach

            @if($isFavorite == 'true')
            <div class="row favorite">
                <i data-flyer="{{$flyer->id}}"
                   class="favorite-icon isFavorite ion-android-star-outline">
                   <h2 id="addToWishList">Add to wish list</h2></i>
               </div>
               @else
               <div class="row favorite"><i data-flyer="{{$flyer->id}}"
                 class="favorite-icon ion-android-star-outline"><h2
                 id="addToWishList">Add to wish list</h2></i>
             </div>
             @endif
             @else
             <div class="row favorite"><i data-flyer="{{$flyer->id}}"
             class="favorite-icon ion-android-star-outline"><h2 id="addToWishList">
                 Add to wish list</h2></i>
             </div>
             @endif
         </div>
     </div>

 </div>
</div>
</div>

{{-- ADD REVIEW TO FLYER MODAL --}}
<div id="addReviewModal" class="modal">
    <div class="modal-dialog animated">
        <div class="modal-content">
            <form class="form-horizontal" id="addReviewModalForm" method="get" action="/addReviewToFlyerModal">
                {{csrf_field()}}
                <div class="modal-header">
                    <strong>Write your review below</strong>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <input style="display:none" id="flyerIdShowPage" name="flyerIdShowPage"
                        value="{{$flyer->id}}"/>
                        <textarea type="text" name="reviewTextarea" id="reviewTextarea"
                        class="form-control"></textarea>
                    </div>
                    <div class="col-md-12" id="ratingAreaModal">
                        <div class="ratingReviewModal row">
                            <input type="radio" id="ratingReviewModalStar5" name="ratingReviewModal"
                            value="5"/><label for="ratingReviewModalStar5"></label>
                            <input type="radio" id="ratingReviewModalStar4" name="ratingReviewModal"
                            value="4"/><label for="ratingReviewModalStar4"></label>
                            <input type="radio" id="ratingReviewModalStar3" name="ratingReviewModal"
                            value="3"/><label for="ratingReviewModalStar3"></label>
                            <input type="radio" id="ratingReviewModalStar2" name="ratingReviewModal"
                            value="2"/><label for="ratingReviewModalStar2"></label>
                            <input type="radio" id="ratingReviewModalStar1" name="ratingReviewModal"
                            value="1"/><label for="ratingReviewModalStar1"></label>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-default" type="button" onclick="addReviewModal.close();">Cancel</button>
                    <button class="btn btn-primary" id="addReviewModalButton" type="submit"
                    onclick="addReviewModal.close();">Save
                </button>
            </div>
        </form>
    </div>
</div>
</div>


<div class="row description-area">
    <h4 class="clearfix">Description</h4>
    <p>With a highly breathable upper and casual silouette, the Nike Roshe Run is definately a perfect model for
        summer. For Spring /Summer 2013 a fresh Mint Green colorway's version of this 'yet classic' pair is now
        available to order.</p>
    </div>

    {{-- SIMILAR FLYERS --}}
    <div class="row similar-flyers">
        <h4 id="similar-flyers-heeading" style="margin-top: 10px;">Similar FLyers</h4>
        <div class="container-fluid">
            <div class="row">

                {{-- SIMILAR FLYERS AREA --}}
                <div class="col-md-12">
                    <div id="Carousel" class="carousel slide">
                        <ol class="carousel-indicators" id="similarFlyersIndicators">
                            <li data-target="#Carousel" data-slide-to="0" class="active"></li>
                            @if(count($flyerPhotos) > 4)
                            <li data-target="#Carousel" data-slide-to="1"></li>
                            @endif
                            @if(count($flyerPhotos) > 8)
                            <li data-target="#Carousel" data-slide-to="2"></li>
                            @endif
                        </ol>
                        <?php $numberOfSimilarProperties = $flyerPhotos; ?>

                        {{--// convert object in array--}}
                        @foreach($similarFlyerId as $similarFlyer)
                        <?php $similarFlyerers[] = $similarFlyer; ?>
                        @endforeach

                        <!-- Carousel items -->
                        <div class="carousel-inner">
                            @if(count($numberOfSimilarProperties) > 0)
                            <?php
                            $count = 1;
                            ?>
                            <div class="item active">
                                <div class="row">
                                    @foreach($flyerPhotos as $flyerPhoto)
                                    @if($count == 5)
                                    @break;
                                    @endif
                                    <?php
                                    $count++;
                                    $randomNumber = rand(0, count($flyerPhotos) - 1);
                                    ?>

                                    <div class="col-md-3" id="similarFlyers">
                                        <div class="post-module hove">
                                            <div class="thumbnail">
                                                @if(is_null($flyerPhotos[$randomNumber]))
                                                <a href="/{{$similarFlyerers[$randomNumber]->zip}}/{{str_replace(' ', '-', $similarFlyerers[$randomNumber]->street)}}"><img
                                                    src="/images/no-image.png" alt="sample90"/></a>
                                                    @else
                                                    <a href="/{{$similarFlyerers[$randomNumber]->zip}}/{{str_replace(' ', '-', $similarFlyerers[$randomNumber]->street)}}"><img
                                                        src="/images/photos/{{$flyerPhotos[$randomNumber]->name}}"
                                                        alt="sample90"/></a>
                                                        @endif
                                                        <div class="date">
                                                            <span class="day">{{ $flyer->created_at->format('d') }}</span>
                                                            <span class="month">{{ $flyer->created_at->format('M') }}</span>
                                                        </div>
                                                    </div>
                                                    <div class="post-content">
                                                        <h2 class="category">{!! $similarFlyerers[$randomNumber]->productID !!}</h2>

                                                        <h1 class="title"><a
                                                            href="/{{$similarFlyerers[$randomNumber]->zip}}/{{$similarFlyerers[$randomNumber]->street}}">{{ $similarFlyerers[$randomNumber]->street }}</a>
                                                        </h1>
                                                        <div class="sub_title">{{ $similarFlyerers[$randomNumber]->price }}</div>
                                                        <div class="descript">{{ $similarFlyerers[$randomNumber]->description }}</div>
                                                        <div class="col-md-12">
                                                            <div class="row" style="margin-top: 20px;">
                                                                <div class="addToCartSimilarFlyers">
                                                                    <a class="addtocart">
                                                                        <div class="add"><i class="fa fa-shopping-cart shippingCart"  aria-hidden="true"></i>&nbsp&nbspAdd to Cart</div>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <?php
                                            unset($similarFlyerers[$randomNumber]);
                                            $similarFlyerers = array_values($similarFlyerers);
                                            ?>
                                            <?php
                                            unset($flyerPhotos[$randomNumber]);
                                            $flyerPhotos = array_values($flyerPhotos);
                                            ?>
                                            @endforeach
                                        </div><!--.row-->
                                    </div><!--.item-->
                                    @endif


                                    @if(count($numberOfSimilarProperties) > 4)
                                    <?php
                                    $count = 1;
                                    ?>
                                    <div class="item">
                                        <div class="row">
                                            @foreach($flyerPhotos as $flyerPhoto)
                                            @if($count == 5)
                                            @break;
                                            @endif
                                            <?php
                                            $count++;
                                            $randomNumber = rand(0, count($flyerPhotos) - 1);
                                            ?>

                                            <div class="col-md-3" id="similarFlyers">
                                                <div class="post-module hove">
                                                    <div class="thumbnail">
                                                        @if(is_null($flyerPhotos[$randomNumber]))
                                                        <a href="/{{$similarFlyerers[$randomNumber]->zip}}/{{str_replace(' ', '-', $similarFlyerers[$randomNumber]->street)}}"><img
                                                            src="/images/no-image.png" alt="sample90"/></a>
                                                            @else
                                                            <a href="/{{$similarFlyerers[$randomNumber]->zip}}/{{str_replace(' ', '-', $similarFlyerers[$randomNumber]->street)}}"><img
                                                                src="/images/photos/{{$flyerPhotos[$randomNumber]->name}}"
                                                                alt="sample90"/></a>
                                                                @endif
                                                                <div class="date">
                                                                    <span class="day">{{ $flyer->created_at->format('d') }}</span>
                                                                    <span class="month">{{ $flyer->created_at->format('M') }}</span>
                                                                </div>
                                                            </div>
                                                            <div class="post-content">
                                                                <h2 class="category">{!! $similarFlyerers[$randomNumber]->productID !!}</h2>

                                                                <h1 class="title"><a
                                                                    href="/{{$similarFlyerers[$randomNumber]->zip}}/{{$similarFlyerers[$randomNumber]->street}}">{{ $similarFlyerers[$randomNumber]->street }}</a>
                                                                </h1>
                                                                <div class="sub_title">{{ $similarFlyerers[$randomNumber]->price }}</div>
                                                                <div class="descript">{{ $similarFlyerers[$randomNumber]->description }}</div>
                                                                <div class="col-md-12">
                                                                    <div class="row" style="margin-top: 20px;">
                                                                        <div class="addToCartSimilarFlyers">
                                                                            <a class="addtocart">
                                                                                <div class="add"><i class="fa fa-shopping-cart shippingCart"  aria-hidden="true"></i>&nbsp&nbspAdd to Cart</div>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <?php
                                                    unset($similarFlyerers[$randomNumber]);
                                                    $similarFlyerers = array_values($similarFlyerers);
                                                    ?>

                                                    <?php
                                                    unset($flyerPhotos[$randomNumber]);
                                                    $flyerPhotos = array_values($flyerPhotos);
                                                    ?>
                                                    @endforeach
                                                </div><!--.row-->
                                            </div><!--.item-->
                                            @endif


                                            @if(count($numberOfSimilarProperties) > 8)
                                            <?php
                                            $count = 1;
                                            ?>
                                            <div class="item">
                                                <div class="row">
                                                    @foreach($flyerPhotos as $flyerPhoto)
                                                    @if($count == 5)
                                                    @break;
                                                    @endif
                                                    <?php
                                                    $count++;
                                                    $randomNumber = rand(0, count($flyerPhotos) - 1);
                                                    ?>
                                                    <div class="col-md-3" id="similarFlyers">
                                                        <div class="post-module hove">
                                                            <div class="thumbnail">
                                                                @if(is_null($flyerPhotos[$randomNumber]))
                                                                <a href="/{{$similarFlyerers[$randomNumber]->zip}}/{{str_replace(' ', '-', $similarFlyerers[$randomNumber]->street)}}"><img
                                                                    src="/images/no-image.png" alt="sample90"/></a>
                                                                    @else
                                                                    <a href="/{{$similarFlyerers[$randomNumber]->zip}}/{{str_replace(' ', '-', $similarFlyerers[$randomNumber]->street)}}"><img
                                                                        src="/images/photos/{{$flyerPhotos[$randomNumber]->name}}"
                                                                        alt="sample90"/></a>
                                                                        @endif
                                                                        <div class="date">
                                                                            <span class="day">{{ $flyer->created_at->format('d') }}</span>
                                                                            <span class="month">{{ $flyer->created_at->format('M') }}</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="post-content">
                                                                        <h2 class="category">{!! $similarFlyerers[$randomNumber]->productID !!}</h2>

                                                                        <h1 class="title"><a
                                                                            href="/{{$similarFlyerers[$randomNumber]->zip}}/{{$similarFlyerers[$randomNumber]->street}}">{{ $similarFlyerers[$randomNumber]->street }}</a>
                                                                        </h1>
                                                                        <div class="sub_title">{{ $similarFlyerers[$randomNumber]->price }}</div>
                                                                        <div class="descript">{{ $similarFlyerers[$randomNumber]->description }}</div>
                                                                        <div class="col-md-12">
                                                                            <div class="row" style="margin-top: 20px;">
                                                                                <div class="addToCartSimilarFlyers">
                                                                                    <a class="addtocart">
                                                                                        <div class="add"><i class="fa fa-shopping-cart shippingCart"  aria-hidden="true"></i>&nbsp&nbspAdd to Cart</div>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <?php
                                                            unset($similarFlyerers[$randomNumber]);
                                                            $similarFlyerers = array_values($similarFlyerers);
                                                            ?>
                                                            <?php
                                                            unset($flyerPhotos[$randomNumber]);
                                                            $flyerPhotos = array_values($flyerPhotos);
                                                            ?>
                                                            @endforeach
                                                        </div><!--.row-->
                                                    </div><!--.item-->
                                                    @endif


                                                </div><!--.carousel-inner-->
                                                <a data-slide="prev" href="#Carousel" class="left carousel-control">‹</a>
                                                <a data-slide="next" href="#Carousel" class="right carousel-control">›</a>
                                            </div><!--.Carousel-->
                                        </div>
                                    </div>
                                </div>

                                {{-- REVIEW FLYER AREA --}}
                                <div class="row">
                                    <div class="row reviewFlyers">
                                        {{-- {{dump($reviewsAtachetToFlyer)}} --}}
                                        @if(count($reviewsAtachetToFlyer) > 0)
                                        <h4 id="flyer-review-heeading">FLyers Reviews</h4>
                                        @endif
                                        @foreach($reviewsAtachetToFlyer as $review)


                                        {{-- EDIT REVIEW TO FLYER MODAL --}}
                                        <div id="editReviewModal-{{$review->id}}" name="editReviewModal" class="modal">
                                            <div class="modal-dialog animated">
                                                <div class="modal-content">
                                                    <form class="form-horizontal" id="editReviewModalForm" method="GET"
                                                    action="/editReviewToFlyerModal">
                                                    {{csrf_field()}}
                                                    <div class="modal-header">
                                                        <strong>Edit your review below</strong>
                                                    </div>

                                                    <div class="modal-body">
                                                        <div class="form-group">
                                                            <input style="display:none" id="flyerIdShowPageForEditReview"
                                                            name="flyerIdShowPageForEditReview" value="{{$flyer->id}}"/>
                                                            <input style="display:none" id="editReviewId" name="editReviewId"
                                                            value="{{$review->id}}"/>

                                                            <textarea type="text" name="editReviewTextarea" id="reviewTextarea"
                                                            class="form-control">{{$review->review}}</textarea>
                                                        </div>
                                                        <div class="col-md-12" id="ratingAreaModal">
                                                            <div class="ratingEditReviewModal {{$review->id}} row"
                                                             id="review-{{$review->id}}">
                                                             @if($review->rating_review == 5)
                                                             <input type="radio" class="selectedRatingReview"
                                                             id="edit_review_star5" name="editRatingReview" value="5"/>
                                                             <label
                                                             for="review_star5"></label>
                                                             <input type="radio" id="edit_review_star4" name="editRatingReview"
                                                             value="4"/><label
                                                             for="review_star4"></label>
                                                             <input type="radio" id="edit_review_star3" name="editRatingReview"
                                                             value="3"/><label
                                                             for="review_star3"></label>
                                                             <input type="radio" id="edit_review_star2" name="editRatingReview"
                                                             value="2"/><label
                                                             for="review_star2"></label>
                                                             <input type="radio" id="edit_review_star1" name="editRatingReview"
                                                             value="1"/><label
                                                             for="review_star1"></label>

                                                             @elseif($review->rating_review == 4)
                                                             <input type="radio" id="edit_review_star5" name="editRatingReview"
                                                             value="5"/><label
                                                             for="review_star5"></label>
                                                             <input type="radio" class="selectedRatingReview"
                                                             id="edit_review_star4" name="editRatingReview" value="4"/>
                                                             <label
                                                             for="review_star4"></label>
                                                             <input type="radio" id="edit_review_star3" name="editRatingReview"
                                                             value="3"/><label
                                                             for="review_star3"></label>
                                                             <input type="radio" id="edit_review_star2" name="editRatingReview"
                                                             value="2"/><label
                                                             for="review_star2"></label>
                                                             <input type="radio" id="edit_review_star1" name="editRatingReview"
                                                             value="1"/><label
                                                             for="review_star1"></label>

                                                             @elseif($review->rating_review == 3)
                                                             <input type="radio" id="edit_review_star5" name="editRatingReview"
                                                             value="5"/><label
                                                             for="review_star5"></label>
                                                             <input type="radio" id="edit_review_star4" name="ratingReview"
                                                             value="4"/><label
                                                             for="review_star4"></label>
                                                             <input type="radio" class="selectedRatingReview"
                                                             id="edit_review_star3" name="editRatingReview" value="3"/>
                                                             <label
                                                             for="review_star3"></label>
                                                             <input type="radio" id="edit_review_star2" name="editRatingReview"
                                                             value="2"/><label
                                                             for="review_star2"></label>
                                                             <input type="radio" id="edit_review_star1" name="editRatingReview"
                                                             value="1"/><label
                                                             for="review_star1"></label>

                                                             @elseif($review->rating_review == 2)
                                                             <input type="radio" id="edit_review_star5" name="editRatingReview"
                                                             value="5"/><label
                                                             for="review_star5"></label>
                                                             <input type="radio" id="edit_review_star4" name="editRatingReview"
                                                             value="4"/><label
                                                             for="review_star4"></label>
                                                             <input type="radio" id="edit_review_star3" name="editRatingReview"
                                                             value="3"/><label
                                                             for="review_star3"></label>
                                                             <input type="radio" class="selectedRatingReview"
                                                             id="edit_review_star2" name="editRatingReview" value="2"/>
                                                             <label
                                                             for="review_star2"></label>
                                                             <input type="radio" id="edit_review_star1" name="editRatingReview"
                                                             value="1"/><label
                                                             for="review_star1"></label>

                                                             @elseif($review->rating_review == 1)
                                                             <input type="radio" id="edit_review_star5" name="editRatingReview"
                                                             value="5"/><label
                                                             for="review_star5"></label>
                                                             <input type="radio" id="edit_review_star4" name="editRatingReview"
                                                             value="4"/><label
                                                             for="review_star4"></label>
                                                             <input type="radio" id="edit_review_star3" name="editRatingReview"
                                                             value="3"/><label
                                                             for="review_star3"></label>
                                                             <input type="radio" id="edit_review_star2" name="editRatingReview"
                                                             value="2"/><label
                                                             for="review_star2"></label>
                                                             <input type="radio" class="selectedRatingReview"
                                                             id="edit_review_star1" name="editRatingReview" value="1"/>
                                                             <label
                                                             for="review_star1"></label>
                                                             @else
                                                             <input type="radio" id="edit_review_star5" name="ratingReview"
                                                             value="5"/><label
                                                             for="review_star5"></label>
                                                             <input type="radio" id="edit_review_star4" name="ratingReview"
                                                             value="4"/><label
                                                             for="review_star4"></label>
                                                             <input type="radio" id="edit_review_star3" name="ratingReview"
                                                             value="3"/><label
                                                             for="review_star3"></label>
                                                             <input type="radio" id="edit_review_star2" name="ratingReview"
                                                             value="2"/><label
                                                             for="review_star2"></label>
                                                             <input type="radio" id="edit_review_star1" name="ratingReview"
                                                             value="1"/><label
                                                             for="review_star1"></label>
                                                             @endif
                                                         </div>
                                                     </div>
                                                 </div>

                                                 <div class="modal-footer">
                                                    <button class="btn btn-default" type="button">Cancel</button>
                                                    <button class="btn btn-primary {{$review->id}}"
                                                        id="editReviewModalButton" type="submit">Save
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>


                                {{-- REPLY REVIEW TO FLYER MODAL --}}
                                <div id="replyReviewModal-{{$review->id}}" name="replyReviewModal" class="modal">
                                    <div class="modal-dialog animated">
                                        <div class="modal-content">
                                            <form class="form-horizontal" id="replyReviewModalForm" method="GET"
                                            action="/replyReviewToFlyerModal">
                                            {{csrf_field()}}
                                            <div class="modal-header">
                                                <strong>Reply to review below</strong>
                                            </div>

                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <input style="display:none" id="replyReviewId" name="replyReviewId"
                                                    value="{{$review->id}}"/>
                                                    <textarea type="text" name="replyReviewTextarea" id="reviewTextarea"
                                                    class="form-control"></textarea>
                                                </div>
                                            </div>

                                            <div class="modal-footer">
                                                <button class="btn btn-default" type="button">Cancel</button>
                                                <button class="btn btn-primary {{$review->id}}"
                                                    id="replyReviewModalButton" type="submit">Save
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>


                            {{-- SHOW REVIEWS AREA  --}}
                            <div class="reviewArea">
                                <div class="client">
                                    <span class="image-container">
                                        @if($review->image_path == null)
                                        <img width="80" height="80" src="/images/no-user-image.png">
                                        @else
                                        <img width="80" height="80" src="/{{$review->image_path}}">
                                        @endif
                                    </span>

                                    <div class="row ratingNameAndDate">
                                        <div class="col-md-12">
                                            <div class="col-md-9">
                                                <h2 class="heading">{{$review->name}}</h2>
                                            </div>
                                            <div class="col-md-3">
                                                @if(Auth::check())
                                                <span style="margin-left:180px"
                                                id="reviewDate">{{Carbon\Carbon::parse($review->created_at)->format('d-m-Y')}}</span>
                                                @if(($user->id == $review->user_id) || ($user->hasRole('admin')))
                                                <a href="#" <i data-toggle="modal"
                                                data-target="#editReviewModal-{{$review->id}}"
                                                class="fa fa-pencil-square-o {{$review->id}}"
                                                aria-hidden="true" name="reviewnumber"
                                                id="editReviewPen"></i></a>
                                                @endif
                                                @else
                                                <span style="margin-left:180px"
                                                id="reviewDate">{{Carbon\Carbon::parse($review->created_at)->format('d-m-Y')}}</span>
                                                @endif

                                                <div class="col-md-12" id="ratingReviewArea">
                                                    <div class="col-md-6">
                                                        <div class="ratingReview reviewIds-{{$review->id}} row">
                                                            @if($review->rating_review == 5)
                                                            <input type="radio" class="selectedRatingReview"
                                                            id="review_star5" name="ratingReview" value="5"/><label
                                                            for="review_star5"></label>
                                                            <input type="radio" id="review_star4" name="ratingReview"
                                                            value="4"/><label
                                                            for="review_star4"></label>
                                                            <input type="radio" id="review_star3" name="ratingReview"
                                                            value="3"/><label
                                                            for="review_star3"></label>
                                                            <input type="radio" id="review_star2" name="ratingReview"
                                                            value="2"/><label
                                                            for="review_star2"></label>
                                                            <input type="radio" id="review_star1" name="ratingReview"
                                                            value="1"/><label
                                                            for="review_star1"></label>

                                                            @elseif($review->rating_review == 4)
                                                            <input type="radio" id="review_star5" name="ratingReview"
                                                            value="5"/><label
                                                            for="review_star5"></label>
                                                            <input type="radio" class="selectedRatingReview"
                                                            id="review_star4" name="ratingReview" value="4"/><label
                                                            for="review_star4"></label>
                                                            <input type="radio" id="review_star3" name="ratingReview"
                                                            value="3"/><label
                                                            for="review_star3"></label>
                                                            <input type="radio" id="review_star2" name="ratingReview"
                                                            value="2"/><label
                                                            for="review_star2"></label>
                                                            <input type="radio" id="review_star1" name="ratingReview"
                                                            value="1"/><label
                                                            for="review_star1"></label>

                                                            @elseif($review->rating_review == 3)
                                                            <input type="radio" id="review_star5" name="ratingReview"
                                                            value="5"/><label
                                                            for="review_star5"></label>
                                                            <input type="radio" id="review_star4" name="ratingReview"
                                                            value="4"/><label
                                                            for="review_star4"></label>
                                                            <input type="radio" class="selectedRatingReview"
                                                            id="review_star3" name="ratingReview" value="3"/><label
                                                            for="review_star3"></label>
                                                            <input type="radio" id="review_star2" name="ratingReview"
                                                            value="2"/><label
                                                            for="review_star2"></label>
                                                            <input type="radio" id="review_star1" name="ratingReview"
                                                            value="1"/><label
                                                            for="review_star1"></label>

                                                            @elseif($review->rating_review == 2)
                                                            <input type="radio" id="review_star5" name="ratingReview"
                                                            value="5"/><label
                                                            for="review_star5"></label>
                                                            <input type="radio" id="review_star4" name="ratingReview"
                                                            value="4"/><label
                                                            for="review_star4"></label>
                                                            <input type="radio" id="review_star3" name="ratingReview"
                                                            value="3"/><label
                                                            for="review_star3"></label>
                                                            <input type="radio" class="selectedRatingReview"
                                                            id="review_star2" name="ratingReview" value="2"/><label
                                                            for="review_star2"></label>
                                                            <input type="radio" id="review_star1" name="ratingReview"
                                                            value="1"/><label
                                                            for="review_star1"></label>

                                                            @elseif($review->rating_review == 1)
                                                            <input type="radio" id="review_star5" name="ratingReview"
                                                            value="5"/><label
                                                            for="review_star5"></label>
                                                            <input type="radio" id="review_star4" name="ratingReview"
                                                            value="4"/><label
                                                            for="review_star4"></label>
                                                            <input type="radio" id="review_star3" name="ratingReview"
                                                            value="3"/><label
                                                            for="review_star3"></label>
                                                            <input type="radio" id="review_star2" name="ratingReview"
                                                            value="2"/><label
                                                            for="review_star2"></label>
                                                            <input type="radio" class="selectedRatingReview"
                                                            id="review_star1" name="ratingReview" value="1"/><label
                                                            for="review_star1"></label>
                                                            @else
                                                            <input type="radio" id="review_star5" name="ratingReview"
                                                            value="5"/><label
                                                            for="review_star5"></label>
                                                            <input type="radio" id="review_star4" name="ratingReview"
                                                            value="4"/><label
                                                            for="review_star4"></label>
                                                            <input type="radio" id="review_star3" name="ratingReview"
                                                            value="3"/><label
                                                            for="review_star3"></label>
                                                            <input type="radio" id="review_star2" name="ratingReview"
                                                            value="2"/><label
                                                            for="review_star2"></label>
                                                            <input type="radio" id="review_star1" name="ratingReview"
                                                            value="1"/><label
                                                            for="review_star1"></label>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <p class="info">{{$review->review}}</p>
                                </div>
                                <div class="replyReview">
                                    @if(Auth::check())
                                    <a href="#" data-toggle="modal" data-target="#replyReviewModal-{{$review->id}}"
                                       id="replyToReview"><i class="fa fa-reply {{$review->id}}"
                                       aria-hidden="true"></i> reply</a>
                                       @endif
                                   </div>

                                   {{-- REPLY REVIEW AREA --}}
                                   @foreach($replyToReviews as $replyReview)
                                   {{-- EDIT REPLY TO REVIEW TO FLYER MODAL --}}
                                   <div id="editReplyReviewModal-{{$replyReview->id}}" name="editReplyReviewToFlyerModal"
                                     class="modal">
                                     <div class="modal-dialog animated">
                                        <div class="modal-content">
                                            <form class="form-horizontal" id="editReplyReviewModalForm" method="GET"
                                            action="/editReplyReviewToFlyerModal">
                                            {{csrf_field()}}
                                            <div class="modal-header">
                                                <strong>Edit your reply to this review below</strong>
                                            </div>

                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <input style="display:none" id="editReplyReviewId"
                                                    name="editReplyReviewId" value="{{$replyReview->id}}"/>
                                                    <input style="display:none" id="editReplyUserId"
                                                    name="editReplyUserId" value="{{$replyReview->user_id}}"/>
                                                    <textarea type="text" name="editReplyReviewTextarea"
                                                    id="reviewTextarea"
                                                    class="form-control">{{$replyReview->reply_review}}</textarea>
                                                </div>
                                            </div>

                                            <div class="modal-footer">
                                                <button class="btn btn-default" type="button">Cancel</button>
                                                <button class="btn btn-primary" id="editReviewModalButton"
                                                type="submit">Save
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        @if($replyReview->review_id == $review->id)
                        <div class="reviewArea" id="replyReviewArea" style="margin-left: 100px;">
                            <div class="client">
                                <span class="image-container">
                                    @if($replyReview->image_path == null)
                                    <img width="80" height="80" src="/images/no-user-image.png">
                                    @else
                                    <img width="80" height="80" src="/{{$replyReview->image_path}}">
                                    @endif
                                </span>

                                <div class="row ratingNameAndDate">
                                    <div class="col-md-12">
                                        <div class="col-md-8">
                                            <h2 class="heading">{{$replyReview->name}}</h2>
                                        </div>
                                        <div class="col-md-4">
                                            @if(Auth::check())
                                            <span id="replyReviewDate">{{Carbon\Carbon::parse($replyReview->created_at)->format('d-m-Y')}}</span>
                                            @if(($user->id == $replyReview->user_id) || ($user->hasRole('admin')))
                                            <a href="#" <i data-toggle="modal"
                                            data-target="#editReplyReviewModal-{{$replyReview->id}}"
                                            class="fa fa-pencil-square-o {{$replyReview->id}}"
                                            aria-hidden="true" name="reviewnumber"
                                            id="editReplayReviewPen"></i></a>
                                            @endif
                                            @else
                                            <span style="margin-left:190px"
                                            id="reviewDate">{{Carbon\Carbon::parse($replyReview->created_at)->format('d-m-Y')}}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <p class="info">{{$replyReview->reply_review}}</p>
                            </div>
                        </div>
                        @endif
                        @endforeach

                    </div>
                    @endforeach
                </div>
            </div>

        </div>
    </div>
</div>
</div>
@stop


@section('pagescriptModal')
<script src="/js/rmodal.js"></script>
<script src="/js/custom/customModal.js"></script>
<script src="/js/jssor.slider-23.1.0.mini.js"></script>
<script src="/js/custom/jssor_slider.js"></script>
<script type="text/javascript">
    jssor_slider();
</script>
@stop