@if(count($allFeaturedFlyers) > 0)
<h1 id="featuresFlyersHeading">Featured Flyers</h1>

<div class="row allFeaturedFlyersRow" id="app-shop">
@foreach($allFeaturedFlyers->chunk(4) as  $flyers)
	<div class="gridListView">
		@foreach($flyers as $flyer)

		<?php 
		$ratings = 0;
		foreach($flyer->rating as $rating){
			if($rating->rating !== 'undefined'){
				$ratings = $rating->rating;
			}
		}
		$reviewRatings = 0;
		foreach($flyer->review_ratings as $rev_rate){
			if($rev_rate->rating_review !== 'undefined'){
				$reviewRatings += $rev_rate->rating_review;
			}
		}
		$numberOfReview_ratings = count($flyer->review_ratings);
		if($ratings !== 0){
			$numberOfReview_ratings += 1;
		}
		$productRating = 0;
		if($numberOfReview_ratings !== 0){
			$productRating = round(($ratings + $reviewRatings) / $numberOfReview_ratings);
		}
		?>

		<div class="column grid" id="articles">
			<div class="post-module hove {{ $flyer->id }}">
			<input type="hidden" id="productIdAllFlyers" value="{{ $flyer->productID }}" />

				{{-- thumbnail --}}
				<div class="thumbnail">
					<a href="/{{$flyer->zip}}/{{str_replace(' ', '-', $flyer->street)}}">
						@if(count($flyer->photos) > 0)
						@foreach($flyer->photos as $photo)
						<img src="/{{$photo->path}}" />
						@endforeach
						@else
						<img src="/images/no-image.png" />
						@endif
					</a>
				</div>
				{{-- end thumbnail --}}

				{{-- post content --}}
				<div class="post-content">
					<div class="row quickviewAndFavoritePanel">

						<div class="row productDetails">
							<h2 class="category">{{ $flyer->productID }}</h2>
							<h1 class="title"><a href="/{{$flyer->zip}}/{{str_replace(' ', '-', $flyer->street)}}">{{ $flyer->street }}</a></h1>

							<div class="row ratingReview" id="ratingReviewArea">
								@if($productRating == 5)
								<input type="radio" class="selectedRatingReview" id="review_star5" name="ratingReview" value="5"/>
								<label for="review_star5"></label>
								<input type="radio" id="review_star4" name="ratingReview" value="4"/>
								<label for="review_star4"></label>
								<input type="radio" id="review_star3" name="ratingReview" value="3"/>
								<label for="review_star3"></label>
								<input type="radio" id="review_star2" name="ratingReview" value="2"/>
								<label for="review_star2"></label>
								<input type="radio" id="review_star1" name="ratingReview" value="1"/>
								<label for="review_star1"></label>

								@elseif($productRating == 4)
								<input type="radio" id="review_star5" name="ratingReview" value="5"/>
								<label for="review_star5"></label>
								<input type="radio" class="selectedRatingReview" id="review_star4" name="ratingReview" value="4"/>
								<label for="review_star4"></label>
								<input type="radio" id="review_star3" name="ratingReview" value="3"/>
								<label for="review_star3"></label>
								<input type="radio" id="review_star2" name="ratingReview" value="2"/>
								<label for="review_star2"></label>
								<input type="radio" id="review_star1" name="ratingReview" value="1"/>
								<label for="review_star1"></label>

								@elseif($ratings == 3)
								<input type="radio" id="review_star5" name="ratingReview" value="5"/>
								<label for="review_star5"></label>
								<input type="radio" id="review_star4" name="ratingReview" value="4"/>
								<label for="review_star4"></label>
								<input type="radio" class="selectedRatingReview" id="review_star3" name="ratingReview" value="3"/>
								<label for="review_star3"></label>
								<input type="radio" id="review_star2" name="ratingReview" value="2"/>
								<label for="review_star2"></label>
								<input type="radio" id="review_star1" name="ratingReview" value="1"/>
								<label for="review_star1"></label>

								@elseif($productRating == 2)
								<input type="radio" id="review_star5" name="ratingReview" value="5"/>
								<label for="review_star5"></label>
								<input type="radio" id="review_star4" name="ratingReview" value="4"/>
								<label for="review_star4"></label>
								<input type="radio" id="review_star3" name="ratingReview" value="3"/>
								<label for="review_star3"></label>
								<input type="radio" class="selectedRatingReview" id="review_star2" name="ratingReview" value="2"/>
								<label for="review_star2"></label>
								<input type="radio" id="review_star1" name="ratingReview" value="1"/>
								<label for="review_star1"></label>

								@elseif($productRating == 1)
								<input type="radio" id="review_star5" name="ratingReview" value="5"/>
								<label for="review_star5"></label>
								<input type="radio" id="review_star4" name="ratingReview" value="4"/>
								<label for="review_star4"></label>
								<input type="radio" id="review_star3" name="ratingReview" value="3"/>
								<label for="review_star3"></label>
								<input type="radio" id="review_star2" name="ratingReview" value="2"/>
								<label for="review_star2"></label>
								<input type="radio" class="selectedRatingReview" id="review_star1" name="ratingReview" value="1"/>
								<label for="review_star1"></label>

								@else
								<input type="radio" id="review_star5" name="ratingReview" value="5"/>
								<label for="review_star5"></label>
								<input type="radio" id="review_star4" name="ratingReview" value="4"/>
								<label for="review_star4"></label>
								<input type="radio" id="review_star3" name="ratingReview" value="3"/>
								<label for="review_star3"></label>
								<input type="radio" id="review_star2" name="ratingReview" value="2"/>
								<label for="review_star2"></label>
								<input type="radio" id="review_star1" name="ratingReview" value="1"/>
								<label for="review_star1"></label>
								@endif
							</div>
							<div class="numberOfReviews">
								@if($numberOfReview_ratings > 1)
								{{ $numberOfReview_ratings }} reviews
								@else
								{{ $numberOfReview_ratings }} review
								@endif
							</div>

							<div class="sub_title">{{ $flyer->price }}</div>
							<div class="descript">{{ $flyer->description }}</div>


							<div class="row">
								<div class="col-lg-12 addToCartColumn">
									<button class="btn btn-success addToCartButton" onclick="event.preventDefault();" @click="addToCartVueAllFlyers({{ $flyer->id }})">
										<i class="fa fa-shopping-cart" aria-hidden="true"></i>&nbsp&nbspAdd to Cart
									</button>
								</div>
							</div>
						</div>

					</div>
				</div>
				{{-- \end post content --}}

			</div>
		</div>
		@endforeach
	</div>
	@endforeach
</div>
@endif