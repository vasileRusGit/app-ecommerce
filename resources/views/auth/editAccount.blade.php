@extends('layout')

@section('content')
<div class="container">
    <div class="row loginRegisterPanel">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                @if(count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                        {{$error}}
                        @endforeach
                    </ul>
                </div>
                @endif
                <div class="panel-heading">Edit account</div>

                <div class="panel-body">
                    {{ Form::open(['method' => 'PUT', 'class' => 'form-horizontal','route' => ['user.update.account', $user->id], 'files' => true]) }}
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name" class="col-md-4 control-label">Name</label>
                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control" name="name" value="{{ $user->name }}" required autofocus>
                            @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-4 control-label">E-Mail Address</label>
                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control" name="email" value="{{ $user->email }}" required>

                            @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="col-md-4 control-label">Password</label>
                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control" name="password">

                            @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>
                        <div class="col-md-6">
                            <input id="password-confirm" type="password" class="form-control" name="password-confirm">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">User Image:</label>
                        <div class="col-md-6">
                            <label for="custom-file-upload" style="width: 100%" class="userImage">
                                <span class="filupp-file-name js-value">Browse Files</span>
                                <input type="file" name="editUserImageFrontPage" value="1" id="custom-file-upload"/>
                            </label>
                        </div>
                        @if($user->image_path == null)
                        <img src="/images/no-user-image.png" id="imageHolderFrontPage"/>
                        @else
                        <img src="/{{ $user->image_path }}" id="imageHolderFrontPage" />
                        @endif
                    </div>

                    <hr>

                    <div class="delivery">
                        <a href="#" class="more show">+ show more</a>
                        <a href="#" class="less hide">- show less</a>

                        {{-- delivery --}}
                        <div class="form-group hide">
                            <div class="editAccountDelivery">Delivery Address Information</div>
                            <label for="country" class="col-md-4 control-label">Country: </label>
                            <div class="col-md-6">
                                <input id="country" type="text" class="form-control" value="{{ $user->country }}" name="country" >
                            </div>
                        </div>
                        <div class="form-group hide">
                            <label for="state" class="col-md-4 control-label">State: </label>
                            <div class="col-md-6">
                                <input id="state" type="text" class="form-control" value="{{ $user->state }}" name="state" >
                            </div>
                        </div>
                        <div class="form-group hide">
                            <label for="address" class="col-md-4 control-label">Address: </label>
                            <div class="col-md-6">
                                <input id="address" type="text" class="form-control" value="{{ $user->address }}" name="address" >
                            </div>
                        </div>
                        <div class="form-group hide">
                            <label for="zip" class="col-md-4 control-label">Zip: </label>
                            <div class="col-md-6">
                                <input id="zip" type="number" class="form-control" value="{{ $user->zip }}" name="zip" >
                            </div>
                        </div>
                        <div class="form-group hide">
                            <label for="phone" class="col-md-4 control-label">Phone: </label>
                            <div class="col-md-6">
                                <input id="phone" type="number" class="form-control" value="{{ $user->phone }}" name="phone" >
                            </div>
                        </div>

                        {{-- credit card --}}
                        <div class="form-group hide">
                            <div class="editAccountCreditCard">CreditCard Information</div>
                            <label for="cardNumber1" class="col-md-4 control-label">Card Number: </label>
                            <div class="col-md-6">
                                <div class="col-md-3" style="padding-left: 0px; padding-right: 5px;"><input id="cardNumber" type="text" maxlength="4" oninput="this.value=this.value.replace(/[^0-9]/g,'');" class="form-control" value="{{ $user->card_number_1 }}" name="cardNumber1"></div>
                                <div class="col-md-3" style="padding-left: 5px; padding-right: 5px;">
                                    <input id="cardNumber2" type="text" maxlength="4" oninput="this.value=this.value.replace(/[^0-9]/g,'');" class="form-control" value="{{ $user->card_number_2 }}" name="cardNumber2" >
                                </div>
                                <div class="col-md-3" style="padding-left: 5px; padding-right: 5px;">
                                    <input id="cardNumber3" type="text" maxlength="4" oninput="this.value=this.value.replace(/[^0-9]/g,'');" class="form-control" value="{{ $user->card_number_3 }}" name="cardNumber3">
                                    </div>
                                <div class="col-md-3" style="padding-left: 5px; padding-right: 0;">
                                    <input id="cardNumber4" type="text" maxlength="4" oninput="this.value=this.value.replace(/[^0-9]/g,'');" class="form-control" value="{{ $user->card_number_4 }}" name="cardNumber4" >
                                </div>
                            </div>
                        </div>
                        <div class="form-group hide">
                            <label for="cardHolder" class="col-md-4 control-label">Card Holder: </label>
                            <div class="col-md-6">
                                <input id="cardHolder" type="text" class="form-control" value="{{ $user->card_holder }}" name="cardHolder" >
                            </div>
                        </div>
                        <div class="form-group hide">
                            <label for="cardExpirationMonth" class="col-md-4 control-label">Expiration Month: </label>
                            <div class="col-md-3">
                            <input id="cardExpirationMonth" type="text" class="form-control" value="{{ $user->card_expiration_month }}" name="cardExpirationMonth" maxlength="2" oninput="this.value=this.value.replace(/[^0-9]/g,'');" style="max-width:80px">
                            </div>
                            <label for="cardExpirationYear" class="col-md-4 control-label" style="margin-left: -10%; padding-left: 0;width: 20%;">Expiration Year: </label>
                            <div class="col-md-3">
                            <input id="cardExpirationYear" type="text" class="form-control" value="{{ $user->card_expiration_year }}" name="cardExpirationYear" maxlength="4" oninput="this.value=this.value.replace(/[^0-9]/g,'');" style="max-width:80px">
                            </div>
                        </div>
                        <div class="form-group hide">
                            <label for="ccv" class="col-md-4 control-label">CCV: </label>
                            <div class="col-md-6">
                                <input id="ccv" maxlength="4" oninput="this.value=this.value.replace(/[^0-9]/g,'');" type="text" class="form-control" value="{{ $user->card_ccv }}" name="ccv" >
                            </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <hr>
                        <div class="col-md-6 col-md-offset-4">
                            {{ Form::submit('Update', ['class' => 'btn btn-warning']) }}
                        </div>
                    </div>
                    {{Form::close()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection