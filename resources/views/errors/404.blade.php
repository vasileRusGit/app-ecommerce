<style>
/*	html, body {
		height: 100%;
	}*/

	body {
		color: #B0BEC5;
		font-weight: 100;
		font-family: 'Lato', sans-serif;
	}

	.content404 {
		text-align: center;
		vertical-align: middle;
	}

	.title404 {
		font-size: 72px;
		margin-bottom: 50px;
	}
</style>

<div class="content404">
	<div class="title404">404 No result found.</div>
</div>