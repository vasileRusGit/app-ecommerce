<!DOCTYPE html>

<html>
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <meta name="viewport" content="width=device-width">
    <title>Admin Panel</title>
    <link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" href="/css/admin/adminMobileVersion.css">
    <link rel="stylesheet" href="/css/admin/adminLte.css">
    <link rel="stylesheet" href="/css/sweetalert.css">
    <link rel="stylesheet" href="/css/dropzone.css">
    <link rel="stylesheet" href="/css/admin/skin-blue.css">
    <link rel="stylesheet" href="/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap4.min.css">
    {{-- <link rel="stylesheet" href="https://cdn.datatables.net/rowreorder/1.2.0/css/rowReorder.dataTables.min.css"> --}}
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.dataTables.min.css">
</head>

@if ($toggleStatus == 'off')
<body class="skin-blue">
    @else
    <body class="skin-blue sidebar-mini sidebar-collapse">
        @endif
        <div class="wrapper">

            <!-- Main Header -->
            <header class="main-header">
                @include('admin/header')
            </header>


            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                @include('admin/sidebar')
            </aside>


            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                @if(Request::path() == "admin")
                @include('admin/contentDashboard')
                @endif
                @if(Request::path() == "admin/flyer/create")
                @include('admin/flyer/contentAddNewFlyer')
                @endif
                @if(Request::path() == "admin/flyers/show")
                @include('admin/flyer/contentShowFlyers')
                @endif
                @if(Request::path() == "admin/featured/flyers/show")
                @include('admin/flyer/contentShowFeaturedFlyers')
                @endif
                @if(Request::path() == "admin/flyers/rating/show")
                @include('admin/flyer/contentShowFlyersRating')
                @endif
                @if(Request::path() == "admin/flyers/review/show")
                @include('admin/flyer/contentShowFlyersReview')
                @endif
                @if(Request::path() == "admin/invoice/create")
                @include('admin/invoice/contentAddNewInvoice')
                @endif
                @if(Request::path() == "admin/invoice/show")
                @include('admin/invoice/contentShowAdminInvoice')
                @endif
                @if((strpos(Request::path(), 'edit') !== false) && (strpos(Request::path(), 'invoice') !== false))
                @include('admin/invoice/contentEditInvoice')
                @endif
                @if((strpos(Request::path(), 'edit') !== false) && (strpos(Request::path(), 'flyer') !== false))
                @include('admin/flyer/contentEditFlyer')
                @endif
                @if((strpos(Request::path(), 'edit') !== false) && (strpos(Request::path(), 'review') !== false))
                @include('admin/flyer/contentEditReview')
                @endif
                @if((strpos(Request::path(), 'edit') !== false) && (strpos(Request::path(), 'user') !== false))
                @include('admin/user/contentEditUser')
                @endif
                @if(Request::path() == "admin/users/permission")
                @include('admin/user/contentUsersPermission')
                @endif
                @if(Request::path() == "admin/users/profiles")
                @include('admin/user/contentUsersProfiles')
                @endif
                @if(Request::path() == "admin/user/create")
                @include('admin/user/contentAddNewUser')
                @endif
            </div>

        </div><!-- ./wrapper -->

        <!-- REQUIRED JS SCRIPTS -->
        <script src="/js/jquery.js"></script>
        <script src="/js/app.js"></script>
        <script src="/js/adminLte.js"></script>
        <script src="/js/bootstrap.js"></script>
        <script src="/js/custom/productID.js"></script>
        <script src="/js/custom/featuredFlyers.js"></script>
        <script src="/js/custom/adminPanelCustom.js"></script>
        <script src="/js/sweetalert.js"></script>
        <script src="/js/custom/custom.js"></script>
        <script src="/js/jquery.cookie.js"></script>
        <script src="/js/lodash.js"></script>
        <script src="/js/dropzone.js"></script>
        <script src="/js/custom/adminSubmitForm&Errors.js"></script>
        <script src="/js/custom/adminRatingReview.js"></script>
        <script src="/js/jquery.dataTables.min.js"></script>
        <script src="/js/dataTables.bootstrap4.min.js"></script>
        <script src="/js/rmodal.js"></script>
        <script src="/js/custom/adminCustomModal.js"></script>

        <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap4.min.js"></script>
        {{-- <script src="https://cdn.datatables.net/rowreorder/1.2.0/js/dataTables.rowReorder.min.js"></script> --}}
        <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>


        <script>
            Dropzone.options.upload = {
                url: "/admin/flyer/create/adminPhotos",
                paramName: "file",
                maxFilesize: 20,
                autoProccessQueue: false,
                uploadMultiple: true,
                addRemoveLinks: true,
                parallelUploads: 10,
                accept: function(file, done) {

                }
            };

            $('#datatableShowFlyers, #datatableShowUsers, #datatableShowInvoices').DataTable({
                responsive: true,
            } );

            
        </script>

        @include('flash')

    </body>
    </html>