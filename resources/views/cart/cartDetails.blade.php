@extends('layout')

@section('content')

<div class="preloadCart"><img src="/images/loading.gif"></div>

<div class="row checkoutpage">
  <div class="wrap cf">
    <div class="heading cf">
      <h1>My Cart</h1>
      <a href="/flyers" class="continue">Continue Shopping</a>
    </div>
    <div class="cart">
     <ul class="cartWrap">

      <checkout-cart></checkout-cart>

    </ul>
  </div>

  <div class="promoCode"><label for="promo">Have A Promo Code?</label><input type="text" v-model="promoCode" @keydown.enter="promoCodeFunction" name="promo" placholder="Enter Code" />
    <a href="#" @click="promoCodeFunction"  class="btn"></a></div>

    <div class="subtotal cf">
      <ul>
        <li class="totalRow"><span class="label">Subtotal</span><span class="value">$ @{{ total | priceFilter }}</span></li>

        <li class="totalRow"><span class="label">Shipping</span><span class="value">$5.00</span></li>

        <li class="totalRow"><span class="label">Tax</span><span class="value">$4.00</span></li>
        <li class="totalRow final"><span class="label totalLabel">Total</span><span class="value">$ @{{ total | priceFilter }}</span></li>
        <li class="totalRow"><a href="/checkout" @click="checkUserDeliveryAddress" class="btn continue" >Checkout</a></li>
      </ul>
    </div>
  </div>
</div>
@stop