@extends('layout')

@section('content')

<div class="invoices frontend">

<h1 class="invoicesTitle">My Invoices</h1>

    <!-- Main content -->
    <section class="datatableInvoice ">
        <table id="datatableShowInvoices" class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th style="width:20px;">#</th>
                    <th style="width:70px;">Invoice ID</th>
                    <th>Customer Name</th>
                    <th class="visible-desktop" >Customer Email</th>
                    <th class="visible-desktop">Date</th>
                    <th class="visible-desktop">Payment</th>
                    <th class="visible-desktop" style="width: 120px">Payment Method</th>
                </tr>
            </thead>

            <tbody>
                <?php $count = 0;?>
                @foreach($invoices as $invoice)
                <?php $count++; ?>
                <tr>
                    <td>{{$count}}</td>
                    <td style="font-weight:bold"><a href="/invoice-preview/{{$invoice->invoice_id}}">{{$invoice->invoice_id}}</a></td>
                    <td>{{$invoice->name}}</td>
                    <td>{{$invoice->email}}</td>
                    <td>{{$invoice->created_at}}</td>
                    <td>$ {{number_format($invoice->payment_sum, 2)}}</td>
                    <td style="color:red; text-align:center;font-weight:bold">{{ $invoice->payment_method }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </section>
</div>
@stop
