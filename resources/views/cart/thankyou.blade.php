@extends('layout')

@section('content')

<div class="row thankyou center">
	{{-- <h1> Thankyou For your purchase </h1> --}}

	<div class="container" style="border: 7px solid #a6cd56; padding:40px; margin-top: 50px;margin-bottom: 100px">
		<h1 class="display-3 black bold" style="font-size: 70px">Thank You!</h1>
		<img src="http://www.clker.com/cliparts/D/1/e/4/N/X/ok-button-th.png">
		<hr>
		<p class="lead"><strong>Please check your email</strong> for further instructions on how to complete your account setup.</p>
		<hr>
		<p class="lead">
			<p class="lead"><strong>To access your invoice please Access!</strong></p>
			<hr>
			<p class="lead"><i class="fa fa-user" aria-hidden="true"></i> User <i class="fa fa-arrow-right" aria-hidden="true"></i> Invoices!</p>
		</p>
	</div>


</div>
@stop
