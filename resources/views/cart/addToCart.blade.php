<div id="container">
  <div id='cart' class="hideCart">
    <h2>Your Shopping Cart</h2>
    
    <div class="col-md-12" id="shopCartHeader">
        <div class="col-md-2 image">
          <h4>Product Image</h4>
        </div>

        <div class="col-md-3 name">
          <h4>Product Name</h4>
        </div>

        <div class="col-md-2 center unitPrice">
          <h4>Unit Price</h4>
        </div>

        <div class="col-md-2 center qty">
          <h4>Product Qty</h4>
          </div>

          <div class="col-md-2 center totalPrice">
            <h4>Total</h4>
          </div>
          <div class="col-md-1 center deleteIcon"></div>
      </div>

      <ul class='cart-items'>
        <shopping-cart></shopping-cart>
      </ul>

      <div class="row"></div>
      <div class='total'>
        <div class='subtotalTotal'>
          Subtotal
          <span v-if="products.length == 0">@{{ products.length }}</span>
          <span>$ </span><span v-show="products.length > 0">
             @{{ total | priceFilter }}
          </span>
        </div>
        <div class='taxes'>
          Tax
          <span>$0.00</span>
        </div>
        <div class='shipping'>
          Shipping
          <span>$0.00</span>
        </div>
        <div class='finalTotal'>
          Total
          <span v-if="products.length == 0">@{{ products.length }}</span>
          <span>$ </span><span v-show="products.length > 0">
             @{{ total | priceFilter }}
          </span>
        </div>
        <div class="bottomButtons">
          <a href="/checkout" class='checkout'> Check Out</a>
          <a href="/cart-details" class='seeMoreDetails'> View More Details</a>
        </div>
        <p class='error'></p>
      </div>
    </div>
  </div>