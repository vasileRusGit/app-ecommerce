@extends('layout')

@section('content')

<div class="invoice-box">
 <div class="row">
  <div class="col-xs-12">
    <div class="invoice-title">
      <div class="row">
        <div class="col-md-6">
          <img src="/images/ecommerce-logo.png" class="invoicePreviewImage">
        </div>
        <div class="col-md-6">
        <h3 class="pull-right invoicePreviewOrderId">Order # 12345</h3>
        </div>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-xs-6">
        <address>
          <strong>Billed To:</strong><br>
          {{ $invoice->name }}<br>
          {{ $invoice->email }}
        </address>
      </div>
      <div class="col-xs-6 text-right">
        <address>
          <strong>Shipping Address:</strong><br>
          {{ $invoice->country }}<br>
          {{ $invoice->state }}<br>
          {{ $invoice->address }}<br>
          {{ $invoice->zip }}, {{ $invoice->phone }}
        </address>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-6">
        <address>
          <strong>Payment Method:</strong><br>
          <span class="bold" style="color:#a6cd56">{{ $invoice->payment_method }}</span><br>
          @if($invoice->payment_method  == 'Credit Card')
          Visa ending **** 4242<br>
          @endif
        </address>
      </div>
      <div class="col-xs-6 text-right">
        <address>
          <strong>Order Date:</strong><br>
          {{ date('F d, Y h:mA', strtotime( $invoice->created_at )) }}<br><br>
        </address>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="table-responsive">
      <table class="table table-condensed">
        <thead>
          <tr class="heading">
            <td><strong>Item</strong></td>
            <td class="text-center"><strong>Price</strong></td>
            <td class="text-center"><strong>Quantity</strong></td>
            <td class="text-right"><strong>Totals</strong></td>
          </tr>
        </thead>
        <tbody>
          <!-- foreach ($order->lineItems as $line) or some such thing here -->
          <?php $subtotal = 0; ?>
          @foreach($invoiceProducts as $inv)
          <tr>
            <td>{{ $inv->product_name }}</td>
            <td class="text-center">${{ number_format($inv->product_price, 2) }}</td>
            <td class="text-center">{{ $inv->product_qty }}</td>
            <td class="text-right">${{ number_format($inv->product_price * $inv->product_qty, 2) }}</td>
            <?php $subtotal += $inv->product_price * $inv->product_qty; ?>
          </tr>
          @endforeach
          <tr>
            <td class="thick-line"></td>
            <td class="thick-line"></td>
            <td class="thick-line text-center"><strong>Subtotal</strong></td>
            <td class="thick-line text-right">${{ number_format($subtotal, 2) }}</td>
          </tr>
          <tr>
            <td class="no-line"></td>
            <td class="no-line"></td>
            <td class="no-line text-center"><strong>Shipping</strong></td>
            <?php $shippingCosts = 750; ?>
            <td class="no-line text-right">${{ number_format($shippingCosts, 2) }}</td>
          </tr>
          <tr>
            <td class="no-line"></td>
            <td class="no-line"></td>
            <td class="no-line text-center"><strong>Total</strong></td>
            <td class="no-line text-right red bold">${{ number_format($subtotal - $shippingCosts, 2) }}
              <a href="/invoice/pdf/{{ $invoice->invoice_id }}" download="invoice.pdf" ><i class="fa fa-download" aria-hidden="true"></i></a>
            </td>
          </tr>

        </tbody>
      </table>
    </div>
  </div>
</div>
</div>

@stop
