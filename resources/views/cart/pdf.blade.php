<!DOCTYPE html>
<!-- saved from url=(0093)https://htmlpdfapi.com/uploads/attachments/56603852726169116d960100/invoice_1.html?1449146449 -->
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>HTML to API - Invoice</title>
  <link href="./HTML to API - Invoice_files/css" rel="stylesheet" type="text/css">
  <!-- <link rel="stylesheet" href="sass/main.css" media="screen" charset="utf-8"/> -->
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  
  <style type="text/css">
      html, body, div, span, applet, object, iframe,
    h1, h2, h3, h4, h5, h6, p, blockquote, pre,
    a, abbr, acronym, address, big, cite, code,
    del, dfn, em, img, ins, kbd, q, s, samp,
    small, strike, strong, sub, sup, tt, var,
    b, u, i, center,
    dl, dt, dd, ol, ul, li,
    fieldset, form, label, legend,
    table, caption, tbody, tfoot, thead, tr, th, td,
    article, aside, canvas, details, embed,
    figure, figcaption, footer, header, hgroup,
    menu, nav, output, ruby, section, summary,
    time, mark, audio, video {
      margin: 0;
      padding: 0;
      border: 0;
      font: inherit;
      font-size: 100%;
      vertical-align: baseline;
    }

    html {
      line-height: 1;
    }

    ol, ul {
      list-style: none;
    }


    caption, th, td {
      text-align: left;
      font-weight: normal;
      vertical-align: middle;
    }

    q, blockquote {
      quotes: none;
    }
    q:before, q:after, blockquote:before, blockquote:after {
      content: "";
      content: none;
    }

    a img {
      border: none;
    }

    article, aside, details, figcaption, figure, footer, header, hgroup, main, menu, nav, section, summary {
      display: block;
    }

    body {
      font-family: 'Source Sans Pro', sans-serif;
      font-weight: 300;
      font-size: 12px;
      margin: 0;
      padding: 0;
    }
    body a {
      text-decoration: none;
      color: inherit;
    }
    body a:hover {
      color: inherit;
      opacity: 0.7;
    }
    body .container {
      min-width: 500px;
      margin: 0 auto;
      padding: 0 20px;
    }
    body .clearfix:after {
      content: "";
      display: table;
      clear: both;
    }
    body .left {
      float: left;
    }
    body .right {
      float: right;
    }
    body .helper {
      display: inline-block;
      height: 100%;
      vertical-align: middle;
    }
    body .no-break {
      page-break-inside: avoid;
    }

    header {
      margin-top: 20px;
      margin-bottom: 50px;
    }
    header figure {
      float: left;
      width: 60px;
      height: 60px;
      margin-right: 10px;
      background-color: #8BC34A;
      /*border-radius: 50%;*/
      text-align: center;
    }
    header figure img {
      margin-top: 13px;
    }
    header .company-address {
      float: left;
      max-width: 150px;
      line-height: 1.7em;
    }
    header .company-address .title {
      color: #8BC34A;
      font-weight: 400;
      font-size: 1.5em;
      text-transform: uppercase;
    }
    header .company-contact {
      float: right;
      height: 60px;
      padding: 0 10px;
      background-color: #8BC34A;
      color: white;
    }
    header .company-contact span {
      display: inline-block;
      vertical-align: middle;
    }
    header .company-contact .circle {
      width: 20px;
      height: 20px;
      background-color: white;
      /*border-radius: 50%;*/
      text-align: center;
    }
    header .company-contact .circle img {
      vertical-align: middle;
    }
    header .company-contact .phone {
      height: 100%;
      margin-right: 20px;
    }
    header .company-contact .email {
      height: 100%;
      min-width: 100px;
      text-align: right;
    }

    section .details {
      margin-bottom: 55px;
    }
    section .details .client {
      width: 50%;
      line-height: 20px;
    }
    section .details .client .name {
      color: #8BC34A;
    }
    section .details .data {
      width: 50%;
      text-align: right;
    }
    section .details .title {
      margin-bottom: 15px;
      color: #8BC34A;
      font-size: 3em;
      font-weight: 400;
      text-transform: uppercase;
    }
    section table {
      width: 100%;
      /*border-collapse: collapse;*/
      /*border-spacing: 0;*/
      font-size: 0.9166em;
    }
    section table .qty, section table .unit, section table .total {
      width: 15%;
    }
    section table .desc {
      width: 55%;
    }
    section table thead {
      display: table-header-group;
      vertical-align: middle;
      /*border-color: inherit;*/
    }
    section table thead th {
      padding: 5px 10px;
      background: #8BC34A;
      /*border-bottom: 5px solid #FFFFFF;*/
      /*border-right: 4px solid #FFFFFF;*/
      text-align: right;
      color: white;
      font-weight: 400;
      text-transform: uppercase;
    }
    section table thead th:last-child {
      border-right: none;
    }
    section table thead .desc {
      text-align: left;
    }
    section table thead .qty {
      text-align: center;
    }
    section table tbody td {
      padding: 10px;
      background: #E8F3DB;
      color: #777777;
      text-align: right;
      /*border-bottom: 5px solid #FFFFFF;*/
      /*border-right: 4px solid #E8F3DB;*/
    }
    section table tbody td:last-child {
      border-right: none;
    }
    section table tbody h3 {
      margin-bottom: 5px;
      color: #8BC34A;
      font-weight: 600;
    }
    section table tbody .desc {
      text-align: left;
    }
    section table tbody .qty {
      text-align: center;
    }
    section table.grand-total {
      margin-bottom: 45px;
    }
    section table.grand-total td {
      padding: 5px 10px;
      border: none;
      color: #777777;
      text-align: right;
    }
    section table.grand-total .desc {
      background-color: transparent;
    }
    section table.grand-total tr:last-child td {
      font-weight: 600;
      color: #8BC34A;
      font-size: 1.18181818181818em;
    }
  .invoice-box{
    max-width:800px;
    margin:auto;
    padding:30px;
    /*border:1px solid #eee;*/
    box-shadow:0 0 10px rgba(0, 0, 0, .15);
    font-size:16px;
    line-height:24px;
    font-family:'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    color:#555;
    margin-top: 30px;
    margin-bottom: 50px;
  }

  .invoice-box table{
    width:100%;
    line-height:inherit;
    text-align:left;
  }

  .invoice-box table td{
    padding:5px;
    vertical-align:top;
  }

  .invoice-box table tr td:nth-child(2){
    text-align:right;
  }

  .invoice-box table tr.top table td{
    padding-bottom:10px;
  }

  .invoice-box table tr.top table td.title{
    font-size:45px;
    line-height:45px;
    color:#333;
  }

  .invoice-box table tr.information table td{
    padding-bottom:40px;
  }

  .invoice-box table tr.heading td{
    background:#eee;
    /*border-bottom:1px solid #ddd;*/
    font-weight:bold;
  }
  .invoice-box table tr.headingTotal td{
    border-top:5px solid #a6cd56;
    font-weight:bold;
    font-size: 22px;
  }

  .invoice-box table tr.details td{
    padding-bottom:20px;
  }

  .invoice-box table tr.item td{
    /*border-bottom:1px solid #eee;*/
  }

  .invoice-box table tr.item.last td{
    border-bottom:none;
  }

  .invoice-box table tr.total td:nth-child(2){
    /*border-top:2px solid #eee;*/
    font-weight:bold;
  }

  @media only screen and (max-width: 600px) {
    .invoice-box table tr.top table td{
      width:100%;
      display:block;
      text-align:center;
    }

    .invoice-box table tr.information table td{
      width:100%;
      display:block;
      text-align:center;
    }
  }
  </style>
</head>

<body>
  

<div class="invoicePDF">
    <div class="invoice-box">
  <table cellpadding="0" cellspacing="0">
    <tr class="top">
      <td colspan="3">
        <table>
          <tr>
            <td class="title">
              <img src="../public/images/ecommerce-logo.png" style=" margin-top:-60px;">
            </td>
            <td style="float: right">
              Invoice #: {{ $invoice->invoice_id }}<br>
              Created: {{ $invoice->created_at }}<br>
              Due: February 1, 2015
            </td>
          </tr>
        </table>
      </td>
    </tr>

    <tr class="information">
      <td colspan="3">
        <table>
          <tr>
            <td>      
              Next Step Webs, Inc.<br>
              12345 Sunny Road<br>
              Sunnyville, TX 12345
            </td>

            <td>      
              Acme INC <br>
              {{ $invoice->address }}, 
              {{ $invoice->country }}, 
              {{ $invoice->state }}, 
              {{ $invoice->zip }}<br>
              {{ $invoice->name }}<br>
              {{ $invoice->email }}
            </td>
          </tr>
        </table>
      </td>
    </tr>

    <tr class="heading" >
      <td>Product Name</td>
      <td style="text-align:center">Quantity</td>
      <td style="text-align:right">Price</td>
    </tr>

    @foreach($invoiceProducts as $inv)
    <tr class="details">
      <td>{{ $inv->product_name }}</td>
      <td style="text-align:center">{{ $inv->product_qty }}</td>
      <td style="text-align:right">${{ number_format($inv->product_price, 2) }}</td>
    </tr>
    @endforeach

    <tr class="heading">
      <td>Payment Method</td>
      <td></td>
      <td></td>
    </tr>

    <tr class="details" style="margin-top:20">
      <td style="color: green;font-weight: bold;">{{ $invoice->payment_method }}</td>
    </tr>

    <tr class="heading">
      <td>Item</td>
      <td></td>
      <td style="text-align:right">Price</td>
    </tr>

    <?php 
    $subtotal = 0;
    foreach($invoiceProducts as $invo){
      $subtotal += $invo->product_qty * $invo->product_price;
    }
    ?>

    <tr class="item">
      <td>Subtotal</td>
      <td></td>
      <td style="text-align:right">${{ number_format($subtotal, 2) }}</td>
    </tr>

    <tr class="item">
      <td style="padding-bottom: 30px;">Tax</td>
      <td></td>
      <td style="text-align:right">$75.00</td>
    </tr>

    <tr class="headingTotal">
      <td style="padding-top: 20px">Total</td>
      <td></td>
      <td style="text-align:right; color:red; padding-top: 20px;">
        ${{ number_format(($subtotal + 75), 2)}}
      </td>
    </tr>
  </table>
</div>
  </div>

</body></html>