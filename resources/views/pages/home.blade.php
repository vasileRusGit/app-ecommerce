@extends('layout')

@section('sliderPosition')
<div class="header-content">
    <div class="homeMainPictureContent">
        <h1>
            Online Ecommerce Store
        </h1>
        <h2>
            Easy way to set up a new store for your products
        </h2>
        @if(\Auth::user())
        <a href="/flyers/create">
            <button class="btn btn-lg" id="outlineButton" type="button">
                Add New Product
            </button>
        </a>
        <button class="btn btn-lg" id="filledButton" type="button">
            GETING STARTED
        </button>
        @else
        <a href="/login">
            <button class="btn btn-lg" id="outlineButton" type="button">
                GETING STARTED
            </button>
        </a>
        <a href="/register">
            <button class="btn btn-lg" id="filledButton" type="button">
                REGISTER
            </button>
        </a>
        @endif
    </div>
</div>
@stop

@section('content')
<div class="featuredFlyers">
    @include('flyers.featured')
</div>

{{-- about products --}}
<div class="aboutProduct">
    <h1>
        About Our Products
    </h1>
    <div class="row">
        <div class="col-md-4">
            <img src="/images/no-image.png">
            </img>
        </div>
        <div class="col-md-8 aboutText">

            <p>
                Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.
<br>

                Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet.
            </p>
        </div>
    </div>
</div>
{{-- end about products --}}

{{-- partners --}}
<div class="partners">
    <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-1" id="p1">
            <img src="/images/brand/acer.png"/>
        </div>
        <div class="col-md-1" id="p2">
            <img src="/images/brand/asics.png"/>
        </div>
        <div class="col-md-1" id="p3">
            <img src="/images/brand/bmw.png"/>
        </div>
        <div class="col-md-1" id="p4">
            <img src="/images/brand/coca_cola.jpg"/>
        </div>
        <div class="col-md-1" id="p5">
            <img src="/images/brand/dell.png"/>
        </div>
        <div class="col-md-1" id="p6">
            <img src="/images/brand/lucky_brand.png"/>
        </div>
        <div class="col-md-1" id="p7">
            <img src="/images/brand/mail_chimp.png"/>
        </div>
        <div class="col-md-1" id="p8">
            <img src="/images/brand/mercedez.png"/>
        </div>
        <div class="col-md-1" id="p9">
            <img src="/images/brand/rolex.png"/>
        </div>
        <div class="col-md-1" id="p10">
            <img src="/images/brand/toshiba.png"/>
        </div>
        <div class="col-md-1">
        </div>
    </div>
</div>
{{-- end partners --}}

{{-- testimonials --}}
<div class="testimonials">
    <h1>
        Testimonials
    </h1>
    <div class="row">
        <div class="col-md-4">
            <div class="testimonial-quote group right">
                <img src="https://placehold.it/120x120">
                    <div class="quote-container">
                        <div>
                            <div class="ejblockquote">
                                <p>
                                    Overall, fantastic! I'd recommend them to anyone looking for a creative, thoughtful, and professional team.”
                                </p>
                            </div>
                            <cite>
                                <span>
                                    Kristi Bruno
                                </span>
                                <br>
                                    Social Media Specialist
                                    <br>
                                        American College of Chest Physicians
                                    </br>
                                </br>
                            </cite>
                        </div>
                    </div>
                </img>
            </div>
        </div>
        <div class="col-md-4">
            <div class="testimonial-quote group right">
                <img src="https://placehold.it/120x120">
                    <div class="quote-container">
                        <div>
                            <div class="ejblockquote">
                                <p>
                                    Overall, fantastic! I'd recommend them to anyone looking for a creative, thoughtful, and professional team.”
                                </p>
                            </div>
                            <cite>
                                <span>
                                    Kristi Bruno
                                </span>
                                <br>
                                    Social Media Specialist
                                    <br>
                                        American College of Chest Physicians
                                    </br>
                                </br>
                            </cite>
                        </div>
                    </div>
                </img>
            </div>
        </div>
        <div class="col-md-4">
            <div class="testimonial-quote group right">
                <img src="https://placehold.it/120x120">
                    <div class="quote-container">
                        <div>
                            <div class="ejblockquote">
                                <p>
                                    Overall, fantastic! I'd recommend them to anyone looking for a creative, thoughtful, and professional team.”
                                </p>
                            </div>
                            <cite>
                                <span>
                                    Kristi Bruno
                                </span>
                                <br>
                                    Social Media Specialist
                                    <br>
                                        American College of Chest Physicians
                                    </br>
                                </br>
                            </cite>
                        </div>
                    </div>
                </img>
            </div>
        </div>
    </div>
</div>
{{-- end testimonials --}}
@stop
