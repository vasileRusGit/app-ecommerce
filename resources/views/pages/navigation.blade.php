<div id="header-wrap">

    <div id="header">

        <nav class="navbar navbar-default navbar-static-top navigation-bar is-visible" status="toggle">
            <div class="container-fluid">
                <div class="navbar-header">

                <div class="row navigationLogo">
                        <a class="navbar-brand" href="/"></a>
                    </div>
                    <div class="row toggleSearchCartRow">
                        <!-- Collapsed Hamburger -->
                        <button type="button" class="navbar-toggle collapsed" id="navbarToggle" data-toggle="collapse"
                        data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>


                    <a>
                        {!! \Form::open(['route' => 'search.flyers', 'class' => 'login-form','method' => 'GET', 'role' => 'search']) !!}
                        {!! Form::text('street', Request::get('street'), ['id'=>'s', 'placeholder' => 'search']) !!}
                        {!! \Form::close() !!}
                    </a>

                    <div class="preloadNavigationCart"><img src="/images/loading.gif"></div>

                    <a href="#" id="shippingCart" ><span id="testLoaderCart" class='cart-quantity empty'><div class="navigationShopCart">@{{ products.length }}</div></span><i class="fa fa-shopping-cart shippingCart" aria-hidden="true"></i></a>
                </div>

            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    <li><a href="/">Home</a></li>
                    <li><a href="/flyers">Products</a></li>
                    <li><a href="/favorite-flyers">Favorites</a></li>
                    @if(\Auth::check())
                    <li><a href="/invoice">Invoices</a></li>
                    @endif
                    <li id="liveChat"><a href="/chat">Live Chat</a></li>
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right loginNavbarArea" >
                    <!-- Authentication Links -->
                    @if(Auth::guest())
                    <li>
                        {!! \Form::open(['route' => 'search.flyers', 'class' => 'login-form','method' => 'GET', 'role' => 'search']) !!}
                        {!! Form::text('street', Request::get('street'), ['id'=>'s', 'placeholder' => 'search']) !!}
                        {!! \Form::close() !!}
                    </li>
                    {{-- <li><input href="#" type="search" id="s" ></li> --}}
                    <li>

                    <div class="preloadNavigationCart"><img src="/images/loading.gif"></div>

                    <a href="#" id="shippingCart" ><span class='cart-quantity empty'><div class="navigationShopCart">@{{ products.length }}</div></span><i class="fa fa-shopping-cart shippingCart" aria-hidden="true"></i></a></li>
                    <li><a href="{{ url('/login') }}">Login</a></li>
                    <li><a href="{{ url('/register') }}">Register</a></li>
                    @else
                    <!-- User Account Menu -->
                    {{--                     <li><a href="#"><i class="fa fa-shopping-cart shippingCart" aria-hidden="true"></i><span class='cart-quantity empty'>0</span>  Cart</a></li> --}}
                    <li>
                        {!! \Form::open(['route' => 'search.flyers', 'class' => 'login-form','method' => 'GET', 'role' => 'search']) !!}
                        {!! Form::text('street', Request::get('street'), ['id'=>'s', 'placeholder' => 'search']) !!}
                        {!! \Form::close() !!}
                    </li>
                    <li>
                    <div class="preloadNavigationCart"><img src="/images/loading.gif"></div>
                    
                    <a href="javascript:void(0)" id="shippingCart">
                    <span class='cart-quantity empty'><div class="navigationShopCart">@{{ products.length }}</div></span>
                    <i class="fa fa-shopping-cart shippingCart" aria-hidden="true"></i></a>
                    </li>

                    <li class="dropdown user user-menu" style="margin-top: 2px;">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="margin-top: -20px;">
                            <!-- The user image in the navbar-->
                            @if(Auth::user()->image_path == null)

                            <img src="/images/no-user-image.png" id="imageHolderUserAccountHeader"/>
                            @else
                            <img src="/{{ Auth::user()->image_path }}" id="imageHolderUserAccountHeader" />
                            @endif
                        </a>
                        <ul class="dropdown-menu" id="dropdownMenuUl" role="menu">
                            <li id="userNameLiSpan"><span class="userNameDropdown">{{Auth::user()->name}}</span></li>
                            <li>
                                <a href="/admin">Admin Panel</a>
                            </li>
                            {{-- @endif --}}
                            <li>
                                <a href="/user/edit/account/{{Auth::user()->id}}">Edit Account</a>
                            </li>
                            <li>
                                <a href="{{ url('/logout') }}"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ url('/logout') }}" method="POST">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>
                @endif
            </ul>
        </div>
    </div>
</nav>
</div>
</div>