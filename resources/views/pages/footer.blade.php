<footer class="nb-footer">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="about">
				<img src="/images/ecommerce-logo.png" class="img-responsive center-block" alt="" id=footerImage>
				<div class="social-media">
						<ul class="list-inline">
							<li><a href="http://www.nextbootstrap.com/" title=""><i class="fa fa-facebook"></i></a></li>
							<li><a href="http://www.nextbootstrap.com/" title=""><i class="fa fa-twitter"></i></a></li>
							<li><a href="http://www.nextbootstrap.com/" title=""><i class="fa fa-google-plus"></i></a></li>
							<li><a href="http://www.nextbootstrap.com/" title=""><i class="fa fa-linkedin"></i></a></li>
						</ul>
					</div>
					<p>Bootstrap Footer example snippets with CSS, Javascript and HTML. Code example of bootstrap-3 footer using HTML, Javascript, jQuery, and CSS. 5 Beautiful and Responsive Footer Templates. Pin a fixed-height footer to the bottom of the viewport in desktop browsers with this custom HTML and CSS.</p>
				</div>
			</div>
		</div>
	</div>

	<section class="copyright" style="text-align: center">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<p>Copyright &copy; 2017 Online Market Store.</p>
				</div>
			</div>
		</div>
	</section>
</footer>