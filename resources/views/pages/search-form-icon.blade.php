<div id="container">
    <div id="searchFormIcon" class="searchFormIcon hideSearchForm">
        <div  class="form">

            {!! \Form::open(['route' => 'search.flyers.icon', 'class' => 'login-form-icon','method' => 'GET', 'role' => 'search']) !!}

            <div class="row">
                <div class="col-md-3">
                    {!! Form::text('productID', Request::get('productIDIcon'), ['class'=>'form-control form-input', 'placeholder' => 'Product ID']) !!}
                </div>
                <div class="col-md-3">
                    <span class="input-group-btn">
                     <button id="searchFormButtonIcon" class="btn btn-default" type="submit">Search Icon</button>
                     </span>
                 </div>
            </div><!-- end row -->
         {!! \Form::close() !!}
        </div>
    </div>
</div>