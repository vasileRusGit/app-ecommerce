<div class="form">

    {!! \Form::open(['route' => 'search.flyers', 'class' => 'searchForm','method' => 'GET', 'role' => 'search']) !!}

    <div class="row">
        <div class="col-md-3">
            {!! Form::text('productID', Request::get('productID'), ['class'=>'form-control form-input', 'placeholder' => 'Product ID']) !!}
        </div>
        <div class="col-md-3 form-group searchImputFromPrice">
            <select id="searchFormFrom" class="form-control" name="price-from">
                @for($flyers = 0; $flyers <= 10; $flyers++)
                    <option style="font-size:13pt; height:30px;" value="{{ $flyers * 2000 }}"
                        @if(Request::get('price-from') !== null && (Request::get('price-from') == ($flyers * 2000))) selected="selected" @endif>{{ $flyers * 2000 }} €
                    </option>
                @endfor
            </select>
        </div>
        <div class="col-md-3">
            {!! Form::text('street', Request::get('street'), ['class'=>'form-control form-input', 'placeholder' => 'Street']) !!}
        </div>
        <div class="col-md-3">
            {!! Form::text('city', Request::get('city'), ['class'=>'form-control form-input', 'placeholder' => 'City']) !!}
        </div>
    </div><!-- end row -->

    <div class="row">
        <div class="col-md-3 description">
            {!! Form::text('description', Request::get('description'), ['class'=>'form-control form-input', 'placeholder' => 'Description']) !!}
        </div>
        <div class="col-md-3 form-group searchImputToPrice">
            <select id="searchFormTo" class="form-control" name="price-to" >
                <option value="9999999999999">Peste 20000 €</option>
                @for($flyers = 10; $flyers >= 1; $flyers--)
                    <option value="{{ $flyers * 2000 }}"
                        @if(Request::get('price-to') !== null && (Request::get('price-to') == ($flyers * 2000))) selected="selected" @endif>{{ $flyers * 2000 }} €
                    </option>
                @endfor
            </select>
        </div>
        <div class="col-md-3">
            <span class="input-group-btn">
               <button id="searchFormButton" class="btn btn-default" type="submit">Search</button>
           </span>
       </div>
   </div><!-- end row -->

   {!! \Form::close() !!}

</div>