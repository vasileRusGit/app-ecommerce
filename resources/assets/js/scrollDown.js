$(document).ready(function() {
    // var url = window.location;
    // // Will only work if string in href matches with location
    // $('#app-navbar-collapse ul a[href="' + url + '"]').parent().addClass('active');
    // // Will also work for relative and absolute hrefs
    // $('#app-navbar-collapse ul a').filter(function () {
    //     return this.href == url;
    // }).parent().addClass('active');
    // on page load scroll down to see the last messages
    $('div').animate({scrollTop: 999999}, 'slow');

    // on message receve scroll down
    $(".chat-log").on('DOMSubtreeModified', function() {
        $('div').animate({scrollTop: 999999}, 'slow');
    });
});