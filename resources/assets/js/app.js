require('./bootstrap');
import moment from 'moment'
var VueCookie = require('vue-cookie');
Vue.use(VueCookie);
Vue.component('chat-message', require('./components/ChatMessage.vue'));
Vue.component('chat-log', require('./components/ChatLog.vue'));
Vue.component('chat-composer', require('./components/ChatComposer.vue'));

Vue.component('shopping-cart', require('./components/ShoppingCart.vue'));
Vue.component('checkout-cart', require('./components/CheckoutCart.vue'));

Vue.component('checkout-progress', require('./components/CheckoutProgress.vue'));

Vue.filter('formatDate', function(value) {
	if (value) {
		return moment(String(value)).add(3, 'hours').format('h:mm A');
	}
});
Vue.filter('formatDateOnEmit', function(value) {
	if (value) {
		return moment(value).format('h:mm A')
	}
});
Vue.filter('formatSeconds', function(value) {
	if (value) {
		var time = parseInt(value.replace(/\D/g, ''));
		return time;
	}
});
Vue.filter('priceFilter', function(value) {
	if (value) {
		var price = value.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
		return price;
	}
});
const app = new Vue({
	el: '#app-vue-chat',
	data() {
		return {
            messages: [],
            usersInRoom: [],
            products: [],
            userLoggedIn: '',
            promoCode: ''
        }
    },
    methods: {
      addMessage(message, show) {
        // console.log(message);
         if (message.message !== undefined) {
                // add to exsiting messages
                this.messages.push(message, show);
                // persist to the database
                axios.post('/messages', message).then(response => {
                    // scroll down to the bottom of the chat
                    $('div').animate({
                    	scrollTop: 999999
                    }, 'slow');
                });
            }
        },
        addToCartVue() {
        	let productImage = $.trim($('.imagePreview img').attr('src'));
        	let productID = $.trim($('#productIdAllFlyers').attr('value'));
        	let productName = $.trim($('#product-spec .titleForPencil').text());
        	let productPrice = $('#productDetails > h3.sale').text().replace('$', '').replace(/[,]+/g, "").trim();
        	let productQuantity = $('.productQtyProductPage input').val();;
        	let objectvalue = [];
        	for (var key in this.products) {
        		objectvalue.push(this.products[key]['product_name']);
        	}
        	if (objectvalue.includes(productName)) {
        		swal('Attention!', 'You already added this product into the cart!', 'info')
        	} else {
        		if (this.userLoggedIn == true) {
        			this.addProductToCartUserLoggedIn(productID, productName, productPrice, productQuantity, productImage);
        		} else {
        			this.addProductToCartUserNotLoggedIn(productID, productName, productPrice, productQuantity, productImage);
        		}
        	}
        },

        promoCodeFunction(){
            let coupon = ['discount10', 'discount20', 'dicount30', 'discount40', 'discount50'];
            if(coupon.indexOf(this.promoCode)){
                alert('not valid coupon');
            }else{
                for (let i = 0; i < this.products.length; i++) {
                    this.products[i].product_price = this.products[i].product_price - 10;
                }
            }
            this.promoCode = '';
        },

        addToCartVueAllFlyers(number){
           let productImage = $.trim($('.' + number + ' img').attr('src'));
           let productID = $.trim($('#productIdAllFlyers').attr('value'));
           let productName = $.trim($('.' + number + ' .title').text());
           let productPrice = $('.'+number+' .sub_title').text().substr(1).replace(/[,]+/g, "").trim();
           let productQuantity = $('.productQtyAllProductPage .'+number+'.input-number').val();
           console.log(productQuantity);
           let objectvalue = [];

           for (var key in this.products) {
              objectvalue.push(this.products[key]['product_name']);
          }
          if (objectvalue.includes(productName)) {
              swal('Attention!', 'You already added this product into the cart!', 'info')
          } else {
              if (this.userLoggedIn == true) {
                 this.addProductToCartUserLoggedIn(productID, productName, productPrice, productQuantity, productImage);
             } else {
                 this.addProductToCartUserNotLoggedIn(productID, productName, productPrice, productQuantity, productImage);
             }
         }
     },

     addProductToCartUserLoggedIn(productID, productName, productPrice, productQuantity, productImage){
       this.products.push({
          product_image_path: productImage,
          product_name: productName,
          product_price: productPrice,
          product_qty: productQuantity,
          productID: productID
      });

            // save to database the productd added to cart
            $.ajax({
            	type: 'POST',
            	url: '/add-product-to-shopping-cart',
            	data: {
            		productName: productName,
            		productPrice: productPrice,
            		productQuantity: productQuantity,
            		productImage: productImage,
            		productID: productID
            	},
            	success: function(raspunsDeLaAjax) {}
            });
        },

        addProductToCartUserNotLoggedIn(productID, productName, productPrice, productQuantity, productImage){
        	this.products.push({
        		product_image_path: productImage,
        		product_name: productName,
        		product_price: productPrice,
        		product_qty: productQuantity,
        		productID: productID
        	});

        	this.$cookie.set('cart', JSON.stringify(this.products), 1);
        },

        // show more about checkout page products
        deleteShopItem(productname){
				// delete the product that have the specified name
				for(var i = 0; i < this.parent_products.products.length; i++) {
					var obj = this.parent_products.products[i];

					if((obj.product_name === undefined) || (obj.product_name === null)){
						this.parent_products.products.splice(i, 1);
						i--;
					} else if(productname.indexOf(obj.product_name) !== -1) {
						this.parent_products.products.splice(i, 1);
						i--;
					} 
				}
				if(this.parent_logged_in == false){
					// delete from cookie
					let cart = JSON.parse(this.$cookie.get('cart'));
					for(let i = 0 ; i <= cart.length - 1; i++){
						var obj = cart[i];

						if(productname.indexOf(obj.product_name) !== -1) {
							cart.splice(i, 1);
							i--;
						} 
					}
					this.$cookie.delete('cart');
					this.$cookie.set('cart', JSON.stringify(cart), 1);
				}else{
					//delete fro mdb
					$.ajax({
						type: 'DELETE',
						url: '/remove-product-from-shopping-cart',
						data: {
							productName: productname,
						},
						success: function (raspunsDeLaAjax) {

						}
					});
				}
			},

		},

		created() {
    	// check to see if user is logged includes
    	this.userLoggedIn = document.querySelector('#token').getAttribute('value');

    	// return products form the cart
    	if (this.userLoggedIn == true) {
    		axios.get('/cart-products').then(response => {
    			this.products = response.data;
    		});
    	} else {
    		let cart = JSON.parse(this.$cookie.get('cart'));
    		for(let i = 0 ; i <= cart.length - 1; i++){
    			let prod_name = cart[i].product_name;
    			let prod_price = cart[i].product_price;
    			let prod_qty = cart[i].product_qty;
    			let prod_image = cart[i].product_image_path;
    			let prod_id = cart[i].productID;

    			if((prod_name === undefined) || (prod_name === null)){
    				this.products = [];
    			}else{
    				this.products.push({
    					product_name: prod_name,
    					product_price: prod_price,
    					product_qty: prod_qty,
    					product_image_path: prod_image,
    					productID: prod_id
    				});
    			}
    		}
    	}

    	// return messages
    	axios.get('/messages').then(response => {
    		this.messages = response.data;
    	});
    	Echo.join('chatroom').here((users) => {
    		this.usersInRoom = users;
    	}).joining((user) => {
    		this.usersInRoom.push(user);
    	}).leaving((user) => {
    		this.usersInRoom = this.usersInRoom.filter(u => u != user)
    	}).listen("MessagePosted", (e) => {
            this.messages.push({
            	message: e.message.message,
            	user: e.user,
            });
        });
    },

    computed: {
    	total() {
    		let sum = 0;
    		for (let i = 0; i < this.products.length; i++) {
    			sum += this.products[i].product_price * this.products[i].product_qty;
    		}
    		return sum;
    	},
    },
});