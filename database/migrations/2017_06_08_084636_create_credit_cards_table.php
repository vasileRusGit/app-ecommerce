<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credit_cards', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->text('card_number_1')->nullable();
            $table->text('card_number_2')->nullable();
            $table->text('card_number_3')->nullable();
            $table->text('card_number_4')->nullable();
            $table->text('card_holder')->nullable();
            $table->text('card_expiration_month')->nullable();
            $table->text('card_expiration_year')->nullable();
            $table->text('card_ccv')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credit_cards');
    }
}
