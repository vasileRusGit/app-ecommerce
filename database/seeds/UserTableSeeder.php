<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$role_admin = Role::where('name', 'Admin')->first();
    	$role_employee = Role::where('name', 'Employee')->first();
    	$role_user = Role::where('name', 'User')->first();
        $role_guest = Role::where('name', 'Guest')->first();

        $admin = new User();
        $admin->name = 'Admin';
        $admin->email = 'admin@admin.com';
        $admin->password = bcrypt('123456');
        $admin->save();
        $admin->roles()->attach($role_admin);

        $employee = new User();
        $employee->name = 'Employee';
        $employee->email = 'employee@employee.com';
        $employee->password = bcrypt('123456');
        $employee->save();
        $employee->roles()->attach($role_employee);

        $user = new User();
        $user->name = 'User';
        $user->email = 'user@user.com';
        $user->password = bcrypt('123456');
        $user->save();
        $user->roles()->attach($role_user);

        $guest = new User();
        $guest->name = 'Guest';
        $guest->email = 'guest@guest.com';
        $guest->password = bcrypt('123456');
        $guest->save();
        $guest->roles()->attach($role_guest);
    }
}
