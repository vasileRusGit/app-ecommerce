<?php

//homepage
Route::get('/', [
    'as'   => 'home',
    'uses' => 'FlyersController@homeMethod',
    function () {
        return view('pages.home');
    },
    ]);


// shopping cart
Route::get('/checkout', [
    'as'   => 'checkout',
    'uses' => 'ShoppingCartController@checkout',
    ]);
Route::get('/cart-details', [
    'as'   => 'cart-details',
    'uses' => 'ShoppingCartController@cartDetails',
    ]);
Route::get('/add-to-cart', [
    'as'   => 'add.to.cart',
    'uses' => 'ShoppingCartController@addToCart',
    ]);
Route::get('/shopping-cart', [
    'as'   => 'shopping.cart',
    'uses' => 'ShoppingCartController@shoppingCart',
    ]);
Route::get('/cart-products', [
    'as'   => 'cart.products',
    'uses' => 'ShoppingCartController@cartProducts',
    ]);
Route::post('/add-product-to-shopping-cart', [
    'as'   => 'add.to.shopping.cart',
    'uses' => 'ShoppingCartController@addProductToShoppingCart',
    ]);
Route::post('/increment-product-qty', [
    'as'   => 'increment.product.qty',
    'uses' => 'ShoppingCartController@incrementProductQty',
    ]);
Route::post('/decrement-product-qty', [
    'as'   => 'decremenet.product.qty',
    'uses' => 'ShoppingCartController@decrementProductQty',
    ]);
Route::delete('/remove-product-from-shopping-cart', [
    'as'   => 'remove.from.shopping.cart',
    'uses' => 'ShoppingCartController@deleteProductFromShoppingCart',
    ]);
Route::post('/submit-checkout-form', [
    'as'   => 'submit.checkout.form',
    'uses' => 'ShoppingCartController@submitCheckoutForm',
    ]);
Route::get('/thankyou-page', [
    'as'   => 'thankyou.page',
    'uses' => 'ShoppingCartController@thankyou',
    ]);
Route::post('/accept-payment', [
    'as'   => 'accept.payment',
    'uses' => 'ShoppingCartController@acceptPayment',
    ]);
Route::get('/user-delivery-address', [
    'as'   => 'user.delivery.address', 
    'uses' => 'ShoppingCartController@userDeliveryAddress',
    ]);
Route::get('/user-credit-card-informations', [
    'as'   => 'user.credit.card.information', 
    'uses' => 'ShoppingCartController@userCreditCardInformation',
    ]);
Route::get('/user-email', [
    'as'   => 'user.email', 
    'uses' => 'ShoppingCartController@userEmail',
    ]);


// INVOICE
Route::get('/invoice', [
    'as'   => 'invoice.page',
    'uses' => 'ShoppingCartController@showInvoices',
    ])->middleware('auth');
Route::get('/invoice-preview/{invoice}', [
    'as'   => 'invoice.page',
    'uses' => 'ShoppingCartController@previewInvoice',
    ])->middleware('auth');
Route::get('/invoice/pdf/{invoice}', 'PDFController@pdf')->middleware('auth');


//AUTHENTIFICATION
Auth::routes();
Route::get('/user/edit/account/{user}', [
    'as'   => 'user.edit.account',
    'uses' => 'UserController@editUserAccount',
    ]);
Route::put('/user/update/account/{user}', [
    'as'   => 'user.update.account',
    'uses' => 'UserController@updateUserAccount',
    ]);
//ajax grid and list
Route::post('/grid-list', [
    'uses' => 'FlyersController@ajaxCall',
    ]);
//ajax filter
Route::get('/filter/{selectedFilter}', [
    'as'   => 'select.filter',
    'uses' => '\App\FilterFlyers@ajaxCallGetFilterSelectedValue']);
Route::get('/search/{selectedFilter}', [
    'as'   => 'select.search.filter',
    'uses' => '\App\FilterFlyers@ajaxCallGetSearchFilterSelectedValue']);


//FLYERS
Route::resource('flyers', 'FlyersController');
Route::get('flyers/create', 'FlyersController@createFlyer');
Route::get('/flyers', [
    'as' => 'all.flyers', 'uses' => 'FlyersController@showAllFlyers']);
Route::post('/sendFlyerIdFromTheModal', [
    'as' => 'all.flyers.modal.flyer.id', 'uses' => 'FlyersController@showAllFlyers']);
Route::get('{zip}/{street}', 'FlyersController@showIndividualFlyer');


// CREATE FLYERS
Route::post('flyers/create/photos', ['as' => 'flyer.create.photo', 'uses' => 'FlyersController@flyerCreatePhoto']);
Route::post('flyers/{flyer}/edit/photos', ['as' => 'flyer.edit.photo', 'uses' => 'FlyersController@flyerEditPhoto']);


// FAVORITES
Route::get('/favorite-flyers', [
    'as'   => 'favorite-flyers',
    'uses' => '\App\FavoriteFlyersClass@showFavoriteFlyers']);
Route::post('/addToFavorite', [
    'as'   => 'add.to.favorite',
    'uses' => '\App\FavoriteFlyersClass@addFlyerToFavorite']);
Route::delete('/deleteFromFavorite', [
    'as'   => 'delete.from.favorite',
    'uses' => '\App\FavoriteFlyersClass@removeFlyerFromFavorite']);
Route::post('/addToFavoriteFromAllFlyersPage', [
    'as'   => 'add.to.favorite.all.page',
    'uses' => '\App\FavoriteFlyersClass@addFlyerToFavoriteFromAllFleyersPage']);
Route::delete('/deleteFromFavoriteFromAllFlyersPage', [
    'as'   => 'delete.from.favorite.all.page',
    'uses' => '\App\FavoriteFlyersClass@removeFlyerFromFavoriteFromAllFleyersPage']);


// REVIEWS
Route::post('/setRatingsOnProduct', [
    'as'   => 'add.rating',
    'uses' => '\App\RatingReviewsClass@addRatingToFlyer']);
Route::post('/setRatingsOnReviews', [
    'as'   => 'add.rating.reviews',
    'uses' => '\App\RatingReviewsClass@addRatingToReview']);
Route::get('/addReviewToFlyerModal', [
    'as'   => 'add.review',
    'uses' => '\App\RatingReviewsClass@addReviewToFlyerModal']);
Route::get('/editReviewToFlyerModal', [
    'as'   => 'edit.review',
    'uses' => '\App\RatingReviewsClass@editReviewToFlyerModal']);
Route::get('/replyReviewToFlyerModal', [
    'as'   => 'add.review',
    'uses' => '\App\RatingReviewsClass@replyReview']);
Route::get('/editReplyReviewToFlyerModal', [
    'as'   => 'add.review',
    'uses' => '\App\RatingReviewsClass@editReplyReview']);
Route::post('/addRatingToFlyerModal', [
    'as'   => 'add.get.review.id',
    'uses' => '\App\RatingReviewsClass@addRatingToFlyerModal']);
Route::post('/editRatingToFlyerModal', [
    'as'   => 'add.edit.review.id',
    'uses' => '\App\RatingReviewsClass@editRatingToFlyerModal']);


//DELETE PICTURES FROM FLYERS
Route::delete('photos/{id}', 'PhotosController@destroy');


// SEARCH FLYERS
Route::get('/search', [
    'as'   => 'search.flyers',
    'uses' => '\App\SearchFlyersClass@searchFlyers',
    ]);
Route::get('/search-icon', [
    'as'   => 'search.flyers.icon',
    'uses' => '\App\SearchFlyersClass@searchFlyersIcon',
    ]);

// CHAT
Route::get('/chat', [
    'as'   => 'chat.home',
    'uses' => 'ChatController@home',
    ])->middleware('auth');

Route::get('/messages', [
    'as'   => 'chat.messages',
    'uses' => 'ChatController@returnChatMessages',
    ])->middleware('auth');

Route::post('/messages', [
    'as'   => 'chat.message',
    'uses' => 'ChatController@sendMessage',
    ])->middleware('auth');


// ADMIN PANNEL ROUTE
Route::get('/admin', [
    'middleware' => 'roles',
    'roles'      => ['admin', 'user'],
    'uses'       => 'RolesController@users',
    function () {
        return redirect()->route('/');
    }]);


// AMDIN FLYER ROUTES
Route::get('/admin', [
    'middleware' => 'roles',
    'roles'      => ['admin', 'user'],
    'uses'       => 'AdminPanelController@dashboard',
    function () {
        return redirect()->route('/');
    }]);
Route::get('/admin/flyer/create', 'AdminFlyerController@addNewAdminFlyer');
Route::post('admin/flyer/create/adminPhotos', [
    'as'   => 'admin.flyer.adminPhoto',
    'uses' => 'AdminFlyerController@addAdminFlyerPhoto']);
Route::get('/admin/flyers/show', 'AdminFlyerController@showAdminFlyers');
Route::get('/admin/featured/flyers/show', 'AdminFlyerController@showAdminFeaturedFlyers');
Route::get('/admin/flyer/{flyer}/edit', 'AdminFlyerController@editAdminFlyer');
Route::post('/admin/flyer/update/adminPhoto/{flyer}', [
    'as'   => 'admin.flyer.update.adminPhoto',
    'uses' => 'AdminFlyerController@updateAdminFlyerPhoto']);
Route::put('/admin/flyer/update/{flyer}', [
    'as'   => 'admin.flyer.update',
    'uses' => 'AdminFlyerController@updateAdminFlyer']);
Route::post('/admin/flyers/show', [
    'as'   => 'admin.flyer.store',
    'uses' => 'AdminFlyerController@storeAdminFlyer']);
Route::delete('/admin/flyer/delete/{flyer}', [
    'as'   => 'admin.flyer.destroy',
    'uses' => 'AdminFlyerController@destroyAdminFlyer']);
Route::get('/admin/featured/flyers', 'AdminFlyerController@featuredFlyers');


// ADMIN RATING ROUTES
Route::get('/admin/flyers/rating/show', 'AdminRatingReviewController@showAdminFlyersRating');
Route::get('/admin/flyers/rating', 'AdminRatingReviewController@updateFlyersRating');
Route::delete('/admin/flyer/rating/delete/{rating}', [
    'as'   => 'admin.flyer.rating.destroy',
    'uses' => 'AdminRatingReviewController@destroyAdminFlyerRating']);
// AMDIN REVIEWS ROUTES
Route::get('/admin/flyers/review/show', 'AdminRatingReviewController@showAdminFlyersReview');
Route::get('/admin/flyers/review', 'AdminRatingReviewController@updateFlyersReview');
Route::get('/admin/review/{review}/edit', 'AdminRatingReviewController@editAdminReview');
Route::put('/admin/review/update/{review}', [
    'as'   => 'admin.review.update',
    'uses' => 'AdminRatingReviewController@updateAdminReview']);
Route::delete('/admin/review/delete/{review}', [
    'as'   => 'admin.review.destroy',
    'uses' => 'AdminRatingReviewController@destroyAdminReview']);
// AMDIN REPLY REVIEWS ROUTES
Route::get('/admin/reply/review/edit/{replyReview}', [
    'as'   => 'admin.reply.review.update',
    'uses' => 'AdminRatingReviewController@updateAdminReplyReview']);
Route::delete('/admin/reply/review/delete/{replyReview}', [
    'as'   => 'admin.reply.review.destroy',
    'uses' => 'AdminRatingReviewController@destroyAdminReplyReview']);
// AMDIN RATING REVIEWS ROUTES
Route::put('/admin/rating/review/update/{ratingReview}', [
    'as'   => 'admin.rating.review.update',
    'uses' => 'AdminRatingReviewController@updateAdminRatingReview']);
Route::delete('/admin/rating/review/delete/{ratingReview}', [
    'as'   => 'admin.rating.review.destroy',
    'uses' => 'AdminRatingReviewController@destroyAdminRatingReview']);
//ADMIN MODAL REPLY AND REVIEW
Route::get('/admin/reply/review/add/{review}', [
    'as'   => 'add.admin.review.reply.modal',
    'uses' => 'AdminRatingReviewController@replyReviewAdminModal']);
Route::get('/admin/rate/review/add/{review}', [
    'as'   => 'add.admin.rate.reply.modal',
    'uses' => 'AdminRatingReviewController@rateReviewAdminModal']);


// AMDIN INVOICES ROUTES
Route::get('/admin/invoice/create', [
	'as' => 'admin.create.invoice',
	'uses' => 'AdminInvoiceController@addNewAdminInvoice']);
Route::post('/admin/invoice/store', [
	'as' => 'admin.store.invoice',
	'uses' => 'AdminInvoiceController@storeNewAdminInvoice']);
Route::get('/admin/invoice/show', [
	'as' => 'admin.show.invoice',
	'uses' => 'AdminInvoiceController@manageAdminInvoice']);
Route::post('/admin/invoice/{invoice}/update', [
    'as' => 'admin.update.invoice',
    'uses' => 'AdminInvoiceController@updateAdminInvoice']);
Route::get('/admin/invoice/{invoice}/edit', [
	'as' => 'admin.edit.invoice',
	'uses' => 'AdminInvoiceController@editAdminInvoice']);
Route::delete('/admin/invoice/{invoice}/delete', [
	'as' => 'admin.invoice.destroy',
	'uses' => 'AdminInvoiceController@destroyAdminInvoice']);


// ADMIN USER PROFILE AND PERMISSION ROUTES
Route::get('/admin/user/create', 'UserController@addNewUser');
Route::put('/admin/user/create', [
    'as'   => 'admin.user.create',
    'uses' => 'UserController@storeNewUser']);
Route::get('/admin/users/permission', [
    'as'   => 'admin.user.permision',
    'uses' => 'RolesController@usersPermission']);
Route::get('/admin/users/permission/{user}', [
    'as'   => 'admin.user.permision.change',
    'uses' => 'RolesController@changeUserPermision']);
Route::get('/admin/users/profiles', [
    'as'   => 'admin.user.profiles',
    'uses' => 'UserController@userProfile']);
Route::put('/admin/users/profiles/{user}', [
    'as'   => 'admin.user.profile.update',
    'uses' => 'UserController@updateUserProfiles']);
Route::get('/admin/user/{user}/edit', [
    'as'   => 'admin.user.update',
    'uses' => 'UserController@editAdminUser']);
Route::put('/admin/user/update/{user}', [
    'as'   => 'admin.user.update',
    'uses' => 'UserController@updateAdminUserAcount']);
Route::put('/admin/user/update/delivery-address/{user}', [
    'as'   => 'admin.user.delivery.update',
    'uses' => 'UserController@updateAdminUserDelivery']);
Route::put('/admin/user/update/credit-card/{user}', [
    'as'   => 'admin.user.card.update',
    'uses' => 'UserController@updateAdminUserCreditCard']);
Route::delete('/admin/user/delete/{user}', [
    'as'   => 'admin.user.destroy',
    'uses' => 'UserController@destroyAdminUser']);

// TEST
Route::resource('API', 'RestApiController');
