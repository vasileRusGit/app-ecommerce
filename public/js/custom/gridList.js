$(function () {
    //cookie search page
    var cookieValue = $.cookie('gridList');
    if (_.isUndefined(cookieValue)) {
        $.cookie('gridList', 'list', {expires: date, path: '/'});
    }

    var urlPath = window.location.pathname;
    if (urlPath !== '/') {
        $('.articles').removeClass('col-md-4').addClass('col-md-12');

        // call to set the cookie value if is undefined
        var date = new Date();
        date.setTime(date.getTime() + (6000 * 1000));
        $.cookie('adminToggle', 'off', {expires: date, path: '/'});

        if (cookieValue == 'list') {
            list();
        } else {
            grid();
        }

        function list() {
            $('.grid-button span').removeClass('active');
            $('.list-button span').addClass('active');

            $('.column').removeClass('col-md-12').addClass('row');
            $('.column').removeClass('grid').addClass('list');
            $('.gridListView').removeClass('grid').addClass('list');
            // set cookie
            setCookie('list');
        }

        function grid() {
            $('.list-button span').removeClass('active');
            $('.grid-button span').addClass('active');

            $('.gridListView').removeClass('row').removeClass('list').addClass('grid');
            $('.column').removeClass('row').addClass('col-md-4');
            $('.column').removeClass('list').addClass('grid');
            // set cookie
            setCookie('grid');
        }


        function setCookie(cookieValue) {
            // remove cookie
            $.removeCookie('gridList');
            if (_.isUndefined(cookieValue)) {
                $.cookie('gridList', 'list', {expires: date, path: '/'});
            }
            // set cookie
            $.cookie('gridList', cookieValue, {expires: date, path: '/'});
            sendData(cookieValue);
        }

        function sendData(cookieValue) {
            $.ajax({
                type: 'POST',
                url: '/grid-list',
                data: {cookievalue: cookieValue},
                success: function (raspunsDeLaAjax) {
                }
            });
        }

        // show pencil on hover street
        // $('.titleForPencil').hover(function(){
        $('.titleEdit').addClass('ion-edit');
        // },function(){
        // 	$('.titleEdit').removeClass('ion-edit');
        // });


        // MOVE PRICE TO AND PRICE FROM ONE UPON ANOTHER
        function jqUpdateSize() {
            // Get the dimensions of the viewport
            var width = $(window).width();
            $('#jqWidth').html(width);      // Display the width

            if (width <= 990) {
                $('.searchImputToPrice').insertAfter('.searchImputFromPrice');
                $('.searchImputToPrice').css('margin-top', '20px');

                // hide grid/list buttons
                $('.buttons').hide();

                // code for mobile layout
                $('.post-content').css({
                    "margin-left": "0px",
                    "padding-top": "20px",
                    "max-width": "106%",
                    "padding-bottom": "0px"
                });
                $('.descript').css({
                    "margin-left": "10px",
                    "margin-bottom": "10px"
                });
                $('.title').css('margin-left', '10px');
                $('.sub_title').css('margin-left', '10px');
                $('.thumbnail').css('max-width', '400px');
                $('.post-module').css('max-height', '400px');
                $('.dysplayOnDestopListView').css('margin-right', '7px');
                // $('.allFlyersRow #articles').css({'width': '97%'});
                // $('.gridListView #articles').css({"padding-right": '0'});
                $('.post-meta').hide();
                if (width == 414) {
                    $('.dysplayOnDestopListView').css('margin-right', '0px');
                }
                if ($('.allSearchFlyersRow #articles').hasClass('list')) {
                    $('.gridListView #articles').css('width', '104%');
                }
                if ($('.allSearchFlyersRow #articles').hasClass('grid')) {
                    $('.gridListView #articles').css('width', '95%');
                }
            } else {
                $('.searchImputToPrice').insertAfter('.description');
                $('.searchImputToPrice').css('margin-top', '0px');

                $('.list-button').on('click', function () {
                    // if another driflist cookie is added , delete them
                    if ($.cookie('gridList', {path: '/flyers'})) {
                        // remove cookie
                        $.removeCookie('gridList', {path: '/flyers'});
                    }
                    list();
                });
                $('.grid-button').on('click', function () {
                    // if another driflist cookie is added , delete them
                    if ($.cookie('gridList', {path: '/flyers'})) {
                        // remove cookie
                        $.removeCookie('gridList', {path: '/flyers'});
                    }
                    grid();
                });
            }
        }

        $(document).ready(jqUpdateSize);    // When the page first loads
        $(window).resize(jqUpdateSize);     // When the browser changes size
    }
});