$(function () {
    var checkFlyerPage = $('#writeReviewPen').text();
    if(checkFlyerPage !== undefined){
        var addReviewModal = new RModal(document.getElementById('addReviewModal'), {
            //content: 'Abracadabra'
            beforeOpen: function(next) {
                next();
            }
            , afterOpen: function() {
            }

            , beforeClose: function(next) {
                next();
            }
            , afterClose: function() {
            }
            // , bodyClass: 'modal-open'
            // , dialogClass: 'modal-dialog'
            // , dialogOpenClass: 'animated fadeIn'
            // , dialogCloseClass: 'animated fadeOut'

            , focus: true
            // , focusElements: ['input.form-control', 'textarea', 'button.btn-primary']

            , escapeClose: true
        });

        document.addEventListener('keydown', function(ev) {
            addReviewModal.keydown(ev);
        }, false);

        document.getElementById('writeReviewPen').addEventListener("click", function(ev) {
            ev.preventDefault();
                // on write reivew click
                if ($('#app-navbar-collapse > ul.nav.navbar-nav.navbar-right > li > a').text().includes('LoginRegister')) {
                    swal(
                        'Attention!',
                        'In order to write a review this product you need to be logged in!',
                        'info'
                        )
                }else{
                    addReviewModal.open();
                }
            }, false);

        window.addReviewModal = addReviewModal;
    }

    // close the modal on close and save
    $('.modal-footer').click(function(){
        $('.modal').modal('hide');
    });
});