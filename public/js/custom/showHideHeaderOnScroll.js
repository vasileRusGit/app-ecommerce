$(document).ready(function(){
    var previousScroll = 0;

    $(window).scroll(function(){
        if($('.navbar-toggle').hasClass('collapsed')){

            var currentScroll = $(this).scrollTop();
            if (currentScroll > 0 && currentScroll < $(document).height() - $(window).height()){
                if (currentScroll > previousScroll){
                    window.setTimeout(hideNav, 300);
                } else {
                    window.setTimeout(showNav, 300);
                }
                previousScroll = currentScroll;
            }
        }else{
            $('.user-menu').addClass('open');
            $('.navbar-collapse').addClass('scrollHeight');
            if($('.loginNavbarArea').text().replace(/\s+/g,'') == "LoginRegister"){
                $('.loginNavbarArea').addClass('scrollHeightLogin');
            }else{
                var screenHeight = $(window).height();
                if(screenHeight <= 374){
                    $('.loginNavbarArea').addClass('scrollHeight');
                }else{
                    $('.loginNavbarArea').addClass('scrollHeightUserMenu');
                }
            }
            
        }

    });

    function hideNav() {
        $(".navigation-bar").removeClass("is-visible").addClass("is-hidden");
    }
    function showNav() {
        $(".navigation-bar").removeClass("is-hidden").addClass("is-visible");
    }


    // postioning logo non the middlke of the screen
    var width = $(window).width();
    if(width <= 760){
        var logoWidth = 128;
        var logoMarginLength = (width - logoWidth) / 5;
        console.log(logoMarginLength);
        // var searchMarginLength = width - 30;
        $('.navbar-brand').css('margin-right', logoMarginLength + 'px');
    }
});