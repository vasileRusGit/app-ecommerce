$(document).ready(function () {
    $('#submit-all').click(function () {
        $('#addPhotoForm').submit();
    });


    // FLYER ADMIN PANEL
    // submit the add new flyer form ADMIN PANNEL
    $('#createAdminFlyerSubmitButton').on('click', function () {
        var street = $('#street').val();
        var city = $('#city').val();
        var zip = $('#zip').val();
        var state = $('#state').val();
        var price = $('#price').val();

        if ((street !== '') && (city !== '') && (zip !== '') && (state !== '') && (price !== '') && (isNaN(price) == false)) {
            $('#createFlyerForm').submit();
        } else {
            requiredFlyer();
        }
    });
    // submit the update flyer form ADMIN PANNEL
    $('#updateAdminFlyerSubmitButton').on('click', function () {
        var street = $('#street').val();
        var city = $('#city').val();
        var zip = $('#zip').val();
        var state = $('#state').val();
        var price = $('#price').val();

        if ((street !== '') && (city !== '') && (zip !== '') && (state !== '') && (price !== '') && (isNaN(price) == false)) {
            $('#adminEditFlyerForm').submit();
        } else {
            requiredFlyer();
        }
    });

    function requiredCss(selector, focusSelector) {
        $(selector + ' input').addClass('errorInput');
        $(selector + ' label').addClass('errorLabel');
        if ($(selector + ' #appendText').text() == '') {
            $(selector).append("<strong><p id='appendText' style='color: #dd0000'>This field is reuired!</p></strong>");
        }
        $(focusSelector).focus();
    }

    function removeRequiredCss(selector) {
        $(selector + ' input').removeClass('errorInput');
        $(selector + ' label').removeClass('errorLabel');
        $(selector + ' #appendText').remove();
    }

    function requiredFlyer() {
        var street = $('#street').val();
        var city = $('#city').val();
        var zip = $('#zip').val();
        var state = $('#state').val();
        var price = $('#price').val();

        if (price == '') {
            requiredCss('.priceForm', '#price');
        } else {
            if (isNaN(price)) {
                requiredCss('.priceForm');
                if ($('.priceForm #appendText').text() !== '') {
                    $('.priceForm #appendText').remove();
                    $('.priceForm').append("<strong><p id='appendText' style='color: #dd0000'>This field must be a number!</p></strong>");
                    $('#price').focus();
                }
            } else {
                removeRequiredCss('.priceForm');
            }
        }

        if (state == '') {
            requiredCss('.stateForm', '#state');
        } else {
            removeRequiredCss('.stateForm');
        }

        if (zip == '') {
            requiredCss('.zipForm', '#zip');
        } else {
            removeRequiredCss('.zipForm');
        }

        if (city == '') {
            requiredCss('.cityForm', '#city');
        } else {
            removeRequiredCss('.cityForm');
        }

        if (street == '') {
            requiredCss('.streetForm', '#street');
        } else {
            removeRequiredCss('.streetForm');
        }
    }


    // USER ADMIN PANEL
    // submit the add new user form ADMIN PANNEL
    $('#createAdminUserSubmitButton').on('click', function () {
        var username = $('#name').val();
        var email = $('#email').val();
        var password = $('#password').val();
        var repeatPassword = $('#repeat-password').val();

        if ((username !== '') && (email !== '') && (password !== '') && (repeatPassword !== '') && (isEmail(email) == true)) {
            if(password == repeatPassword) {
                $('#adminCreateUserForm').submit();
            }else{
                requiredCss('.passwordForm', '#password');
                $('.passwordForm #appendText').remove();
                $('.passwordForm').append("<strong><p id='appendText' style='color: #dd0000'>This passwords must match!</p></strong>");
                $('#password').val('');
                $('#repeat-password').val('');
                removeRequiredCss('.repeatPasswordForm');
                $('#password').focus();
            }
        } else {
            requiredUser();
        }
    });

    // submit the update user form ADMIN PANNEL
    $('#updateAdminUserSubmitButton').on('click', function () {
        var username = $('#name').val();
        var email = $('#email').val();
        var password = $('#password').val();
        var repeatPassword = $('#repeat-password').val();

        if ((username !== '') && (email !== '') && (password !== '') && (repeatPassword !== '') && (isEmail(email) == true)) {
            if(password == repeatPassword) {
                $('#adminEditUserForm').submit();
            }else{
                requiredCss('.passwordForm', '#password');
                $('.passwordForm #appendText').remove();
                $('.passwordForm').append("<strong><p id='appendText' style='color: #dd0000'>This passwords must match!</p></strong>");
                $('#password').val('');
                $('#repeat-password').val('');
                removeRequiredCss('.repeatPasswordForm');
                $('#password').focus();
            }
        } else {
            requiredUser();
        }
    });

    function requiredUser() {
        var username = $('#name').val();
        var email = $('#email').val();
        var password = $('#password').val();
        var repeatPassword = $('#repeat-password').val();

        if (repeatPassword == '') {
            requiredCss('.repeatPasswordForm', '#repeat-password');
        } else {
            removeRequiredCss('.repeatPasswordForm');
        }

        if (password == '') {
            requiredCss('.passwordForm', '#password');
        } else {
            removeRequiredCss('.passwordForm');
        }

        if (email == '') {
            requiredCss('.emailForm', '#email');
        } else {
            if (isEmail(email) == false) {
                requiredCss('.emailForm');
                if ($('.emailForm #appendText').text() !== '') {
                    $('.emailForm #appendText').remove();
                    $('.emailForm').append("<strong><p id='appendText' style='color: #dd0000'>This field must be an email!</p></strong>");
                    $('#email').focus();
                }
            }else{
                removeRequiredCss('.emailForm');
            }
        }

        if (username == '') {
            requiredCss('.usernameForm', '#name');
        } else {
            removeRequiredCss('.usernameForm');
        }
    }

    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }
});