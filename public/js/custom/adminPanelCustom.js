$(document).ready(function () {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    // NAVIGATION ACTIVE CLASS
    var url = window.location;

    // Will only work if string in href matches with location
    $('ul.sidebar-menu a[href="' + url + '"]').parent().removeClass('active');

    // Will also work for relative and absolute hrefs
    $('ul.sidebar-menu a').filter(function () {
        return this.href == stripQueryStringAndHashFromPath(url);
    }).parent().addClass('active').parent().parent().addClass('active');

    function stripQueryStringAndHashFromPath(url) {
      return url.toString().split("?")[0].toString().split("#")[0];
  }

  var date = new Date();
  date.setTime(date.getTime() + (6000 * 1000));

    // COOKIE VALUE FOR ADMIN TOOGLE SIDEBAR
    var cookieValueAdminToggle = $.cookie('adminToggle');
    if(_.isUndefined(cookieValueAdminToggle)){
        $.cookie('adminToggle', 'off', { expires: date , path: '/'});
    }

    $('body > div > header > nav > a').on('click', function(){
        if(cookieValueAdminToggle == 'off'){
            toggleOn();
        }else{
            toggleOff();
        }
    });

    if ($('body').hasClass('sidebar-mini')){
        $('#imageHolderUserAccountSidebar').addClass('toggledSidebarImage');
    }else{
        $('#imageHolderUserAccountSidebar').removeClass('toggledSidebarImage');
    }


    function toggleOff(){
        // remove cookie
        $.removeCookie('adminToggle');
        // set cookie
        $.cookie('adminToggle', 'off', { expires: date , path: '/'});
    }
    function toggleOn(){
        // remove cookie
        $.removeCookie('adminToggle');
        // set cookie
        $.cookie('adminToggle', 'on', { expires: date , path: '/'});
    };


    // SIDEBAR TOGGLE
    // get header username adn pannel
    var usernamePanel  = document.getElementById('userName').text;
    var usernamePanelFirstLetter = usernamePanel.charAt(0);
    

    // sidebar-mini sidebar-collapse
    $('body > div > header > nav > a').on('click', function(){
        if ($('body').hasClass('sidebar-mini')){
            $('#imageHolderUserAccountSidebar').removeClass('toggledSidebarImage');

            $('.skin-blue').removeClass('sidebar-mini');

            $(window).on('hashchange', function(e){
                history.replaceState ("", document.title, e.originalEvent.oldURL);
            });
            // change header username to original
            document.getElementById('userName').innerHTML = usernamePanel;

            // remove html icon for toggled sidebar
            if($( window ).width() >= 768){

                $('.dash').removeClass('fa fa-dashboard');
                // $('.newFlyer').removeClass('fa fa-plus');
                // $('.showFlyer').removeClass('fa fa-folder-open');
                // $('.userCreate').removeClass('fa fa-user-plus');
                // $('.userProfiles').removeClass('fa fa-user');
                // $('.userPermisions').removeClass('fa fa-users');
                // $('.featuredFlyer').removeClass('fa fa-eye');
                // $('.ratingFlyers').removeClass('fa fa-star-half-o');
                // $('.rewviewFlyers').removeClass('fa fa-comments-o');
                // $('.invoiceCreate').removeClass('fa fa-plus');
                // $('.invoiceManage').removeClass('fa fa-book');
            }
            

            // when toggle is on, then need to add fa classes on lcick
            if (cookieValueAdminToggle == 'off'){
                if($.cookie('adminToggle', { path: '/' })){
                    // remove cookie
                    $.removeCookie('adminToggle', { path: '/' });
                }
                toggleOff();
            }
        }else{
            $('#imageHolderUserAccountSidebar').addClass('toggledSidebarImage');

            $(window).on('hashchange', function(e){
                history.replaceState ("", document.title, e.originalEvent.oldURL);
            });

            $('.skin-blue').addClass('sidebar-mini');

            // change header username to just to the first letter
            if($(window).width() >= 768){
                document.getElementById('userName').innerHTML = usernamePanelFirstLetter.bold();

                // add html icon for toggled sidebar
                $('.dash').addClass('fa fa-dashboard');
                // $('.newFlyer').addClass('fa fa-plus');
                // $('.showFlyer').addClass('fa fa-folder-open');
                // $('.userCreate').addClass('fa fa-user-plus');
                // $('.userProfiles').addClass('fa fa-user');
                // $('.userPermisions').addClass('fa fa-users');
                // $('.featuredFlyer').addClass('fa fa-eye');
                // $('.ratingFlyers').addClass('fa fa-star-half-o');
                // $('.rewviewFlyers').addClass('fa fa-comments-o');
                // $('.invoiceCreate').addClass('fa fa-plus');
                // $('.invoiceManage').addClass('fa fa-book');
            }

            if (cookieValueAdminToggle == 'on'){
                if($.cookie('adminToggle', { path: '/' })){
                    // remove cookie
                    $.removeCookie('adminToggle', { path: '/' });
                }
                toggleOn();
            }
        }
    });

    if (cookieValueAdminToggle == 'on'){
        if (!$('.dash').hasClass('fa-dashboard')){
            // add html icon for toggled sidebar
            if($(window).width() >= 768){
                $('.dash').addClass('fa fa-dashboard');
                // $('.newFlyer').addClass('fa fa-plus');
                // $('.showFlyer').addClass('fa fa-folder-open');
                // $('.userCreate').addClass('fa fa-user-plus');
                // $('.userProfiles').addClass('fa fa-user');
                // $('.userPermisions').addClass('fa fa-users');
                // $('.ratingFlyers').addClass('fa fa-star-half-o');
                // $('.rewviewFlyers').addClass('fa fa-comments-o');
                // $('.featuredFlyer').addClass('fa fa-eye');
                // $('.invoiceCreate').addClass('fa fa-plus');
                // $('.invoiceManage').addClass('fa fa-book');

                // change header username to just to the first letter
                document.getElementById('userName').innerHTML = usernamePanelFirstLetter.bold();
            }
        }
    }


    // SWEET ALERT ON DELETE ADMIN FLYERS
    $('#adminDeleteFlyer').click(function (e) {
        e.preventDefault();
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this imaginary file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "Cancel",
            closeOnConfirm: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $('#submitDeleteForm').submit();
                swal({
                    title: "Deleted!",
                    text: "Your row has been deleted.",
                    type: "success",
                    showConfirmButton: false,
                    timer: 5000
                });
            }
            else {
                swal("Cancelled", "Your flyer is safe :)", "error");
            }
        });
    });

    // SWEET ALERT ON DELTE ADMIN USERS
    $('#adminDeleteUser').on('click', function (e) {
        e.preventDefault();
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this imaginary file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel plx!",
            closeOnConfirm: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $('#submitDeleteForm').submit();
                swal({
                    title: "Deleted!",
                    text: "Your row has been deleted.",
                    type: "success",
                    showConfirmButton: false,
                    timer: 5000
                });
            }
            else if (isConfirm == 'cancel'){
                swal(
                    'Cancelled',
                    'Your imaginary file is safe :)',
                    'error',
                    '3000'
                    );
            }
        });
    });
});