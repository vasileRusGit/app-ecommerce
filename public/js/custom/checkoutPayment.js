$(document).ready(function() {
    // credit card validation
    $('.input-cart-number').on('keyup change', function() {
        $t = $(this);
        if ($t.val().length > 3) {
            $t.next().focus();
        }
        var card_number = '';
        $('.input-cart-number').each(function() {
            card_number += $(this).val() + '';
            if ($(this).val().length == 4) {
                $(this).next().focus();
            }
        })
        $('.credit-card-box .number').html(card_number);
    });
    $('#card-holder').on('keyup change', function() {
        $t = $(this);
        $('.credit-card-box .card-holder div').html($t.val());
    });
    $('#card-holder').on('keyup change', function() {
        $t = $(this);
        $('.credit-card-box .card-holder div').html($t.val());
    });
    $('#card-expiration-month, #card-expiration-year').change(function() {
        m = $('#card-expiration-month option').index($('#card-expiration-month option:selected'));
        m = (m < 10) ? '0' + m : m;
        y = $('#card-expiration-year').val().substr(2, 2);
        $('.card-expiration-date div').html(m + '/' + y);
    })
    $('#card-ccv').on('focus', function() {
        $('.credit-card-box').addClass('hover');
    }).on('blur', function() {
        $('.credit-card-box').removeClass('hover');
    }).on('keyup change', function() {
        $('.ccv div').html($(this).val());
    });
    // on page load select credit card
    $('#true-option').prop('checked', true);
    $('.ramburs .panel').attr('disabled', true);
    $('.ramburs .panel').addClass('opacityClass03');
    // blur the not active payment metdo
    $('#true-option').click(function() {
        $('.creditCard .form').removeClass('opacityClass03');
        $('.creditCard .form input, .creditCard .form button').attr('disabled', false);
        $('.creditCard .credit-card-box').removeClass('opacityClass03');
        $('.ramburs .panel').attr('disabled', true);
        $('.ramburs .panel').addClass('opacityClass03');
    });
    $('#false-option').click(function() {
        $('.creditCard .form').addClass('opacityClass03');
        $('.creditCard .form input, .creditCard .form button').attr('disabled', true);
        $('.creditCard .credit-card-box').addClass('opacityClass03');
        $('.ramburs .panel').attr('disabled', false);
        $('.ramburs .panel').removeClass('opacityClass03');
    });
});