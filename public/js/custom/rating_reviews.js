$(function () {
    // on rating star click set the rating for the flyer
    $('.rating > input').click(function () {
        if ($('#app-navbar-collapse > ul.nav.navbar-nav.navbar-right > li > a').text().includes('LoginRegister')) {
            swal(
                'Attention!',
                'In order to rate this product you need to be logged in!',
                'info'
                )
        } else {
            // remove selectedRating class from previos selected rating
            for (var i = 1; i <= 5; i++) {
                if ($('#star' + i).hasClass('selectedRating')) {
                    $('#star' + i).removeClass('selectedRating');
                }
            }

            $(this).addClass('selectedRating');
            var ratingNumber = $(this).val();

            var urlRating = window.location.pathname;
            var splitUrl = urlRating.split('/');
            var zip = splitUrl[1];
            var street = splitUrl[2];
            setRatingsOnProduct(ratingNumber, zip, street);
        }
    });

    // on add rating star click set the rating for the review
    $('.ratingReviewModal > input').on('click', function (e) {
        e.preventDefault();
        // remove selectedRating class from previos selected rating
        for (var i = 1; i <= 5; i++) {
            if ($('#ratingReviewModalStar' + i).hasClass('selectedModalRating')) {
                $('#ratingReviewModalStar' + i).removeClass('selectedModalRating');
            }
        }

        $(this).addClass('selectedModalRating');
    });
    // add ratings to review model
    $('#addReviewModalButton').click(function () {
        var reviewRatingNumber = $('.selectedModalRating').val();
        var urlRating = window.location.pathname;
        var splitUrl = urlRating.split('/');
        var zip = splitUrl[1];
        var street = splitUrl[2];
        setRatingsOnReviews(zip, street, reviewRatingNumber);
    });


    // edit rating on review modal get the review id on edit when the edit pen is clicked
    $('.fa-pencil-square-o#editReviewPen').click(function () {
        var reviewClass = $(this).attr('class');
        var reviewId = reviewClass.match(/\d+/)[0];

        $('#review-' + reviewId + ' > label').on('click', function (e) {
            e.preventDefault();
            for (var i = 1; i <= 5; i++) {
                if ($('#review-' + reviewId + ' #edit_review_star' + i).hasClass('selectedRatingReview')) {
                    $('#review-' + reviewId + ' #edit_review_star' + i).removeClass('selectedRatingReview');
                }
            }
            $(this).prev().addClass('selectedRatingReview');
        });

        // edit ratings to review model
        $('#editReviewModal-'+reviewId+' #editReviewModalButton').click(function () {
            var reviewRatingNumber = $('#editReviewModal-'+reviewId+' .selectedRatingReview').val();
            var editModalFlyerId = $('#flyerIdShowPageForEditReview').val();

            var reviewClassEditModal = $(this).attr('class');
            var reviewIdEditModal = reviewClassEditModal.match(/\d+/)[0];

            var urlRating = window.location.pathname;
            var splitUrl = urlRating.split('/');
            var zip = splitUrl[1];
            var street = splitUrl[2];
            editRatingToFlyerModal(zip, street, reviewRatingNumber, editModalFlyerId, reviewIdEditModal);
        });
    });
    

    function setRatingsOnProduct(ratingNumber, zip, street) {
        $.ajax({
            type: 'POST',
            url: '/setRatingsOnProduct',
            dataType: 'html',
            data: {
                ratingNumber: ratingNumber,
                zip: zip,
                street: street,
            },
            success: function (data) {

            }
        });
    }

    function setRatingsOnReviews(zip, street, reviewRatingNumber) {
        $.ajax({
            type: 'POST',
            url: '/addRatingToFlyerModal',
            dataType: 'html',
            data: {
                reviewRatingNumber: reviewRatingNumber,
                zip: zip,
                street: street,
            },
            success: function (data) {

            }
        });
    }

    function editRatingToFlyerModal(zip, street, reviewRatingNumber, editModalFlyerId, reviewIdEditModal) {
        $.ajax({
            type: 'POST',
            url: '/editRatingToFlyerModal',
            dataType: 'html',
            data: {
                zip: zip,
                street: street,
                reviewRatingNumber: reviewRatingNumber,
                editModalFlyerId: editModalFlyerId,
                reviewIdEditModal: reviewIdEditModal,
            },
            success: function (data) {

            }
        });
    }
});