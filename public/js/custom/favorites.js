$(function () {
    var ajaxStatus = '';
    var url = window.location.pathname;
    var date = new Date();
    date.setTime(date.getTime() + (6000 * 1000));

    // set the rating from the database
    var ratingFromDatabase = $.trim($('#ratingNumberFromDatabase').text());
    $('#star' + ratingFromDatabase).addClass('selectedRating');

    //cookie favorite, set if is not set
    var favoriteCookieValue = $.cookie('favorite');
    if (_.isUndefined(favoriteCookieValue)) {
        $.cookie("favorite", JSON.stringify([]), {expires: date, path: '/'});
    }

    // on ajax request save selected value
    $('.js-example-basic-single').on("select2:select", function () {
        addToFavoriteGridListLayout();
    });

    // check to see if you are on grid list layout or on single flyer layout
    if ((url == '/flyers') || (url == '/favorite-flyers') || (url.includes('search'))){
        addToFavoriteGridListLayout();
    } else {
        addToFavoriteSingleFlyerLayout();
    }

    function addToFavoriteGridListLayout(){
        var cookieFavoriteObj = JSON.parse($.cookie("favorite"));
        var cookieFavorite = $.map(cookieFavoriteObj, function (value) {
            return [value];
        });

        // all flyers id that are favorite
        cookieFavorite.forEach(function (entry) {
            if ($("i[data-flyer=" + entry + "]").hasClass('isFavorite')) {
                    // on star click remove the favorite class
                    $("i[data-flyer=" + entry + "]").on('click', function () {
                        removeFromFavoriteAllFlyersPage(entry);
                    }
                    );
                }

            }
            );

        // get all ids from the page that are not favorite
        $('[data-flyer]').each(function () {
            var notFavoriteFlyerId = $(this).data('flyer');

            if ($("i[data-flyer=" + notFavoriteFlyerId + "]").hasClass('isFavorite')) {
                $("i[data-flyer=" + notFavoriteFlyerId + "]").removeClass('ion-android-star-outline');
                $("i[data-flyer=" + notFavoriteFlyerId + "]").addClass('ion-android-star');
            } else {
                $("i[data-flyer=" + notFavoriteFlyerId + "]").addClass('ion-android-star-outline');
                $("i[data-flyer=" + notFavoriteFlyerId + "]").removeClass('ion-android-star');
            }

            //flyers that not have isFavoriteClass
            if (!$("i[data-flyer=" + notFavoriteFlyerId + "]").hasClass('isFavorite')) {
                // on star click remove the favorite class
                $(this).on('click', function () {
                    addToFavoriteFromAllFlyersPage(notFavoriteFlyerId);
                });
            }
        });


        var favoriteObj = JSON.parse($.cookie("favorite"));
        var favorite = $.map(favoriteObj, function (value) {
            return [value];
        });

        $('div[class=favorite] > i').on('click', function () {
            favorite.forEach(function (entry) {
                if ($("i[data-flyer=" + entry + "]").hasClass('isFavorite')) {
                    $("i[data-flyer=" + entry + "]").removeClass('ion-android-star-outline');
                    $("i[data-flyer=" + entry + "]").addClass('ion-android-star');
                } else {
                    $("i[data-flyer=" + entry + "]").add('ion-android-star-outline');
                    $("i[data-flyer=" + entry + "]").removeClass('ion-android-star');
                }
            });

            var dataFlyerId = $(this).data('flyer');
            if (!isInArray(dataFlyerId, favorite)) {
                favorite.push(dataFlyerId);
                eraseCookie('favorite');
                $.cookie("favorite", JSON.stringify(favorite), {expires: date, path: '/'});
            }
        });

        function isInArray(value, array) {
            return array.indexOf(value) > -1;
        }

        function eraseCookie(name) {
            document.cookie = name + '=; Max-Age=0'
        }
    }

    function addToFavoriteSingleFlyerLayout(){
        // add class isFavorite for individual flyers
        if ($('.favorite-icon').hasClass('isFavorite')) {
            $('.favorite-icon').removeClass('ion-android-star-outline');
            $('.favorite-icon').addClass('ion-android-star');
        } else {
            $('.favorite-icon').removeClass('ion-android-star');
            $('.favorite-icon').addClass('ion-android-star-outline');
        }

        // on star click change the class for individual flyers
        $('.favorite-icon').on('click', function (e) {
            e.preventDefault();
            var flyerId = $(this).data('flyer');

            if ($('.favorite-icon').hasClass('ion-android-star-outline')) {
                addTofavorite(flyerId);
            } else {
                removeTofavorite(flyerId);
            }
        });
    }

    function addTofavorite(flyerId) {
        $.ajax({
            type: 'POST',
            url: '/addToFavorite',
            data: {
                flyerId: flyerId
            },
            success: function (raspunsDeLaAjax) {
                $('.favorite-icon').removeClass('ion-android-star-outline');
                $('.favorite-icon').addClass('ion-android-star');
            }
        });
    }

    function removeTofavorite(flyerId) {
        $.ajax({
            type: 'DELETE',
            url: '/deleteFromFavorite',
            data: {
                flyerId: flyerId
            },
            success: function (raspunsDeLaAjax) {
                $('.favorite-icon').removeClass('ion-android-star');
                $('.favorite-icon').addClass('ion-android-star-outline');
            }
        });
    }

    function addToFavoriteFromAllFlyersPage(flyerId) {
        $.ajax({
            type: 'POST',
            url: '/addToFavoriteFromAllFlyersPage',
            data: {
                flyerId: flyerId
            },
            success: function (raspunsDeLaAjax) {
                if (!$("i[data-flyer=" + flyerId + "]").hasClass('isFavorite')) {
                    $("i[data-flyer=" + flyerId + "]").removeClass('ion-android-star-outline');
                    $("i[data-flyer=" + flyerId + "]").addClass('isFavorite');
                    $("i[data-flyer=" + flyerId + "]").addClass('ion-android-star');
                }
                addToFavoriteGridListLayout();
            }
        });
    }

    function removeFromFavoriteAllFlyersPage(flyerId) {
        $.ajax({
            type: 'DELETE',
            url: '/deleteFromFavoriteFromAllFlyersPage',
            data: {
                flyerId: flyerId
            },
            success: function (raspunsDeLaAjax) {
                if ($("i[data-flyer=" + flyerId + "]").hasClass('isFavorite')) {
                    $("i[data-flyer=" + flyerId + "]").removeClass('ion-android-star');
                    $("i[data-flyer=" + flyerId + "]").removeClass('isFavorite');
                    $("i[data-flyer=" + flyerId + "]").addClass('ion-android-star-outline');
                }
                addToFavoriteGridListLayout();

                // check to see if you are on favorite page, if yes reload the page after remove flyer from favorites
                var urlfavorites = window.location.pathname;
                if(urlfavorites.includes('favorite')){
                    location.reload();
                }
            }
        });
    }

});