$(document).ready(function () {

	// restrict for edit flyerProductId
	var urlPath = window.location.pathname;
	if(!urlPath.includes('edit')){
		var productId = $('#productIdFromDatabase').text();

		if(productId !== null){
			var flyerProductId = parseInt(productId) + 1;
			var nextProductId = 'ID' + flyerProductId;
			$('#productID').val(nextProductId);
		}else{
			if($('#productIdFromDatabase').text() === ''){
				$('#productID').val('ID1');
			}
		}
	}
	
});