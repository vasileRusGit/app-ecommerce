$(document).ready(function () {
    var url = window.location;
    // Will only work if string in href matches with location
    $('#app-navbar-collapse ul li a[href="' + url + '"]').parent().addClass('active');

    // Will also work for relative and absolute hrefs
    $('#app-navbar-collapse ul li a').filter(function () {
        // console.log(this.href);
        return this.href == stripQueryStringAndHashFromPath(url);
    }).parent().addClass('active');

    function stripQueryStringAndHashFromPath(url) {
      return url.toString().split("?")[0].toString().split("#")[0];
  }


    // DROPZONE
    // submit the add flyer form FRONTEND
    $('#createFlyerSubmitButton').on('click', function () {
        $('#addNewFlyerForm').submit();
    });
    // submit the update flyer form FRONTEND
    $('#updateFlyerSubmitButton').on('click', function () {
        $('#updateDeleteForm').submit();
    });
    // submit the delete flyer form FRONTEND
    $('#updateFlyerSubmitButton').on('click', function () {
        $('#updateDeleteForm').submit();
    });

    // preload for navigation cart
    $(function() {
        $(".preloadNavigationCart").fadeOut(20, function() {
            $(".navigationShopCart").fadeIn(10);       
            // $(".nb-footer").fadeIn(500);   
        });
    });
});


