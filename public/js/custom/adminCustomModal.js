$(function () {

    // open the add new review reply modal
    $('#addReplyReviewAdminModal').click(function(){
        $('#replyReviewAdminModal').modal('show');
    });
    // open the edit review reply modal
    $('[id^=editReviewRepliesButton]').click(function(){
        let modalId = $(this).attr('id').replace(/[^0-9]/g, '');
        $('#editReplyReviewAdminModal-'+ modalId ).modal('show');
    });


    // open the add new rating reply modal
    $('#addNewReviewRatingToModal').click(function(){
        $('#ratingReviewAdminModal').modal('show');
    });


    // close the add reply review modal
    $('#replyReviewModalCancelButton').click(function(){
        $('.modal').modal('hide');
    });
    // close the add rate review modal
    $('#rateReviewModalCancelButton').click(function(){
        $('.modal').modal('hide');
    });
    // close the edit reply review modal
    $('#editReplyReviewModalCancelButton').click(function(){
        $('.modal').modal('hide');
    });


    // header modals
    // messages
    $('.dropdown.messages-menu').click(function(){
        if($('.dropdown-menu.messages').hasClass('in')){
            $('.dropdown-menu.messages').modal('hide');
        }else{
            $('.dropdown-menu.messages').modal('show');
        }
    });
    // notifications
    $('.dropdown.notifications-menu').click(function(){
        if($('.dropdown-menu.notifications').hasClass('in')){
            $('.dropdown-menu.notifications').modal('hide');
        }else{
            $('.dropdown-menu.notifications').modal('show');
        }
    });
    // tasks
    $('.dropdown.tasks-menu').click(function(){
        if($('.dropdown-menu.tasks').hasClass('in')){
            $('.dropdown-menu.tasks').modal('hide');
        }else{
            $('.dropdown-menu.tasks').modal('show');
        }
    });
    // user menu
    $('.dropdown.user-menu').click(function(){
        if($('.dropdown-menu.usermenu').hasClass('in')){
            $('.dropdown-menu.usermenu').modal('hide');
        }else{
            $('.dropdown-menu.usermenu').modal('show');
        }
    });

    // when click on wrapper if some dropdown is opened, closed
    // $('.wrapper').click(function(){
        
    //     if($('.dropdown-menu').hasClass('in')){
    //         $('.dropdown-menu').modal('hide');
    //     }
    // });
});