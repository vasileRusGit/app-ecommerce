$(document).ready(function() {
    var date = new Date();
    var curentUrl = document.URL;
    var width = $(window).width();
    if (width <= 1023) {
        grid();
    }
    // condition to run the search page filter or the show all flyers filter
    var urlPath = window.location.pathname;
    if (urlPath.includes('search')) {
        var urlTermination = getSecondPartSearch(curentUrl);
        if (urlTermination !== undefined) {
            $('.js-example-basic-single').val(urlTermination).change();
        } else {
            $('.js-example-basic-single').val('cele-mai-recente').change();
        }
        // htmlFleyrsCode = $('.allFlyersRow').html();
        $('.js-example-basic-single').on("select2:select", function() {
            // get the selected value
            var selectedvalue = $.trim($('#filterDropdown > span').text());
            var priceFrom = $('#searchFormFrom').val();
            var priceTo = $('#searchFormTo').val();
            var url = $(this).val();
            //redirect to url witout refresh
            window.history.pushState('page2', 'Title', '/search/' + url);
            $.ajax({
                url: '/search/' + url,
                type: 'GET',
                dataType: 'html',
                data: {
                    searchFilterCurrentValue: selectedvalue,
                    searchFilterPriceFrom: priceFrom,
                    searchFilterPriceTo: priceTo,
                },
                success: function(data) {
                    // reload a js file
                    $.getScript("/js/custom/custom.js");
                    
                    $(function() {
                        $(".preload").fadeOut(1000, function() {
                            $(".content").fadeIn(500);
                            $(".nb-footer").fadeIn(500);
                        });
                    });
                    // if filter is selected hide the pagination from showAllFlyers
                    curentUrl = document.URL;
                    urlTermination = getSecondPart(curentUrl);
                    $('.showAllFlyersPagination').hide();
                    // refresh html data for DESKTOP
                    $('.allSearchFlyersRow').empty().html(data);

                    // add class to title for the filtered title
                    $('.title').addClass('filtered');

                    // refresh html data for MOBILE
                    $(function() {
                        $('.articles').removeClass('col-md-4').addClass('col-md-12');

                        // call to set the cookie value if is undefined
                        date.setTime(date.getTime() + (6000 * 1000));
                        $.cookie('adminToggle', 'off', {
                            expires: date,
                            path: '/'
                        });
                        //cookie search page
                        var cookieValue = $.cookie('gridList');
                        if (_.isUndefined(cookieValue)) {
                            $.cookie('gridList', 'list', {
                                expires: date,
                                path: '/'
                            });
                        }
                        if (cookieValue == 'list') {
                            list();
                        } else {
                            grid();
                        }
                        $('.list-button').on('click', function() {
                            // if another driflist cookie is added , delete them
                            if ($.cookie('gridList', {
                                path: '/flyers'
                            })) {
                                // remove cookie
                                $.removeCookie('gridList', {
                                    path: '/flyers'
                                });
                            }
                            list();
                        });
                        $('.grid-button').on('click', function() {
                            // if another driflist cookie is added , delete them
                            if ($.cookie('gridList', {
                                path: '/flyers'
                            })) {
                                // remove cookie
                                $.removeCookie('gridList', {
                                    path: '/flyers'
                                });
                            }
                            grid();
                        });
                    });
                },
                error: function() {
                    alert('error');
                }
            });
        });
    } else {
        var urlTermination = getSecondPart(curentUrl);
        if (urlTermination !== undefined) {
            $('.js-example-basic-single').val(urlTermination).change();
        } else {
            $('.js-example-basic-single').val('cele-mai-recente').change();
        }
        // htmlFleyrsCode = $('.allFlyersRow').html();
        $('.js-example-basic-single').on("select2:select", function() {
            // get the selected value
            var selectedvalue = $('#filterDropdown > span').text();
            var url = $(this).val();
            //redirect to url witout refresh
            window.history.pushState('page2', 'Title', '/filter/' + url);
            $.ajax({
                url: '/filter/' + url,
                type: 'GET',
                dataType: 'html',
                data: {
                    filterCurrentValue: selectedvalue
                },
                success: function(data) {
                    // reload a js file
                    $.getScript("/js/custom/custom.js");


                    // if filter is selected hide the pagination from showAllFlyers
                    curentUrl = document.URL;
                    urlTermination = getSecondPart(curentUrl);
                    $('.showAllFlyersPagination').hide();
                    
                    // refresh html data
                    $('.allFlyersRow').empty().html(data);
                    $('.showAllFlyers').empty().html(data);

                    // add class to title for the filtered title
                    $('.title').addClass('filtered');

                    $(function() {
                        $('.articles').removeClass('col-md-4').addClass('col-md-12');

                        // call to set the cookie value if is undefined
                        date.setTime(date.getTime() + (6000 * 1000));
                        $.cookie('adminToggle', 'off', {
                            expires: date,
                            path: '/'
                        });
                        //cookie search page
                        var cookieValue = $.cookie('gridList');
                        if (_.isUndefined(cookieValue)) {
                            $.cookie('gridList', 'list', {
                                expires: date,
                                path: '/'
                            });
                        }
                        if (cookieValue == 'list') {
                            list();
                        } else {
                            grid();
                        }
                        $('.list-button').on('click', function() {
                            // if another driflist cookie is added , delete them
                            if ($.cookie('gridList', {
                                path: '/flyers'
                            })) {
                                // remove cookie
                                $.removeCookie('gridList', {
                                    path: '/flyers'
                                });
                            }
                            list();
                        });
                        $('.grid-button').on('click', function() {
                            // if another driflist cookie is added , delete them
                            if ($.cookie('gridList', {
                                path: '/flyers'
                            })) {
                                // remove cookie
                                $.removeCookie('gridList', {
                                    path: '/flyers'
                                });
                            }
                            grid();
                        });
                    });
                },
                error: function() {
                    alert('error');
                }
            });
        });
    }

    function loading() {
        $(".preload").fadeOut(1000, function() {
            $(".content").fadeIn(500);
            $(".nb-footer").fadeIn(500);
        });
    }

    function list() {
        $('.grid-button span').removeClass('active');
        $('.list-button span').addClass('active');
        $('.column').removeClass('col-md-12').addClass('row');
        $('.column').removeClass('grid').addClass('list');
        $('.gridListView').removeClass('grid').addClass('list');
        // set cookie
        setCookie('list');
    }

    function grid() {
        $('.list-button span').removeClass('active');
        $('.grid-button span').addClass('active');
        $('.gridListView').removeClass('row').removeClass('list').addClass('grid');
        $('.column').removeClass('row').addClass('col-md-4');
        $('.column').removeClass('list').addClass('grid');
        // set cookie
        setCookie('grid');
    }

    function setCookie(cookieValue) {
        // remove cookie
        $.removeCookie('gridList');
        if (_.isUndefined(cookieValue)) {
            $.cookie('gridList', 'list', {
                expires: date,
                path: '/'
            });
        }
        // set cookie
        $.cookie('gridList', cookieValue, {
            expires: date,
            path: '/'
        });
        sendData(cookieValue);
    }

    function sendData(cookieValue) {
        $.ajax({
            type: 'POST',
            url: '/grid-list',
            data: {
                cookievalue: cookieValue
            },
            success: function(raspunsDeLaAjax) {}
        });
    }

    function getSecondPart(str) {
        if (str.includes('page')) {
            if (str.includes('flyers')) {
                str = 'cele-mai-recente';
                return str;
            } else {
                str = str.split('filter/')[1];
                return str.substring(0, str.indexOf('?'));
            }
        }
        return str.split('filter/')[1];
    }

    function getSecondPartSearch(str) {
        if (str.includes('page')) {
            if (str.split('search/')[1] == undefined) {
                return 'cele-mai-recente';
            } else {
                str = str.split('search/')[1];
                return str.substring(0, str.indexOf('?'));
            }
        }
        return str.split('search/')[1];
    }
});