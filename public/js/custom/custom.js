$(document).ready(function () {
    var url = window.location;
    // Will only work if string in href matches with location
    $('#app-navbar-collapse ul a[href="' + url + '"]').parent().addClass('active');

    // Will also work for relative and absolute hrefs
    $('#app-navbar-collapse ul a').filter(function () {
        return this.href == url;
    }).parent().addClass('active');

    
    // get the name of uploaded file
    $('input[type="file"]').change(function(){
        var value = $("input[type='file']").val();
        $('.js-value').text(value);
    });


    // close the modal for quickview
    $('.modal-footer').click(function(){
        $('.modal').modal('hide');
    });

    // close the quickview modal
    $('#closeQuickviewModalIcon > i').click(function(){
        $('.modal').modal('hide');
    });

    // when the width of window less than 1666
    // var width = $(window).width();
    // if((width <= 1365) && (width > 999)){
    //     $('.gridListViewFeaturedFlyers > div').css('width', '50%');
    //     $('.gridListView.grid #articles').css('width', '50%');
    // }

    // mose over and leave article
    $('.showAllFlyers .post-module, .allSearchFlyersRow .post-module, .allFilerFlyersPage .post-module').mouseenter(function(){
        var flyerId = $(this).attr('class').match(/\d+/)[0];
        $('.'+flyerId+' .positioningIcons').removeClass('hideQuickviewAndFavoritePanel');
        $('.'+flyerId+' .positioningIcons').addClass('showQuickviewAndFavoritePanel');
    });
    $('.showAllFlyers .post-module, .allSearchFlyersRow .post-module, .allFilerFlyersPage .post-module').mouseleave(function(){
        var flyerId = $(this).attr('class').match(/\d+/)[0];
        $('.'+flyerId+' .positioningIcons').removeClass('showQuickviewAndFavoritePanel');
        $('.'+flyerId+' .positioningIcons').addClass('hideQuickviewAndFavoritePanel');
    });


    // send flyer id when open a  modal form all products pageXOffset
    $('.qiuickView').click(function(){
        var flyerId = $(this).attr('class').match(/\d+/)[0];

        $.ajax({
            type: 'POST',
            url: '/sendFlyerIdFromTheModal',
            dataType: 'json',
            data: {
                flyerId: flyerId,
            },
            success: function (data) {
                $('.modal-body').empty().html(data);
            }
        });
    });

    // preload for show all flyers page
    $(function() {
        $(".preload").fadeOut(1000, function() {
            $(".content").fadeIn(500);       
            $(".nb-footer").fadeIn(500);   
        });
    });

    // preload for checkout page cart
    $(function() {
        $(".preloadCart").fadeOut(1000, function() {
            $(".checkoutpage").fadeIn(500);       
            // $(".nb-footer").fadeIn(500);   
        });
    });


    // edit user account
    $('.delivery a').click(function(){

        if($('.delivery a.show').text() == '- show less'){
            $('.delivery a.more').addClass('show');
            $('.delivery a.more').removeClass('hide');
            $('.delivery a.less').removeClass('show');
            $('.delivery a.less').addClass('hide');
            
            $('.delivery .form-group').removeClass('show');
            $('.delivery .form-group').addClass('hide');
        }else if($('.delivery a.show').text() == '+ show more'){
            $('.delivery a.less').addClass('show');
            $('.delivery a.less').removeClass('hide');
            $('.delivery a.more').removeClass('show');
            $('.delivery a.more').addClass('hide');

            $('.delivery .form-group').removeClass('hide');
            $('.delivery .form-group').addClass('show');
        }
    });
});