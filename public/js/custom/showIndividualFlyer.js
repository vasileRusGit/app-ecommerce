$(document).ready(function () {
    var width = $(window).width();
    if(width <= 760){
     $('#productImage').removeClass('productImageCss');
     $('#productName').css('margin-left', '-15px');
     $('.container.content .wrapper').css('margin-bottom', '-20px');
     $('.description-area').css('margin-left', '0px');
     $('.similar-flyers').css('margin-left', '0px');
     $('.left.carousel-control').css({
        "left": "-12px",
        "height": "40px",
        "width": "20px",
        "background": "none repeat scroll 0 0 #cc0000",
        "border": "2px solid #FFFFFF",
        "border-radius": "23px 23px 23px 23px",
        "margin-top": "90px"
    });
     $('.right.carousel-control').css({
        "right": "0px",
        "height": "40px",
        "width": "20px",
        "background": "none repeat scroll 0 0 #cc0000",
        "border": "2px solid #FFFFFF",
        "border-radius": "23px 23px 23px 23px",
        "margin-top": "90px"
    });
     $('.carousel-inner').css({
        "width": "113%",
        "margin-left": "-20px"
    });
     $('.carousel-indicators').css('bottom', '40px');

     if(width == 320){
        $('.post-module').css({
            "min-width": "210px"
        });
    }
    if(width == 414){
        $('.post-module').css({
            "min-width": "305px"
        });
    }

    // positioning the flyer on the middle of the screen 
    if((width >= 480) && (width <= 767)){
        var midleScreen = width / 2;
        var flyerWidth = $('.post-module').width();
        var padingCoruouse = (midleScreen - flyerWidth) + 80;
        console.log(padingCoruouse + "px");
        $('.carousel-inner').css({
            "padding-left": padingCoruouse + "px"
        });
    }

    // hide second and theard similar flyer
    $('#similarFlyers:nth-child(2)').hide();
    $('#similarFlyers:nth-child(3)').hide();
    $('#similarFlyers:nth-child(4)').hide();
}

// IPAD
    if((width >= 768) && (width <= 1023)){
        $('#productImage').css('margin-right', '70px');
    }
});