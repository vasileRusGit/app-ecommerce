$(document).ready(function () {

    var width = $(window).width();
    $('#jqWidth').html(width);      // Display the width

    if (width <= 990) {
        $('html').css('overflow-x', 'hidden');
        // $('.login-page').css({"width": "105%"});
        $('.gridListViewFeaturedFlyers #articlesFeaturedFlyers').css({
            "width": "100%",
            "margin-bottom": "10px"
        });
        $('#featuresFlyersHeading').css('font-size', '30px');
        $('.nb-footer').css('margin-top', '0px');


        // CREATE FLYER PAGE
         var url = window.location.pathname;
        if (url.includes('create')) {
            $('#createFlyerSubmitButton').css('margin-top', '70px');
            $('.nb-footer').css('margin-top', '120px');
            $('#create-flyer').css('font-size', '30px');
        }

        if (url.includes('edit')) {
            // EDIT FLYER PAGE
            $('#updateFlyerSubmitButton').css('margin-top', '70px');
            $('#deleteFlyerButton').css({
                "margin-top": "-62px",
                "margin-left": "100px"
            });
        }

        var numberOfImages = $('.flyerPhotos .row .gallery_images').length;
        if (numberOfImages == 0) {
            $('.flyerPhotos').hide();
        }
        $('.flyerPhotos').css('border', '2px solid red');
    }
});
