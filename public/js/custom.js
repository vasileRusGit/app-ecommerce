$(document).ready(function () {
    // on page load scroll down to see the last messages
    $('div').animate({ scrollTop: 999999 }, 'slow');


    // image preview on desktop and on mobiel
    var width = $(window).width();
    if (width >= 1025) {
        jQuery('a#zoom1').swinxyzoom({mode:'dock', controls: false, size: '100%', dock: { position: 'right' } }); // dock window slippy lens
    }else{
    	jQuery('a#zoom1').swinxyzoom({mode:'dock', controls: false, size: '100%', dock: { position: 'down' } }); // dock window slippy lens
    }
});