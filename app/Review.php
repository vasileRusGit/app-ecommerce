<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    public function review()
    {
        return $this->belongsTo('App\Flyer');
    }

    public function rating_reviews()
    {
        return $this->hasMany('\App\ReviewRating', 'review_id');
    }

    public function reply_review()
    {
        return $this->hasMany('\App\ReplyReview', 'review_id');
    }
}
