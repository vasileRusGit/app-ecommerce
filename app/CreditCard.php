<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CreditCard extends Model
{
    public function credit_card()
    {
        return $this->belongsTo('App\User');
    }
}
