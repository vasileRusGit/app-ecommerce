<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceProducts extends Model
{
    public function invoice_products(){
    	return $this->belongsTo('\App\Invoices');
    }
}
