<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

/**
 * @property mixed id
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'image_name', 'image_path',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *d
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function owns($relation)
    {
        return $relation->user_id == $this->id;
    }

    public function ownsReview($relation)
    {
        return $relation->user_id == $this->id;
    }

    public function flyers()
    {
        return $this->hasMany(Flyer::class);
    }

    public function messages(){
        return $this->hasMany(Message::class);
    }

    public function favorites()
    {
        return $this->hasMany('\App\Favorites', 'user_id');
    }

    public function rating()
    {
        return $this->hasMany('\App\Rating', 'user_id');
    }

    // public function credit_card()
    // {
    //     return $this->has('\App\CreditCard', 'user_id');
    // }

    public function review()
    {
        return $this->hasMany('\App\Review', 'user_id');
    }

    public function rating_reviews()
    {
        return $this->hasMany('\App\ReviewRating', 'user_id');
    }

    public function cart()
    {
        return $this->hasMany('\App\ShoppingCart', 'user_id');
    }

    public function invoice()
    {
        return $this->hasMany('\App\Invoices', 'user_id');
    }

    public function publish(Flyer $flyer)
    {
        return $this->flyers()->save($flyer);
    }

    public function scopeIsSignedIn()
    {
        return \Auth::check();
    }

    public function scopeIsGuest()
    {
        return \Auth::guest();
    }

    public function scopeIsAdmin()
    {
        return \Auth::admin();
    }

    public function scopeUser()
    {
        return \Auth::user();
    }

    // USER ROLE ATTRIBUITES
    public function roles()
    {
        return $this->belongsToMany('App\Role', 'user_role', 'user_id', 'role_id');
    }

    public function hasAnyRole($roles)
    {
        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->hasRole($role)) {
                    return true;
                }
            }
        } else {
            if ($this->hasRole($roles)) {
                return true;
            }
        }
        return false;
    }

    public function hasRole($role)
    {
        if ($this->roles()->where('name', $role)->first()) {
            return true;
        }
        return false;
    }
}

