<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShoppingCart extends Model
{
	public function cart()
    {
        return $this->belongsTo('App\Flyer');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
