<?php 

namespace App;

use App\Http\Requests\FlyerRequest;
use Illuminate\Http\Request;
use App\Flyer;
use App\Photo;
use App\User;
use App\Favorite;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\FlyersController;

class SearchFlyersClass extends FlyersController{
	
	/**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
	public function searchFlyers(Request $request)
	{
		$user = User::user();

		$productIdField = $request->get('productID');
		$searchStreetField = $request->get('street');
		$searchCityField = $request->get('city');
		$searchDescriptionField = $request->get('description');
		$searchPriceFromField = $request->get('price-from');
		$searchPriceToField = $request->get('price-to');

        // conditions for price from
		if ($searchPriceFromField == '> 20000') {
			$searchPriceFromField = 999999999999999999999;
		}
		if (($searchPriceFromField == '< 2000') || ($searchPriceFromField == null)) {
			$searchPriceFromField = 0;
		}

        // conditions for price to
		if (($searchPriceToField == null) || ($searchPriceToField == '> 20000')) {
			$searchPriceToField = 999999999999999999999;
		}

        // condition to change positions if price-from is higher than price to
		if ($searchPriceFromField > $searchPriceToField) {
			$test = $searchPriceFromField;
			$searchPriceFromField = $searchPriceToField;
			$searchPriceToField = $test;

			return redirect()->route('search.flyers', ['price-from' => $searchPriceFromField, 'price-to' => $searchPriceToField]);
		}

		$searchFlyersResult = Flyer::where(array(
			['productID', 'LIKE', "%$productIdField%"],
			['street', 'LIKE', "%$searchStreetField%"],
			['city', 'LIKE', "%$searchCityField%"],
			['description', 'LIKE', "%$searchDescriptionField%"],
			['price', '>', "$searchPriceFromField"],
			['price', '<', "$searchPriceToField"]
			))->orderBy('created_at', 'desc')->paginate(12);


		if (count($searchFlyersResult) < 10) {
			$pagination = Flyer::paginate(0);
		} else {
			$pagination = Flyer::paginate(12);
		}

		$cookievalue = $_COOKIE['gridList'];

        // check if curent loged user has the flyer as favorite
		if (!isset($_COOKIE['favorite'])) {
			setcookie('favorite', json_encode([]), time() + 9999999, '/');
		}
		if (\Auth::check()) {
			$favoriteFlyers = Favorite::where('user_id', '=', $user->id)->pluck('flyer_id');
		} else {
			$favoriteFlyers = json_decode($_COOKIE['favorite'], true);
		}

		return view('pages.search', compact(['searchFlyersResult', 'pagination', 'cookievalue', 'somevlaue', 'favoriteFlyers']));
	}
	

	/**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
	public function searchFlyersIcon(Request $request)
	{
		$user = User::user();

		$productIdField = $request->get('productIDIcon');

		$searchFlyersResult = Flyer::where(array(
			['productID', 'LIKE', "%$productIdField%"]
			))->orderBy('created_at', 'desc')->paginate(8);


		if (count($searchFlyersResult) < 6) {
			$pagination = Flyer::paginate(0);
		} else {
			$pagination = Flyer::paginate(8);
		}

		$cookievalue = $_COOKIE['gridList'];

        // check if curent loged user has the flyer as favorite
		if (!isset($_COOKIE['favorite'])) {
			setcookie('favorite', json_encode([]), time() + 9999999, '/');
		}
		if (\Auth::check()) {
			$favoriteFlyers = Favorite::where('user_id', '=', $user->id)->pluck('flyer_id');
		} else {
			$favoriteFlyers = json_decode($_COOKIE['favorite'], true);
		}

		return view('pages.search', compact(['searchFlyersResult', 'pagination', 'cookievalue', 'somevlaue', 'favoriteFlyers']));
	}
}