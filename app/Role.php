<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Role extends Model
{

	public $timestamps = false;

    protected $fillable = [
        'user_id', 'role_id'
    ];

    public function users()
    {
    	return $this->belongsToMany('App\User', 'user_role', 'role_id', 'user_id');
    }
}
