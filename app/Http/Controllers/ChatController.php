<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Message;
use App\Events\MessagePosted;

class ChatController extends Controller
{
	public function home(){
		return view('pages.chat');
	}

	public function returnChatMessages(){
		$message = Message::with('user')->get();

		return $message;
	}

	public function sendMessage(){
    	// store the new php_user_filter
		$user = \Auth::user();

		$message = $user->messages()->create([
			'message' => request()->get('message')
			]);

		// announce that a new message has been posted
		broadcast(new MessagePosted($message, $user))->toOthers();

		return ['status' => 'ok'];
	}
}
