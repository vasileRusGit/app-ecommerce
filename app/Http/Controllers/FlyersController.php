<?php

namespace App\Http\Controllers;

use App\Favorite;
use App\Flyer;
use App\Http\Requests\FlyerRequest;
use App\Photo;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use URL;

class FlyersController extends Controller
{
    protected $baseDir = "images/photos";
    protected $tempDir = "images/temp";

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createFlyer()
    {
        $lastProductId = $this->productId();

        return view('flyers.create', compact('lastProductId'));
    }

    public function edit($id)
    {
        $flyer = Flyer::find($id);

        return view('/flyers.edit', compact('flyer'));
    }

    public function update($id, Request $request)
    {
        $flyer = Flyer::find($id);

        $this->validate($request, [
            'street' => 'required',
            'price'  => 'required',
            ]);

        $flyer->street      = Input::get('street');
        $flyer->city        = Input::get('city');
        $flyer->zip         = Input::get('zip');
        $flyer->country     = Input::get('country');
        $flyer->price       = Input::get('price');
        $flyer->description = Input::get('description');
        $flyer->featured    = Input::get('featured');
        // if number of admin featured flyers are more than 3 the featured in on and admin feature in null
        $numberOfFeaturedFlyers = DB::table('flyers')->where('admin_featured', 'on')->get();
        if (count($numberOfFeaturedFlyers) < 4) {
            $flyer->admin_featured = Input::get('featured');
        }
        $flyer->save();

        return redirect($flyer->zip . '/' . str_replace(' ', '-', $flyer->street));
    }

    public function destroy($id)
    {
        $flyer = Flyer::find($id);
        $flyer->delete();

        return redirect('/flyers');
    }

    public function flyerCreatePhoto(Request $request)
    {
        $this->validate($request, array(
            'file' => 'mimes:jpg,jpeg,png,bmp',
            ));

        // IF TEMP DIR IS NOT EPTY, DELETE ALL FIELS FROM TEMP DIRECTORY
        $filesFromTempDir = scandir('images/temp');
        $filesFromTempDir = array_diff($filesFromTempDir, [".", ".."]);

        if (count($filesFromTempDir) > 0) {
            foreach ($filesFromTempDir as $tempFiles) {
                preg_match("/([0-9]++)/isU", $tempFiles, $fileName);
                $timeFileName = (int) $fileName[1];
                // delete on new file upload;
                if ((time() > $timeFileName)) {
                    if ($_COOKIE['refreshAction'] == 'yes') {
                        unlink($this->tempDir . '/' . $tempFiles);
                    }
                }
            }
        }
        // add file to temp directory
        $file = $request->file('file');
        $name = time() . '-' . $file->getClientOriginalName();
        $file->move($this->tempDir, $name);
    }

    public function flyerEditPhoto($id)
    {
        $file  = \Request::file('file');
        $name  = time() . '-' . $file->getClientOriginalName();
        $flyer = Flyer::where('id', $id)->first();

        $file->move($this->baseDir, $name);
        $flyer->photos()->create(['name' => $name, 'path' => $this->baseDir . '/' . $name]);

        return redirect($flyer->zip . '/' . str_replace(' ', '-', $flyer->street));
    }

    /**
     * @param FlyerRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(FlyerRequest $request)
    {
        // link the flyer to the user
        $flyer = \Auth::user()->publish(new Flyer($request->all()));

        // MOVE IMAGES FORM TEMP FOLDER TO BASE DIR THEN DELETE THOSE IMAGES
        $filesFromTempDir = scandir('images/temp');
        $filesFromTempDir = array_diff($filesFromTempDir, [".", ".."]);
        $flyer            = Flyer::where('id', Flyer::max('id'))->first();
        foreach ($filesFromTempDir as $name) {
            copy($this->tempDir . '/' . $name, $this->baseDir . '/' . $name);
            $flyer->photos()->create(['name' => $name, 'path' => $this->baseDir . '/' . $name]);
            unlink($this->tempDir . '/' . $name);
        }

        // DELETE IF IMAGE IS NOT LINKED TO A FLYER
        $filesFromBaseDir                  = scandir('images/photos');
        $filesFromBaseDir                  = array_diff($filesFromBaseDir, [".", ".."]);
        $imageNamesFormFlyerPhotosDatabase = Photo::pluck('name');

        // save data from the database into an array
        if (count($imageNamesFormFlyerPhotosDatabase) > 0) {
            foreach ($imageNamesFormFlyerPhotosDatabase as $value) {
                $imageNamesFormDatabase[] = $value;
            }
        } else {
            $imageNamesFormDatabase = [];
        }

        if ($imageNamesFormDatabase !== null) {
            foreach ($filesFromBaseDir as $baseDirFileNames) {
                if (!in_array($baseDirFileNames, $imageNamesFormDatabase)) {
                    unlink($this->baseDir . '/' . $baseDirFileNames);
                }
            }
        }

        flash()->success('Success!', 'New product succesfully added!');

        return redirect($flyer->zip . '/' . str_replace(' ', '-', $flyer->street));
    }

    /**
     * @param $zip
     * @param $street
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showIndividualFlyer($zip, $street)
    {
        $flyer      = Flyer::locatedAt($zip, $street)->firstOrFail();
        $isSignedIn = User::isSignedIn();
        $user       = User::user();
        $isGuest    = User::isGuest();

        $reviewsAtachetToFlyer = DB::table('users')->join('reviews', 'users.id', '=', 'reviews.user_id')
        ->leftJoin('review_ratings', 'reviews.id', '=', 'review_ratings.review_id')
        ->where('reviews.flyer_id', $flyer->id)
        ->orderBy('users.created_at', 'desc')
        ->select('reviews.id', 'reviews.review', 'users.name', 'users.image_path', 'review_ratings.rating_review', 'reviews.created_at', 'reviews.user_id')
        ->get();

        $replyToReviews = DB::table('review_replies')->join('reviews', 'review_replies.review_id', '=', 'reviews.id')
        ->leftJoin('users', 'review_replies.user_id', '=', 'users.id')
        ->orderBy('review_replies.created_at', 'desc')
        ->select('review_replies.id', 'review_replies.review_id', 'users.image_path', 'users.name', 'review_replies.user_id', 'review_replies.created_at', 'review_replies.reply_review')
        ->get();

        // get form database the ratings and calculate the rating number
        $addedRatingFromDatabase      = $this->calculateTheRatings($flyer->id);
        $numberOfUsersThatRateProduct = $this->getTheNumberOfTheUserThatRateTheProduct($flyer->id);

        if ($numberOfUsersThatRateProduct == 0) {
            $calculateRating = 0;
        } else {
            $calculateRating = round($addedRatingFromDatabase / $numberOfUsersThatRateProduct);
        }

        $flyerId             = DB::table('flyers')->pluck('id');
        $lastInsertedFlyerId = 0;
        foreach ($flyerId as $product) {
            preg_match('/[0-9]++/isU', $product, $found);
            if ($lastInsertedFlyerId < $found) {
                $lastInsertedFlyerId = $found;
            }
        }

        // reviews
        $reviews = DB::table('reviews')->where('flyer_id', $flyer->id)->orderBy('created_at', 'desc')->get();

        foreach ($flyerId as $id) {
            $flyerPhotos[] = DB::table('flyer_photos')->where('flyer_id', '=', $id)->first();
        }
        $similarFlyerId = DB::table('flyers')->get();

        // check if curent loged user has the flyer as favorite
        if (!isset($_COOKIE['favorite'])) {
            setcookie('favorite', json_encode([]), time() + 9999999, '/');
        }
        if (\Auth::check()) {
            $favoriteFlyers = (array) Favorite::where('user_id', '=', $user->id)->pluck('flyer_id');
        } else {
            $favoriteFlyers[] = json_decode($_COOKIE['favorite'], true);
        }

        // update admin_featured to have the same value as featured
        $prevUrl = URL::previous();
        if (strpos($prevUrl, 'create') !== false) {
            $numberOfFeaturedFlyers = DB::table('flyers')->where('admin_featured', 'on')->get();
            if (count($numberOfFeaturedFlyers) < 3) {
                $featureCheckbox      = DB::table('flyers')->where('id', $lastInsertedFlyerId)->pluck('featured');
                $featureCheckboxState = $featureCheckbox[0];

                DB::table('flyers')->where('id', $lastInsertedFlyerId)->update(['admin_featured' => $featureCheckboxState]);
            }
        }

        return view('flyers.show', compact('flyer', 'isSignedIn', 'user', 'isGuest', 'favoriteFlyers', 'flyerPhotos', 'similarFlyerId', 'calculateRating', 'numberOfUsersThatRateProduct', 'reviews', 'reviewsAtachetToFlyer', 'replyToReviews', 'numberOfReviewRates', 'reviewRates'));
    }

    public function showAllFlyers(Request $request)
    {
        $user = User::user();

        $allFlyers  = Flyer::with('photos')->with('rating')->with('review_ratings')->orderBy('created_at', 'desc')->paginate(12);
        $pagination = Flyer::paginate(12);

        if (!isset($_COOKIE['gridList'])) {
            setcookie('gridList', 'list', time() + 9999999, '/');
        }
        $cookievalue = $_COOKIE['gridList'];

        // check if curent loged user has the flyer as favorite
        if (!isset($_COOKIE['favorite'])) {
            setcookie('favorite', json_encode([]), time() + 9999999, '/');
        }
        if (\Auth::check()) {
            $favoriteFlyers = Favorite::where('user_id', '=', $user->id)->pluck('flyer_id');
        } else {
            $favoriteFlyers = json_decode($_COOKIE['favorite'], true);
        }



        return view('flyers.showAll', compact('allFlyers', 'pagination', 'cookievalue', 'favoriteFlyers', 'ratingNumberFromDatabase'));
    }

    public function ratingForModal(Request $request){

        $flyerId = $request->input('flyerId');

        // if ($flyerId == null) {
        //     $ratingNumberFromDatabase = 0;

        //     return view('flyers.showAll', compact('allFlyers', 'pagination', 'cookievalue', 'favoriteFlyers', 'ratingNumberFromDatabase'));
        // } else {
            // get form database the ratings and calculate the rating number
        $addedRatingFromDatabase      = $this->calculateTheRatings($flyerId);
        $numberOfUsersThatRateProduct = $this->getTheNumberOfTheUserThatRateTheProduct($flyerId);
        dd($numberOfUsersThatRateProduct);

        if ($numberOfUsersThatRateProduct == 0) {
            $ratingNumberFromDatabase = 0;
        } else {
            $ratingNumberFromDatabase = round($addedRatingFromDatabase / $numberOfUsersThatRateProduct);
        }

        return $ratingNumberFromDatabase;

            // return view('flyers.showAll', compact('allFlyers', 'pagination', 'cookievalue', 'favoriteFlyers', 'ratingNumberFromDatabase'));
        // }
    }

    public function showFlyerRationOnModal(Request $request)
    {
        $flyerId = $request->input('flyerId');

        // get form database the ratings and calculate the rating number
        $addedRatingFromDatabase      = $this->calculateTheRatings($flyerId);
        $numberOfUsersThatRateProduct = $this->getTheNumberOfTheUserThatRateTheProduct($flyerId);

        if ($numberOfUsersThatRateProduct == 0) {
            $ratingNumberFromDatabase = 0;
        } else {
            $ratingNumberFromDatabase = round($addedRatingFromDatabase / $numberOfUsersThatRateProduct);
        }

        $user = User::user();

        $allFlyers  = Flyer::with('photos')->orderBy('created_at', 'desc')->paginate(12);
        $pagination = Flyer::paginate(12);

        if (!isset($_COOKIE['gridList'])) {
            setcookie('gridList', 'list', time() + 9999999, '/');
        }
        $cookievalue = $_COOKIE['gridList'];

        // check if curent loged user has the flyer as favorite
        if (!isset($_COOKIE['favorite'])) {
            setcookie('favorite', json_encode([]), time() + 9999999, '/');
        }
        if (\Auth::check()) {
            $favoriteFlyers = Favorite::where('user_id', '=', $user->id)->pluck('flyer_id');
        } else {
            $favoriteFlyers = json_decode($_COOKIE['favorite'], true);
        }

        return view('flyers.showAll', compact('allFlyers', 'pagination', 'cookievalue', 'favoriteFlyers', 'ratingNumberFromDatabase'));
    }

    public function showFeaturedFlyers()
    {
        $allFeaturedFlyers = Flyer::with('photos')->orderBy('created_at', 'desc')->where('admin_featured', 'on')->get();

        return $allFeaturedFlyers;
    }

    public function unauthorized(Request $request)
    {
        if ($request->ajax()) {
            return response(array('message' => 'No way.'), 403);
        }
        flash('No way.');

        return redirect('/');
    }

    public function homeMethod()
    {
        $flyers            = Flyer::get();
        $allFeaturedFlyers = $this->showFeaturedFlyers();

        return view('pages.home', compact('flyers', 'allFeaturedFlyers'));
    }

    public function grid()
    {
        $items = Flyer::get();

        return view('pages.gridList', compact('items'));
    }

    public function productId()
    {
        // get product id column from database
        $flyerProductID[] = DB::table('flyers')->pluck('productID');

        foreach ($flyerProductID as $productId) {
            $flyerProductIds = $productId;
        }
        if (count($flyerProductIds) == 0) {
            $lastProductId = "0";
        } else {
            $productIdArray   = end($flyerProductIds);
            $productIdNumbers = 0;
            foreach ($productIdArray as $product) {
                preg_match('/[0-9]++/isU', $product, $found);
                if ($productIdNumbers < $found) {
                    $productIdNumbers = $found;
                }
            }

            $lastProductId = $productIdNumbers[0];
        }
        return $lastProductId;
    }

    public function calculateTheRatings($flyerId)
    {
        // get aform database the ratings and calculate the rating number
        $ratingsFromDatabase = DB::table('ratings')->where('flyer_id', $flyerId)->pluck('rating');
        $flyerReviewRatings  = DB::table('review_ratings')->where('flyer_id', $flyerId)->pluck('rating_review');
        $flyerRatesDB        = 0;
        foreach ($ratingsFromDatabase as $rating) {
            $flyerRatesDB += $rating;
        }

        $reviewRatesDB = 0;
        foreach ($flyerReviewRatings as $reviewRate) {
            if ($reviewRate > 0) {
                $reviewRatesDB += $reviewRate;
            }
        }

        return $flyerRatesDB + $reviewRatesDB;
    }

    public function getTheNumberOfTheUserThatRateTheProduct($flyerId)
    {
        $ratingsFromDatabase = DB::table('ratings')->where('flyer_id', $flyerId)->pluck('rating');
        $flyerReviewRatings  = DB::table('review_ratings')->where('flyer_id', $flyerId)->pluck('rating_review');

        $numberOfFlyerRatesDB = count($ratingsFromDatabase);

        $numberOfReviewRatesDB = 0;
        foreach ($flyerReviewRatings as $reviewRate) {
            if ($reviewRate > 0) {
                $numberOfReviewRatesDB++;
            }
        }

        return $numberOfFlyerRatesDB + $numberOfReviewRatesDB;
    }

    public function ajaxCall()
    {
//        return false;
    }
}
