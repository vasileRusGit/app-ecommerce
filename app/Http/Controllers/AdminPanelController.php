<?php

namespace App\Http\Controllers;

use App\Http\Requests\FlyerRequest;
use App\Photo;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Request;
use App\Flyer;
use App\User;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class AdminPanelController extends Controller
{
    // protected $baseDir = "images/photos";
    // protected $tempDir = "images/temp";

    public function dashboard(Request $request)
    {
        $flyer = Flyer::get();
        $user = User::user();
        $toggleStatus = $_COOKIE['adminToggle'];

        return view('adminLte', compact('flyer', 'user', 'toggleStatus'));
    }
}
