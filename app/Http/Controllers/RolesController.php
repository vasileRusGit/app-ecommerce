<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\Flyer;
use App\Http\Controllers\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

class RolesController extends Controller
{
    public function usersPermission(Request $request)
    {
    	$users = User::all();
        $flyer = Flyer::get();
        $user = User::user();
        $toggleStatus = $_COOKIE['adminToggle'];

        return view('adminLte', compact('users', 'flyer', 'user','toggleStatus'));
    }

    public function changeUserPermision($id){
        // get the user permision from database
        $user = User::where('id', $id)->first();

        // dd($user);

        if(!empty($user)){
            $user->roles()->detach();
            if(Input::get('role_user')){
                $user->roles()->attach(Role::where('name', 'User')->first());
            }
            if(Input::get('role_employee')){
                $user->roles()->attach(Role::where('name', 'Employee')->first());
            }
            if(Input::get('role_admin')){
                $user->roles()->attach(Role::where('name', 'Admin')->first());
            }
        }
        return redirect()->back();
    }
}