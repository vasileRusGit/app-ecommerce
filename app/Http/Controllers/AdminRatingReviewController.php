<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Flyer;
use App\Review;
use App\Rating;
use App\ReviewReply;
use App\ReviewRating;
use App\User;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

class AdminRatingReviewController extends Controller
{
	public function showAdminFlyersRating()
	{
		$flyer = Flyer::get();
		$user = User::user();
		$toggleStatus = $_COOKIE['adminToggle'];

		$ratedFlyers = DB::table('ratings')->pluck('flyer_id');
		$flyersRating = DB::table('flyers')->join('ratings', 'flyers.id', '=', 'ratings.flyer_id')
		->join('users', 'ratings.user_id', '=', 'users.id')
		->select('ratings.id', 'ratings.flyer_id', 'users.name', 'ratings.created_at', 'ratings.rating','flyers.productID')
		->get();

		if(count($flyersRating) == 0){
			$ratings = [];
		}else{
			foreach($flyersRating as $rated){
				$ratings[] = $rated;
			}
		}

		return view('adminLte', compact('ratings','user', 'toggleStatus', 'flyer'));
	}

	public function updateFlyersRating()
	{
		$id = Input::get('id');
		$newRatingValue = Input::get('adminRatedFlyer');
		DB::table('ratings')->where('id', $id)->update(['rating' => $newRatingValue]);

		return redirect()->back();
	}

	public function destroyAdminFlyerRating($id)
	{
		$flyerReview = Rating::find($id);
		$flyerReview->delete();

		return redirect()->back();
	}

	public function showAdminFlyersReview()
	{
		$user = User::user();
		$flyer = Flyer::get();
		$toggleStatus = $_COOKIE['adminToggle'];

		$flyersReviewd = DB::table('users')->join('reviews', 'users.id', '=', 'reviews.user_id')
		->join('flyers', 'reviews.flyer_id', '=', 'flyers.id')
		->select('reviews.id', 'users.name', 'flyers.productID', 'reviews.flyer_id', 'flyers.street')
		->get();

		if(count($flyersReviewd) == 0){
			$reviews = [];
		}else{
			foreach($flyersReviewd as $review){
				$reviews[] = $review;
			}
		}

		return view('adminLte', compact('reviews','user','toggleStatus', 'flyer'));
	}

	public function editAdminReview($id)
	{
		$user = User::user();
		$flyer = Flyer::get();
		$toggleStatus = $_COOKIE['adminToggle'];

		$review = Review::find($id);
		$replyReviews = DB::table('review_replies')->join('users', 'review_replies.user_id', 'users.id')
		->where('review_id', $review->id)
		->select('review_replies.id', 'users.name', 'review_replies.reply_review')
		->get();

		$ratingReviews = DB::table('review_ratings')->join('users', 'review_ratings.user_id', 'users.id')
		->where('review_id', $review->id)
		->select('review_ratings.id', 'users.name', 'review_ratings.rating_review')
		->get();

		return view('adminLte', compact('review', 'replyReviews', 'ratingReviews', 'user', 'toggleStatus', 'flyer'));
	}

	public function updateAdminReview(Request $request, $id)
	{
		$reviewText = $request->input('reviewTextareaText');

		DB::table('reviews')->where('id', $id)->update(['review' => $reviewText]);

		return redirect()->back();
	}

	

	public function updateFlyersReview(){
		$id = Input::get('id');
		$newReviewValue = Input::get('adminReviewFlyer');
		DB::table('reviews')->where('id', $id)->update(['review' => $newReviewValue]);

		return redirect()->back();
	}


	public function updateAdminReplyReview(Request $request, $id)
	{
		$editReplyReviewText = $request->input('editReplyReviewTextareaAdminModal');

		DB::table('review_replies')->where('id', $id)->update(['reply_review' => $editReplyReviewText]);

		return redirect()->back();
	}

	public function destroyAdminReplyReview(Request $request, $id)
	{
		$replyReview = ReviewReply::find($id);
		$replyReview->delete();

		return redirect()->back();
	}

	public function destroyAdminReview($id)
	{
		$review = Review::find($id);
		$review->delete();

		return redirect()->back();
	}

	public function updateAdminRatingReview(Request $request, $id)
	{
		$ratingValue = $request->input('editReviewRatingAdminModal');
		DB::table('review_ratings')->where('id', $id)->update(['rating_review' => $ratingValue]);
		
		return redirect()->back();
	}

	public function destroyAdminRatingReview($id)
	{
		$ratingReview = ReviewRating::find($id);
		$ratingReview->delete();

		return redirect()->back();
	}

	public function replyReviewAdminModal(Request $request, $id)
	{
		$replyReviewText = $request->input('replyReviewTextareaAdminModal');
		$user = User::user();
		$flyerId = DB::table('reviews')->where('id', $id)->select('flyer_id')->get();

		$replyReview = new ReviewReply();
		$replyReview->user_id = $user->id;
		$replyReview->review_id = $id;
		$replyReview->flyer_id = $flyerId[0]->flyer_id;
		$replyReview->reply_review = $replyReviewText;
		$replyReview->save();

		return redirect()->back();
	}

	public function rateReviewAdminModal(Request $request, $id)
	{
		$rateReviewText = $request->input('reviewRatingAdminModal');
		$user = User::user();
		$flyer = DB::table('reviews')->where('id', $id)->select('flyer_id')->get();
		$flyerId = $flyer[0]->flyer_id;

		$rateReview = new ReviewRating();
		$rateReview->user_id = $user->id;
		$rateReview->flyer_id = $flyerId;
		$rateReview->review_id = $id;
		$rateReview->rating_review = $rateReviewText;
		$rateReview->save();

		return redirect()->back();
	}

	

}
