<?php


namespace App\Http\Controllers;

use App\Http\Requests\FlyerRequest;
use App\Photo;
use Illuminate\Http\Request;
use App\Flyer;

class PhotosController extends Controller
{
    public function destroy($id)
    {
        Photo::find($id)->delete();

        return back();
    }
}