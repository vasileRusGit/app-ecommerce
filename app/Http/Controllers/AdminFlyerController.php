<?php

namespace App\Http\Controllers;

use App\Http\Requests\FlyerRequest;
use App\Photo;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Request;
use App\Flyer;
use App\User;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class AdminFlyerController extends Controller
{
    protected $baseDir = "images/photos";
    protected $tempDir = "images/temp";

    public function addNewAdminFlyer()
    {
        $flyer = Flyer::get();
        $user = User::user();
        $toggleStatus = $_COOKIE['adminToggle'];

        $flyerController = new FlyersController();
        $lastProductId = $flyerController->productId();

        return view('adminLte', compact('flyer', 'user', 'toggleStatus', 'lastProductId'));
    }

    public function dashboardHeader()
    {
        $flyer = Flyer::get();

        return view('admin.header', compact('flyer'));
    }


    public function addAdminFlyerPhoto(Request $request)
    {
        $this->validate($request, array(
            'file' => 'required|mimes:jpg,jpeg,png,bmp'
            ));

        // IF TEMP DIR IS NOT EMPTY, DELETE ALL FILES FROM TEMP DIRECTORY
        $filesFromTempDir = scandir('images/temp');
        $filesFromTempDir = array_diff($filesFromTempDir, [".", ".."]);

        if (count($filesFromTempDir) > 0) {
            foreach ($filesFromTempDir as $tempFiles) {
                preg_match("/([0-9]++)/isU", $tempFiles, $fileName);
                $timeFileName = (int)$fileName[1];
                if (time() > $timeFileName) {
                    unlink($this->tempDir . '/' . $tempFiles);
                }
            }
        }
        // add file to temp directory
        $file = $request->file('file');
        $name = time() . '-' . $file->getClientOriginalName();
        $file->move($this->tempDir, $name);
    }

    public function storeAdminFlyer(FlyerRequest $request)
    {
        // link the flyer to the user
        $flyer = \Auth::user()->publish(new Flyer($request->all()));

        // MOVE IMAGES FORM TEMP FOLDER TO BASE DIR THEN DELETE THOSE IMAGES
        $filesFromTempDir = scandir('images/temp');
        $filesFromTempDir = array_diff($filesFromTempDir, [".", ".."]);
        $flyer            = Flyer::where('id', Flyer::max('id'))->first();
        foreach ($filesFromTempDir as $name) {
            copy($this->tempDir . '/' . $name, $this->baseDir . '/' . $name);
            $flyer->photos()->create(['name' => $name, 'path' => $this->baseDir . '/' . $name]);
            unlink($this->tempDir . '/' . $name);
        }

        // DELETE IF IMAGE IS NOT LINKED TO A FLYER
        $filesFromBaseDir                  = scandir('images/photos');
        $filesFromBaseDir                  = array_diff($filesFromBaseDir, [".", ".."]);
        $imageNamesFormFlyerPhotosDatabase = Photo::pluck('name');

        // save data from the database into an array
        if (count($imageNamesFormFlyerPhotosDatabase) > 0) {
            foreach ($imageNamesFormFlyerPhotosDatabase as $value) {
                $imageNamesFormDatabase[] = $value;
            }
        } else {
            $imageNamesFormDatabase = [];
        }

        if ($imageNamesFormDatabase !== null) {
            foreach ($filesFromBaseDir as $baseDirFileNames) {
                if (!in_array($baseDirFileNames, $imageNamesFormDatabase)) {
                    unlink($this->baseDir . '/' . $baseDirFileNames);
                }
            }
        }

        flash()->success('Success!', 'New product succesfully added!');

        // redirect ot landing page
        return redirect('/admin/flyers/show');
    }

    public function showAdminFlyers()
    {
        $flyers = Flyer::with('photos')->orderBy('id', 'desc')->get();
        $flyer = Flyer::get();
        $user = User::user();
        $toggleStatus = $_COOKIE['adminToggle'];

        return view('adminLte', compact('flyers', 'flyer', 'user', 'toggleStatus'));
    }

    public function showAdminFeaturedFlyers()
    {
        $flyers = Flyer::with('photos')->orderBy('id', 'desc')->where('featured', 'on')->get();
        $flyer = Flyer::get();
        $user = User::user();
        $toggleStatus = $_COOKIE['adminToggle'];

        return view('adminLte', compact('flyers', 'flyer', 'user', 'toggleStatus'));
    }

    public function showAdminFlyersRating()
    {
        $user = User::user();
        $toggleStatus = $_COOKIE['adminToggle'];

        $ratedFlyers = DB::table('ratings')->pluck('flyer_id');
        $flyersRating = DB::table('flyers')->join('ratings', 'flyers.id', '=', 'ratings.flyer_id')
                                            ->join('users', 'ratings.user_id', '=', 'users.id')
                                            ->get();

        foreach($flyersRating as $rated){
            $flyers[] = $rated;
        }

        return view('adminLte', compact('flyers','user', 'toggleStatus'));
    }

    public function showAdminFlyersReview()
    {
        $user = User::user();
        $toggleStatus = $_COOKIE['adminToggle'];

        $flyersReviewd = DB::table('users')->join('reviews', 'users.id', '=', 'reviews.user_id')
                                            ->join('flyers', 'reviews.flyer_id', '=', 'flyers.id')
                                            ->select('reviews.id', 'users.name', 'flyers.productID', 'reviews.flyer_id', 'flyers.street')
                                            ->get();

        foreach($flyersReviewd as $review){
            $reviews[] = $review;
        }

        return view('adminLte', compact('reviews','user','toggleStatus'));
    }

    public function editAdminFlyer($id)
    {
        $flyer = Flyer::find($id);
        $user = User::user();
        $toggleStatus = $_COOKIE['adminToggle'];

        // get product id column from database
        $flyerController = new FlyersController();
        $lastProductId = $flyerController->productId();

        return view('adminLte', compact('flyer', 'user', 'toggleStatus', 'lastProductId'));
    }

    public function updateAdminFlyer($id)
    {
        $flyer = Flyer::find($id);
        $user = User::user();
        $toggleStatus = $_COOKIE['adminToggle'];

        $flyer->street = Input::get('street');
        $flyer->city = Input::get('city');
        $flyer->zip = Input::get('zip');
        $flyer->country = Input::get('country');
        $flyer->price = Input::get('price');
        $flyer->description = Input::get('description');
        $flyer->featured = Input::get('featured');
        $flyer->save();

        return redirect()->route('admin.flyer.store')->with(compact('user', 'flyer', 'toggleStatus'));
    }

    public function updateAdminFlyerPhoto($id)
    {
        $file = \Request::file('file');
        $name = time() . '-' . $file->getClientOriginalName();
        $flyer = Flyer::where('id', $id)->first();

        $file->move($this->baseDir, $name);
        $flyer->photos()->create(['name' => $name, 'path' => $this->baseDir . '/' . $name]);

        return 'Admin update admin photo successfully';
    }

    public function destroyAdminFlyer($id)
    {
        $flyer = Flyer::find($id);
        // dd($flyer);
        $flyer->delete();

        return redirect()->back();
    }

    public function featuredFlyers()
    {
        $id = Input::get('id');
        $featureCheckboxState = Input::get('adminFeatured');
        $allAdminFeaturedFlyers = DB::table('flyers')->where('admin_featured', 'on')->get();

        if(count($allAdminFeaturedFlyers) == 4){
            if($featureCheckboxState == null){
                DB::table('flyers')->where('id', $id)->update(['admin_featured' => $featureCheckboxState]);
            }else{
                // flash messages
                flash()->info('Attention!', "You can have only 4 featured flyers on main page!");
            }
        }else if(count($allAdminFeaturedFlyers) < 4){
            DB::table('flyers')->where('id', $id)->update(['admin_featured' => $featureCheckboxState]);
        }

        return redirect()->back();
    }
}