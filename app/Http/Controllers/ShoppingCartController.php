<?php

namespace App\Http\Controllers;

use App\CreditCard;
use App\DeliveryAddress;
use App\Flyer;
use App\InvoiceProducts;
use App\Invoices;
use App\ShoppingCart;
use App\User;
use Braintree_CreditCard;
use Braintree_Customer;
use Braintree_Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ShoppingCartController extends Controller
{
    public function addToCart()
    {
        return view('cart.addToCart');
    }

    public function shoppingCart()
    {
        return view('cart.shoppingCart');
    }

    public function cartProducts()
    {
        return DB::table('shopping_carts')->join('flyers', 'shopping_carts.flyer_id', 'flyers.id')->where('shopping_carts.user_id', User::user()->id)->get();
    }

    public function cartDetails()
    {
        // dd(User::user()->id);
        if (\Auth::check()) {
            $cartProducts = DB::table('shopping_carts')->join('flyers', 'shopping_carts.flyer_id', 'flyers.id')->get();
            // dd($cartProducts);
        } else {
            $cartProducts = $cookievalue = $_COOKIE['cart'];
        }
        return view('cart.cartDetails', compact('cartProducts'));
    }

    public function checkout()
    {
        return view('cart.checkout');
    }

    public function userDeliveryAddress()
    {
        if (\Auth::check()) {
            return DB::table('delivery_addresses')->where('user_id', User::user()->id)->select('country', 'state', 'address', 'zip', 'phone')->get();
        }
    }

    public function userCreditCardInformation()
    {
        if (\Auth::check()) {
            return DB::table('credit_cards')->where('user_id', User::user()->id)->select('card_number_1', 'card_number_2', 'card_number_3', 'card_number_4', 'card_holder', 'card_expiration_month', 'card_expiration_year', 'card_ccv')->get();
        }
    }

    public function userEmail()
    {
        return DB::table('users')->pluck('email');
    }

    public function addProductToShoppingCart(Request $request)
    {
        $productName     = $request->get('productName');
        $productPrice    = $request->get('productPrice');
        $productQuantity = $request->get('productQuantity');
        $productImage    = $request->get('productImage');
        $flyerId         = Flyer::where('street', $productName)->select('id')->get();

        if (\Auth::check()) {
            $cart                     = new ShoppingCart();
            $cart->user_id            = User::user()->id;
            $cart->flyer_id           = $flyerId[0]->id;
            $cart->product_name       = $productName;
            $cart->product_price      = $productPrice;
            $cart->product_qty        = $productQuantity;
            $cart->product_image_path = $productImage;
            $cart->save();

            return response()->json(['message' => 'Added to shopping cart DB!'], 200);
        }
    }

    public function deleteProductFromShoppingCart(Request $request)
    {
        $productName = $request->get('productName');
        $cart        = ShoppingCart::where('product_name', $productName);
        $cart->delete();
    }

    public function incrementProductQty(Request $request)
    {
        $productQuantity = $request->get('productQuantity');
        $productName     = $request->get('productName');

        if (\Auth::check()) {
            DB::table('shopping_carts')->where('product_name', $productName)->update(['product_qty' => $productQuantity]);

            return response()->json(['message' => 'Incremented product qty to shopping cart DB!'], 200);
        }
    }

    public function decrementProductQty(Request $request)
    {
        $productQuantity = $request->get('productQuantity');
        $productName     = $request->get('productName');

        if (\Auth::check()) {
            DB::table('shopping_carts')->where('product_name', $productName)->update(['product_qty' => $productQuantity]);

            return response()->json(['message' => 'Decrement product qty to shopping cart DB!'], 200);
        }
    }

    public function thankyou()
    {
        return view('cart.thankyou');
    }

    public function showInvoices()
    {
        $invoices = DB::table('invoices')
            ->leftJoin('delivery_addresses', 'invoices.user_id', 'delivery_addresses.user_id')
            ->leftJoin('users', 'invoices.user_id', 'users.id')
            ->where('invoices.user_id', \Auth::user()->id)->orderBy('invoices.created_at', 'desc')->get();

        // dd($invoices);

        return view('cart.invoice', compact('invoices'));
    }

    public function previewInvoice($invoice_id)
    {
        $invoice = DB::table('invoices')->where('invoice_id', $invoice_id)
            ->join('delivery_addresses', 'invoices.user_id', 'delivery_addresses.user_id')
            ->join('users', 'invoices.user_id', 'users.id')
            ->first();

        $invoiceProducts = DB::table('invoices')->join('invoice_products', 'invoices.id', 'invoice_products.invoice_id')
            ->where('invoices.invoice_id', $invoice_id)->get();

        // dd($invoiceProducts);

        return view('cart.invoicePreview', compact('invoice', 'invoiceProducts'));
    }

    ///////////////////// payment area
    public function acceptPayment(Request $request)
    {
        // payment method
        $paymentMethod = $request->get('paymentMethod');

        // customer informations
        $customerFirstName = $request->get('customerFirstName');
        $customerLastName  = $request->get('customerLastName');
        $customerEmail     = $request->get('customerEmail');
        $customerPassword  = $request->get('customerPassword');
        $customerAddress   = $request->get('customerAddress');
        $customerCountry   = $request->get('customerCountry');
        $customerState     = $request->get('customerState');
        $customerZip       = $request->get('customerZip');
        $customerPhone     = $request->get('customerPhone');
        $paymentSum        = $request->get('paymentSum');

        // credit card information
        $cardNumber      = $request->get('cardNumber');
        $cardHolder      = $request->get('cardHolder');
        $expirationMonth = $request->get('expirationMonth');
        $expirationYear  = $request->get('expirationYear');
        $cardCCV         = $request->get('ccv');

        // products
        $products = $request->get('products');

        $customer_id = $this->registerUserOnBrainTree($customerFirstName, $customerLastName, $customerEmail, $customerPhone);
        $card_token  = $this->getCardToken($customer_id, $cardNumber, $expirationMonth, $expirationYear, $cardCCV);

        /// gateway will provide this plan id whenever you creat plans there
        $transction_id = $this->createTransaction($card_token, $customer_id, $paymentSum);

        // call the function to create the user
        $this->createUser($customerFirstName, $customerLastName, $customerEmail, $customerPassword);

        // call teh fucntion to create the credit card
        $this->createCreditCard($cardNumber, $cardHolder, $expirationMonth, $expirationYear, $cardCCV);

        // call teh fucntion to create the use delivery address
        $this->createDeliveryAddress($customerCountry, $customerAddress, $customerState, $customerZip, $customerPhone);

        // call the function to create the invoice
        $this->createInvoice($transction_id, $paymentMethod, $paymentSum);

        // call teh fucntion to create the use delivery address
        $this->createInvoiceProducts($products);

        return response('Your transaction was succesfully register', 200);
    }

    public function createInvoiceProducts($products)
    {
        $invoice_id = DB::table('invoices')->orderBy('created_at', 'desc')->first()->id;

        foreach ($products as $product) {
            // dd($product);
            $productId = DB::table('flyers')->where('productID', $product['productID'])->first()->id;

            $invoiceProduct                = new InvoiceProducts();
            $invoiceProduct->invoice_id    = $invoice_id;
            $invoiceProduct->product_id    = $productId;
            $invoiceProduct->product_name  = $product['product_name'];
            $invoiceProduct->product_qty   = $product['product_qty'];
            $invoiceProduct->product_price = $product['product_price'];
            $invoiceProduct->save();
        }
    }

    public function getCardToken($customer_id, $cardNumber, $expirationMonth, $expirationYear, $cardCCV)
    {
        $card_result = Braintree_CreditCard::create(array(
            'number'          => $cardNumber,
            'expirationMonth' => $expirationMonth,
            'expirationYear'  => $expirationYear,
            'customerId'      => $customer_id,
            'cvv'             => $cardCCV,
        ));
        if ($card_result->success) {
            return $card_result->creditCard->token;
        } else {
            return false;
        }
    }

    public function createTransaction($creditCardToken, $customerId, $paymentSum)
    {
        $result = Braintree_Transaction::sale(
            [
                'customerId' => $customerId,
                'amount'     => $paymentSum,
                'orderId'    => '131231',
            ]
        );
        if ($result->success) {
            return $result->transaction->id;
        } else {
            $errorFound = '';
            foreach ($result->errors->deepAll() as $error1) {
                $errorFound .= $error1->message . "<br />";
            }
        }
    }

    public function registerUserOnBrainTree($customerFirstName, $customerLastName, $customerEmail, $customerPhone)
    {
        $result = Braintree_Customer::create(array(
            'firstName' => $customerFirstName,
            'lastName'  => $customerLastName,
            'email'     => $customerEmail,
            'phone'     => $customerPhone,
        ));
        if ($result->success) {
            return $result->customer->id;
        } else {
            $errorFound = '';
            foreach ($result->errors->deepAll() as $error) {
                $errorFound .= $error->message . "<br />";
            }
            echo $errorFound;
        }
    }

    public function createCreditcard($cardNumber, $cardHolder, $expirationMonth, $expirationYear, $cardCCV)
    {
        $cardNumber1 = substr($cardNumber, 0, -12);
        $cardNumber2 = substr($cardNumber, 4, -8);
        $cardNumber3 = substr($cardNumber, 8, -4);
        $cardNumber4 = substr($cardNumber, 12);

        // check to see if a credit card already exist for the curent user
        if (count(DB::table('credit_cards')->where('user_id', \Auth::user()->id)->get()) > 0) {
            DB::table('credit_cards')->where('user_id', \Auth::user()->id)->update([
                'card_number_1'         => $cardNumber1,
                'card_number_2'         => $cardNumber2,
                'card_number_3'         => $cardNumber3,
                'card_number_4'         => $cardNumber4,
                'card_holder'           => $cardHolder,
                'card_expiration_month' => $expirationMonth,
                'card_expiration_year'  => $expirationYear,
                'card_ccv'              => $cardCCV,
            ]);
        } else {
            $creditCard                        = new CreditCard();
            $creditCard->user_id               = \Auth::user()->id;
            $creditCard->card_number_1         = $cardNumber1;
            $creditCard->card_number_2         = $cardNumber2;
            $creditCard->card_number_3         = $cardNumber3;
            $creditCard->card_number_4         = $cardNumber4;
            $creditCard->card_holder           = $cardHolder;
            $creditCard->card_expiration_month = $expirationMonth;
            $creditCard->card_expiration_year  = $expirationYear;
            $creditCard->card_ccv              = $cardCCV;
            $creditCard->save();
        }
    }

    public function createDeliveryAddress($customerCountry, $customerAddress, $customerState, $customerZip, $customerPhone)
    {
        // check to see if a credit card already exist for the curent user
        if (count(DB::table('delivery_addresses')->where('user_id', \Auth::user()->id)->get()) > 0) {
            DB::table('delivery_addresses')->where('user_id', \Auth::user()->id)->update([
                'country' => $customerCountry,
                'state'   => $customerState,
                'address' => $customerAddress,
                'zip'     => $customerZip,
                'phone'   => $customerPhone,
            ]);
        } else {
            $deliveryAddress          = new DeliveryAddress();
            $deliveryAddress->user_id = \Auth::user()->id;
            $deliveryAddress->country = $customerCountry;
            $deliveryAddress->state   = $customerState;
            $deliveryAddress->address = $customerAddress;
            $deliveryAddress->zip     = $customerZip;
            $deliveryAddress->phone   = $customerPhone;
            $deliveryAddress->save();
        }
    }

    // create invoices
    public function createInvoice($transction_id, $paymentMethod, $paymentSum)
    {
        $invoice = new Invoices();

        $allinvoice_ids = DB::table('invoices')->pluck('invoice_id');
        // convert object into array
        $invoice_ids = [];
        for ($j = 0; $j <= count($allinvoice_ids) - 1; $j++) {
            array_push($invoice_ids, $allinvoice_ids[$j]);
        }

        $randomNumber   = rand(1, 999999);
        $lastinvoice_id = end($invoice_ids);
        if (in_array($randomNumber, $invoice_ids)) {
            $randomNumber = $lastinvoice_id + 1;
        }

        if (\Auth::check()) {
            $userId = User::user()->id;

            $invoice->user_id        = $userId;
            $invoice->invoice_id     = $randomNumber;
            $invoice->transaction_id = $transction_id;
        } else {
            // get the last user id
            $userId = DB::table('users')->select('id')->orderBy('updated_at', 'desc')->first();

            $invoice->user_id        = $userId->id;
            $invoice->invoice_id     = $randomNumber;
            $invoice->transaction_id = $transction_id;
            $invoice->payment_sum    = $paymentSum;

            $invoice->save();
        }

        $invoice->payment_method = $paymentMethod;
        $invoice->payment_sum    = $paymentSum;
        $invoice->save();
    }

    public function createUser($customerFirstName, $customerLastName, $customerEmail, $customerPassword)
    {
        if (\Auth::check()) {
            return;
        } else {
            $user           = new User();
            $user->name     = $customerFirstName . ' ' . $customerLastName;
            $user->email    = $customerEmail;
            $user->password = bcrypt(base64_decode($customerPassword));
            $user->save();

            // add role user
            $userId = DB::table('users')->where('email', $customerEmail)->first()->id;

            DB::table('user_role')->insert(['user_id' => $userId, 'role_id' => 3]);

            // login the user
            $user_id = DB::table('users')->where('email', $customerEmail)->select('id')->get()[0]->id;
            // var_dump($user_id);
            \Auth::loginUsingId($user_id);
        }
    }
}
