<?php

namespace App\Http\Controllers;

use App\Flyer;
use App\Invoices;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

class AdminInvoiceController extends Controller
{
    public function addNewAdminInvoice()
    {
        $flyer        = Flyer::get();
        $user         = User::user();
        $toggleStatus = $_COOKIE['adminToggle'];

        $allUsers = DB::table('users')->get();

        // generate invoice id
        $allInvoiceIds = DB::table('invoices')->pluck('invoice_id');
        // convert object into array
        $invoiceIds = [];
        for ($j = 0; $j <= count($allInvoiceIds) - 1; $j++) {
            array_push($invoiceIds, $allInvoiceIds[$j]);
        }

        $invoiceId     = rand(1, 999999);
        $lastInvoiceId = end($invoiceIds);
        if (in_array($invoiceId, $invoiceIds)) {
            $invoiceId = $lastInvoiceId + 1;
        }

        return view('adminLte', compact('flyer', 'user', 'toggleStatus', 'allUsers', 'invoiceId'));
    }

    public function storeNewAdminInvoice(Request $request)
    {
        $invoice = new Invoices();

        // get data from the form
        $userName            = $request->input('user-name');
        $userDescription     = DB::table('users')->where('name', $userName)->first();
        $userDeliveryAddress = DB::table('delivery_addresses')->where('user_id', $userDescription->id)->get();

        $invoice->user_id            = $userDescription->id;
        $invoice->invoice_id          = $request->get('invoiceid');
        $invoice->payment_method     = $request->get('payment-method');
        $invoice->payment_sum        = $request->get('payment-sum');
        $invoice->save();

        return redirect('/admin/invoice/show');
    }

    public function manageAdminInvoice()
    {
        $flyer        = Flyer::get();
        $user         = User::user();
        $toggleStatus = $_COOKIE['adminToggle'];
        $invoices     = DB::table('invoices')->orderBy('created_at', 'desc')->get();

        $invoices = DB::table('invoices')
             ->leftJoin('users', 'invoices.user_id', 'users.id')
            ->leftJoin('delivery_addresses', 'invoices.user_id', 'delivery_addresses.user_id')
            ->select('invoices.id', 'invoices.invoice_id', 'invoices.user_id', 'invoices.transaction_id', 'invoices.payment_sum', 'invoices.payment_method', 'invoices.updated_at', 'delivery_addresses.country', 'delivery_addresses.state', 'delivery_addresses.address', 'delivery_addresses.zip', 'delivery_addresses.phone', 'users.name', 'users.email')
            ->orderBy('invoices.created_at', 'desc')->get();

            // dd($invoices);

        return view('adminLte', compact('user', 'flyer', 'toggleStatus', 'invoices'));
    }

    public function editAdminInvoice($invoice_id)
    {
        $flyer        = Flyer::get();
        $user         = User::user();
        $toggleStatus = $_COOKIE['adminToggle'];

        $invoice = DB::table('invoices')
        ->join('users', 'invoices.user_id', 'users.id')
        ->where('invoice_id', $invoice_id)->first();

        $allUsers = DB::table('users')->get();

        return view('adminLte', compact('invoice', 'allUsers', 'flyer', 'toggleStatus', 'user'));
    }

    public function updateAdminInvoice($invoice_id){
        $userId = DB::table('users')->where('name', Input::get('user-name'))->select('id')->first()->id;

        DB::table('invoices')->where('invoice_id', $invoice_id)->update(['user_id' => $userId, 'payment_method' => Input::get('payment-method'), 
            'payment_sum' => Input::get('payment-sum')]);

        return redirect('/admin/invoice/show');
    }

    public function destroyAdminInvoice($id)
    {
        $invoice = Invoices::find($id);
        $invoice->delete();

        return redirect()->back();
    }

}