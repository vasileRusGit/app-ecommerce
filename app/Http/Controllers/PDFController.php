<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\DB;
use PDF;

class PDFController extends Controller
{
    public function pdf($id){
    	$invoice   = DB::table('invoices')
    	->leftJoin('users', 'invoices.user_id', 'users.id')
    	->leftJoin('delivery_addresses', 'users.id', 'delivery_addresses.user_id')
    	->where('invoice_id', $id)->first();

    	$invoiceProducts = DB::table('invoices')->leftJoin('invoice_products', 'invoices.id', 'invoice_products.invoice_id')
        ->where('invoices.invoice_id', $id)->get();

    	// dd($invoice);

    	$pdf = PDF::loadView('cart.pdf', compact('invoice', 'invoiceProducts'));
    	return $pdf->download('invoice.pdf');
    }
}
