<?php

namespace App\Http\Controllers;

use App\Http\Requests\FlyerRequest;
use App\Photo;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Request;
use App\Flyer;
use App\User;
use App\CreditCard;
use App\DeliveryAddress;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class UserController extends Controller
{

	public function addNewUser(Request $request)
	{
		$flyer = Flyer::get();
		$user = User::user();
		$toggleStatus = $_COOKIE['adminToggle'];

		return view('adminLte', compact('flyer', 'user','toggleStatus'));
	}

	public function storeNewUser(Request $request)
	{
		$user = User::user();
		$newUser = new User();
		$toggleStatus = $_COOKIE['adminToggle'];

		$newUser->name = Input::get('name');
		$newUser->email = Input::get('email');
		$password = Input::get('password');
		$repeat_password = Input::get('repeat-password');

        // get file from form
		$file =  Input::file('userImage');

		if($file !== null){
			$userPhotoName = time() . '-' .$file->getClientOriginalName();
			$userImagePath = 'images/userPhotos/'.$userPhotoName;
			$file->move('images/userPhotos/', $userPhotoName);

            // save image from the form into database
			$newUser->image_name = $userPhotoName;
			$newUser->image_path = $userImagePath;
		}else{
			// save no-user-image if no image is selected
			$newUser->image_name = 'no-user-image';
			$newUser->image_path = 'images/no-user-image.png';
		}

		if (!empty($_POST)){
			if($password !== $repeat_password){
				$error = "Password and Repeat password fields must match!";
			}else{
                // flash messages
				flash()->success('Success!', 'Thanks for registering on our site');
				$newUser->password = bcrypt(Input::get('password'));
				$newUser->save();

                // assign to the new user ROLE USER
				DB::table('user_role')->insert(['user_id' => $newUser->id, 'role_id' => '3']);
			}
			if(isset($error)){
				$oldName = Input::get('name');
				$oldEmail = Input::get('email');
				return view('adminLte', compact('user', 'oldName', 'oldEmail', 'toggleStatus'))->withErrors($error);
			}else{
				return redirect('/admin/users/profiles')->with(compact('user', 'toggleStatus'));
			}
		}

		return view('adminLte', compact('user', 'toggleStatus'));
	}

	public function editAdminUser($id)
	{
		$user = User::find($id);
		$flyer = Flyer::get();
		$toggleStatus = $_COOKIE['adminToggle'];

		$userAccount = DB::table('users')
		->leftJoin('delivery_addresses', 'users.id', 'delivery_addresses.user_id')
		->leftJoin('credit_cards', 'users.id', 'credit_cards.user_id')
		->where('users.id', $id)->first();

		// dd($userAccount);

		return view('adminLte', compact('user', 'userAccount', 'flyer','toggleStatus'));
	}

	public function userProfile(Request $request)
	{
		$users = User::all();
		$flyer = Flyer::get();
		$user = User::user();
		$toggleStatus = $_COOKIE['adminToggle'];

		return view('adminLte', compact('users', 'flyer', 'user', 'toggleStatus'));
	}

	public function updateUserProfiles($id)
	{
		$user = User::find($id);

		if (!empty($_POST)){
			$user->name = Input::get('name');
			$user->email = Input::get('email');
			if(Input::get('password') == Input::get('repeat-password')){
				$user->password = bcrypt(Input::get('password'));
				$user->save();
				return redirect('/admin/users/profiles');
			}else{
				$my_errors = 'Password and Repeat password must match!';
				return redirect('/admin/users/profiles')->withErrors($my_errors);
			}
		}else{
			return redirect('/admin/users/profiles');
		}
		
	}

	public function editUserAccount($id)
	{

		$user = DB::table('users')
		->leftJoin('credit_cards', 'users.id', 'credit_cards.user_id')
		->leftJoin('delivery_addresses', 'users.id', 'delivery_addresses.user_id')
		->select('users.id', 'users.name', 'users.email', 'users.password', 'users.image_path', 
			'credit_cards.card_number_1','credit_cards.card_number_2','credit_cards.card_number_3','credit_cards.card_number_4','credit_cards.card_holder','credit_cards.card_expiration_month','credit_cards.card_expiration_year','credit_cards.card_ccv',
			'delivery_addresses.country', 'delivery_addresses.state', 'delivery_addresses.address', 'delivery_addresses.zip', 'delivery_addresses.phone')
		->where('users.id', $id)->first();

		// dd($user);

		return view('auth.editAccount', compact('user'));
	}

	public function updateUserAccount($id)
	{
		$user = User::find($id);

		if (!empty($_POST)){
			$user->name = Input::get('name');
			$user->email = Input::get('email');

			// delivery address
			if(count(DB::table('delivery_addresses')->where('user_id', $user->id)->get()) > 0){
				DB::table('delivery_addresses')->where('user_id', $user->id)->update([
					'country' => Input::get('country'),
					'state' => Input::get('state'), 
					'address' => Input::get('address'), 
					'zip' => Input::get('zip'),
					'phone' => Input::get('phone')
					]);
			}else{
				if((Input::get('country') != '') && (Input::get('state') != '') && (Input::get('address') != '') && (Input::get('zip') != '') && (Input::get('phone') != '')){
					$deliveryAddress = new DeliveryAddress();
					$deliveryAddress->user_id = $user->id;
					$deliveryAddress->country = Input::get('country');
					$deliveryAddress->state = Input::get('state');
					$deliveryAddress->address = Input::get('address');
					$deliveryAddress->zip = Input::get('zip');
					$deliveryAddress->phone = Input::get('phone');
					$deliveryAddress->save();
				}
			}

			// credit card address
			if(count(DB::table('credit_cards')->where('user_id', $user->id)->get()) > 0){
				DB::table('credit_cards')->where('user_id', $user->id)->update([
					'card_number_1' => Input::get('cardNumber1'),
					'card_number_2' => Input::get('cardNumber2'), 
					'card_number_3' => Input::get('cardNumber3'), 
					'card_number_4' => Input::get('cardNumber4'),
					'card_holder' => Input::get('cardHolder'), 
					'card_expiration_month' => Input::get('cardExpirationMonth'), 
					'card_expiration_year' => Input::get('cardExpirationYear'), 
					'card_ccv' => Input::get('ccv')
					]);
			}else{
				$creditCard = new CreditCard();
				$creditCard->user_id = $user->id;
				$creditCard->card_number_1 = Input::get('cardNumber1');
				$creditCard->card_number_2 = Input::get('cardNumber2');
				$creditCard->card_number_3 = Input::get('cardNumber3');
				$creditCard->card_number_4 = Input::get('cardNumber4');
				$creditCard->card_holder = Input::get('cardHolder');
				$creditCard->card_expiration_month = Input::get('cardExpirationMonth');
				$creditCard->card_expiration_year = Input::get('cardExpirationYear');
				$creditCard->card_ccv = Input::get('ccv');
				$creditCard->save();
			}

			if(Input::get('password') == Input::get('password-confirm')){
				$user->password = bcrypt(Input::get('password'));

                // get file from form
				$file =  Input::file('editUserImageFrontPage');
				if($file){
					$userPhotoName = time() . '-' .$file->getClientOriginalName();
					$userImagePath = 'images/userPhotos/'.$userPhotoName;
					$file->move('images/userPhotos/', $userPhotoName);

					$user->image_name = $userPhotoName;
					$user->image_path = $userImagePath;
					$user->save();

					flash()->success('Success!', "User updated successfully");
					return redirect()->back();
				}else{
					$user->save();

					flash()->success('Success!', "User updated successfully");
					return redirect()->back();
				}
				
			}else{
				$my_errors = 'Password and Repeat password must match!';
				return redirect()->back()->withErrors($my_errors);
			}
		}
	}

	public function updateAdminUserAcount($id)
	{
		$user = User::find($id);
		$toggleStatus = $_COOKIE['adminToggle'];

		// user account informations
		$user->name = Input::get('name');
		$user->email = Input::get('email');
		$user->password = bcrypt(Input::get('password'));

        // get file from the form
		$file = Input::file('editUserImage');
		if($file) {
			$fileName = $file->getClientOriginalName();
			$userPhotoName = time() . '-' . $fileName;
			$userImagePath = 'images/userPhotos/' . $userPhotoName;
			$file->move('images/userPhotos/', $userPhotoName);

            // save image from the form into database
			$user->image_name = $userPhotoName;
			$user->image_path = $userImagePath;

			$user->save();
		}

        // get the user images fro mdatabase
		$userImageFromDatabase = DB::table('users')->pluck('image_name');
		foreach ($userImageFromDatabase as $databaseImageName) {
			$userImageName[] = $databaseImageName;
		}

        // get the files from the userPhotos Dir
		$filesFromUserDir = scandir('images/userPhotos');
		$filesFromUserDir = array_diff($filesFromUserDir, [".", ".."]);

        // delete the images that are not in the database
		foreach ($filesFromUserDir as $imageFromDir) {
			if (!in_array($imageFromDir, $userImageName)) {
				unlink('images/userPhotos/' . $imageFromDir);
			}
		}
		// flash messages
		flash()->success('Success!', 'Your account information where succesfully updated!');

		return redirect()->route('admin.user.profiles')->with(compact('user', 'toggleStatus'));
	}

	public function updateAdminUserDelivery($id){
		// get the curent user delivery address from database
		if(count(DB::table('delivery_addresses')->where('user_id', $id)->first()) > 0 ){
			// user delivery address informations
			DB::table('delivery_addresses')->where('user_id', $id)->update(['country' => Input::get('country'), 'state' => Input::get('state'), 'address' => Input::get('address'), 'zip' => Input::get('zip'), 'phone' => Input::get('phone')]);	
		}else{
			if((Input::get('country') != '') && (Input::get('state') != '') && (Input::get('address') != '') && (Input::get('zip') != '') && (Input::get('phone') != '')){
				$user_delivery = new DeliveryAddress();

				// user delivery address informations
				$user_delivery->user_id = $id;
				$user_delivery->country = Input::get('country');
				$user_delivery->state = Input::get('state');
				$user_delivery->address = Input::get('address');
				$user_delivery->zip = Input::get('zip');
				$user_delivery->phone = Input::get('phone');

				$user_delivery->save();
			}
		}
		// flash messages
		flash()->success('Success!', 'Your delivery address information where succesfully updated!');

		return redirect()->back();
	}

	public function updateAdminUserCreditCard($id){
		// get the curent user delivery address from database
		if(count(DB::table('credit_cards')->where('user_id', $id)->first()) > 0 ){
			// user delivery address informations
			DB::table('credit_cards')->where('user_id', $id)->update(['card_number_1' => Input::get('card_number_1'), 'card_number_2' => Input::get('card_number_2'), 'card_number_3' => Input::get('card_number_3'), 'card_number_4' => Input::get('card_number_4'), 'card_holder' => Input::get('card_holder'), 'card_expiration_month' => Input::get('card_expiration_month'), 'card_expiration_year' => Input::get('card_expiration_year'), 'card_ccv' => Input::get('card_ccv')]);	
		}else{
			$user_credit_card = new CreditCard();

			// user delivery address informations
			$user_credit_card->user_id = $id;
			$user_credit_card->card_number_1 = Input::get('card_number_1');
			$user_credit_card->card_number_2 = Input::get('card_number_2');
			$user_credit_card->card_number_3 = Input::get('card_number_3');
			$user_credit_card->card_number_4 = Input::get('card_number_4');
			$user_credit_card->card_holder = Input::get('card_holder');
			$user_credit_card->card_expiration_month = Input::get('card_expiration_month');
			$user_credit_card->card_expiration_year = Input::get('card_expiration_year');
			$user_credit_card->card_ccv = Input::get('card_ccv');

			$user_credit_card->save();
		}
		// flash messages
		flash()->success('Success!', 'Your delivery address information where succesfully updated!');

		return redirect()->back();
	}

	public function destroyAdminUser($id)
	{
		$user = User::find($id);

        // delete the user role assign to the current user
		DB::table('user_role')->where('user_id', '=', $id)->delete();

		$user->delete();

		return redirect()->back();
	}
}
