<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReviewReply extends Model
{
    public function reply_review()
    {
        return $this->belongsTo('App\Reviews');
    }
}
