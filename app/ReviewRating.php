<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReviewRating extends Model
{
    public function rating_reviews()
    {
        return $this->belongsTo('App\Flyer');
    }
}
