<?php

namespace App;

use App\Favorite;
use App\Flyer;
use App\Http\Controllers\FlyersController;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class FavoriteFlyersClass extends FlyersController
{

    public function showFavoriteFlyers()
    {
        if (!isset($_COOKIE['gridList'])) {
            setcookie('gridList', 'list', time() + 9999999, '/');
        }
        $cookievalue = $_COOKIE['gridList'];

        if (\Auth::check()) {
            $user = User::user();

            $favoriteFlyers     = Favorite::where('user_id', '=', $user->id)->pluck('flyer_id');
            $allfavoritesFlyers = Flyer::with('photos')->whereIn('id', $favoriteFlyers)->orderBy('created_at', 'desc')->paginate(8);
            $pagination         = Flyer::paginate(8);

            return view('pages.favorite', compact('allfavoritesFlyers', 'pagination', 'cookievalue', 'favoriteFlyers'));
        } else {
            $favoriteFlyers = array_unique(json_decode($_COOKIE['favorite'], true));
            // dd(count($favoriteFlyers));
            if ($favoriteFlyers !== null) {
                $allfavoritesFlyers = Flyer::with('photos')->whereIn('id', $favoriteFlyers)->orderBy('created_at', 'desc')->paginate(8);
                $pagination         = Flyer::paginate(8);
            } else {
                $allfavoritesFlyers = [];
            }

            return view('pages.favorite', compact('allfavoritesFlyers', 'pagination', 'cookievalue', 'favoriteFlyers'));
        }
    }

    public function addFlyerToFavorite(Request $request)
    {
        $flyerId = $request->input('flyerId');
        $user    = User::user();

        // add to favorite just if user is logged in and the flyers is not already favorite for the curent user
        if (\Auth::check()) {
            $favoriteFlyers = Favorite::where('user_id', '=', $user->id)->where('flyer_id', '=', $flyerId)->pluck('flyer_id');

            if (count($favoriteFlyers) == 0) {
                $favorite           = new Favorite();
                $favorite->user_id  = $user->id;
                $favorite->flyer_id = (int) $flyerId;
                $favorite->save();

                return response()->json(['message' => 'Added to favorite!'], 200);
            }
        } else {
            if (!isset($_COOKIE['favorite'])) {
                setcookie('favorite', json_encode([$flyerId]), time() + 9999999, '/');
            }
            $notUserFavoriteFlyers = json_decode($_COOKIE['favorite'], true);

            if ($notUserFavoriteFlyers !== null) {
                if (!in_array($flyerId, $notUserFavoriteFlyers, true)) {
                    array_push($notUserFavoriteFlyers, $flyerId);
                }
            } else {
                $notUserFavoriteFlyers = [];
            }

            setcookie('favorite', json_encode($notUserFavoriteFlyers), time() + 9999999, '/');

            return response()->json(['message' => 'Added to favorite for no user logedin!'], 200);
        }
    }

    public function removeFlyerFromFavorite(Request $request)
    {
        $user    = User::user();
        $flyerId = $request->input('flyerId');

        if (\Auth::check()) {
            $flyer = Favorite::where('user_id', '=', $user->id)->where('flyer_id', '=', (int) $flyerId);
            $flyer->delete();

            return response()->json(['Removed from favorite!']);
        } else {
            $notUserFavoriteFlyers = json_decode($_COOKIE['favorite'], true);

            if (($key = array_search($flyerId, $notUserFavoriteFlyers)) !== false) {
                unset($notUserFavoriteFlyers[$key]);
            }

            setcookie('favorite', json_encode($notUserFavoriteFlyers), time() + 9999999, '/');

            return response()->json(['message' => 'Removed to favorite for no user logedin!'], 200);
        }
    }

    public function addFlyerToFavoriteFromAllFleyersPage(Request $request)
    {
        $flyerId = $request->input('flyerId');
        $user    = User::user();

        // add to favorite just if user is logged in and the flyers is not already favorite for the curent user
        if (\Auth::check()) {
            $favoriteFlyers = Favorite::where('user_id', '=', $user->id)->where('flyer_id', '=', $flyerId)->pluck('flyer_id');

            if (count($favoriteFlyers) == 0) {
                $favorite           = new Favorite();
                $favorite->user_id  = $user->id;
                $favorite->flyer_id = (int) $flyerId;
                $favorite->save();

                return response()->json(['message' => 'Added to favorite!'], 200);
            }
        } else {
            if (!isset($_COOKIE['favorite'])) {
                setcookie('favorite', json_encode([$flyerId]), time() + 9999999, '/');
            }
            $notUserFavoriteFlyers = json_decode($_COOKIE['favorite'], true);

            if (!in_array($flyerId, $notUserFavoriteFlyers, true)) {
                array_push($notUserFavoriteFlyers, $flyerId);
            }

            setcookie('favorite', json_encode($notUserFavoriteFlyers), time() + 9999999, '/');

            return response()->json(['message' => 'Added to favorite for no user logged in in all flyers page!'], 200);
        }
    }

    public function removeFlyerFromFavoriteFromAllFleyersPage(Request $request)
    {
        $user    = User::user();
        $flyerId = $request->input('flyerId');

        if (\Auth::check()) {
            $flyer = Favorite::where('user_id', '=', $user->id)->where('flyer_id', '=', (int) $flyerId);
            $flyer->delete();

            return response()->json(['Removed from favorite!']);
        } else {
            $notUserFavoriteFlyers = json_decode($_COOKIE['favorite'], true);

            if (($key = array_search($flyerId, $notUserFavoriteFlyers)) !== false) {
                unset($notUserFavoriteFlyers[$key]);
            }

            setcookie('favorite', json_encode($notUserFavoriteFlyers), time() + 9999999, '/');

            return response()->json(['message' => 'Removed to favorite for no user logedin in all flyers page!'], 200);
        }
    }
}
