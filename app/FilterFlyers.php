<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Http\Controllers\FlyersController;
use Illuminate\Http\Request;

class FilterFlyers extends FlyersController
{
	public function ajaxCallGetFilterSelectedValue(Request $request)
	{
		$selectedValue = $request->filterCurrentValue;
        // $flyersFormTheDom = $request->htmlCode;

		$user = User::user();

		if (!isset($_COOKIE['gridList'])) {
			setcookie('gridList', 'list', time() + 9999999, '/');
		}
		$cookievalue = $_COOKIE['gridList'];

        // check if curent loged user has the flyer as favorite
		if (!isset($_COOKIE['favorite'])) {
			setcookie('favorite', json_encode([]), time() + 9999999, '/');
		}
		if (\Auth::check()) {
			$favoriteFlyers = Favorite::where('user_id', '=', $user->id)->pluck('flyer_id');
		} else {
			$favoriteFlyers = json_decode($_COOKIE['favorite'], true);
		}

        // return $selectedFilter;
		if (($selectedValue == "Cele mai recente")) {
			$allFlyers = Flyer::with('photos')->orderBy('created_at', 'desc')->paginate(12);
			$pagination = Flyer::paginate(12);

			return view('filterPages.filterFlyersLayout', compact('allFlyers', 'pagination', 'cookievalue', 'favoriteFlyers'));
		} 
		if ($selectedValue == "De la cele mai ieftine") {
			$allFlyers = Flyer::with('photos')->orderBy('price', 'asc')->paginate(12);
			$pagination = Flyer::paginate(12);

			return view('filterPages.filterFlyersLayout', compact('allFlyers', 'pagination', 'cookievalue', 'favoriteFlyers'));
		} 
		if ($selectedValue == "De la cele mai scumpe") {
			$allFlyers = Flyer::with('photos')->orderBy('price', 'desc')->paginate(12);
			$pagination = Flyer::paginate(12);

			return view('filterPages.filterFlyersLayout', compact('allFlyers', 'pagination', 'cookievalue', 'favoriteFlyers'));
		} 
		if ($selectedValue == "De la cei mai multi KM") {
			$allFlyers = Flyer::with('photos')->orderBy('created_at', 'asc')->paginate(12);
			$pagination = Flyer::paginate(12);

			return view('filterPages.filterFlyersLayout', compact('allFlyers', 'pagination', 'cookievalue', 'favoriteFlyers'));
		} 
		if ($selectedValue == "De la cei mai putini KM") {
			$allFlyers = Flyer::with('photos')->orderBy('created_at', 'asc')->paginate(12);
			$pagination = Flyer::paginate(12);

			return view('filterPages.filterFlyersLayout', compact('allFlyers', 'pagination', 'cookievalue', 'favoriteFlyers'));
		}
		if($selectedValue == ''){
			$allFlyers = Flyer::with('photos')->orderBy('created_at', 'desc')->paginate(12);
			$pagination = Flyer::paginate(12);

			return view('flyers.showAll', compact('allFlyers', 'pagination', 'cookievalue', 'favoriteFlyers'));
		}
	}

	public function ajaxCallGetSearchFilterSelectedValue(Request $request){
		$user = User::user();

		$searchSelectedValue = $request->searchFilterCurrentValue;

		if (!isset($_COOKIE['gridList'])) {
			setcookie('gridList', 'list', time() + 9999999, '/');
		}
		$cookievalue = $_COOKIE['gridList'];

        // check if curent loged user has the flyer as favorite
		if (!isset($_COOKIE['favorite'])) {
			setcookie('favorite', json_encode([]), time() + 9999999, '/');
		}
		if (\Auth::check()) {
			$favoriteFlyers = Favorite::where('user_id', '=', $user->id)->pluck('flyer_id');
		} else {
			$favoriteFlyers = json_decode($_COOKIE['favorite'], true);
		}

        // return $selectedFilter;
		if ($searchSelectedValue == "Cele mai recente") {
			$searchFlyersResult = $this->searchFilterQuery($request, 'created_at', 'desc');
			$pagination = Flyer::paginate(12);

			return view('filterPages.searchFilterFlyersLayout', compact('searchFlyersResult', 'pagination', 'cookievalue', 'favoriteFlyers'));
		} 
		if ($searchSelectedValue == "De la cele mai ieftine") {
			$searchFlyersResult = $this->searchFilterQuery($request, 'price', 'asc');
			$pagination = Flyer::paginate(12);

			return view('filterPages.searchFilterFlyersLayout', compact('searchFlyersResult', 'pagination', 'cookievalue', 'favoriteFlyers'));
		} 
		if ($searchSelectedValue == "De la cele mai scumpe") {
			$searchFlyersResult = $this->searchFilterQuery($request, 'price', 'desc');
			$pagination = Flyer::paginate(12);

			return view('filterPages.searchFilterFlyersLayout', compact('searchFlyersResult', 'pagination', 'cookievalue', 'favoriteFlyers'));
		} 
		if ($searchSelectedValue == "De la cei mai multi KM") {
			$searchFlyersResult = Flyer::with('photos')->orderBy('created_at', 'asc')->paginate(12);
			$pagination = $this->paginationOnPage($searchFlyersResult);

			return view('filterPages.searchFilterFlyersLayout', compact('searchFlyersResult', 'pagination', 'cookievalue', 'favoriteFlyers'));
		} 
		if ($searchSelectedValue == "De la cei mai putini KM") {
			$searchFlyersResult = Flyer::with('photos')->orderBy('created_at', 'asc')->paginate(12);
			$pagination = $this->paginationOnPage($searchFlyersResult);

			return view('filterPages.searchFilterFlyersLayout', compact('searchFlyersResult', 'pagination', 'cookievalue', 'favoriteFlyers'));
		}
		if($searchSelectedValue == ''){
			$searchFlyersResult = Flyer::with('photos')->orderBy('created_at', 'desc')->paginate(12);
			$pagination = Flyer::paginate(12);

			return view('pages.search', compact('searchFlyersResult', 'pagination', 'cookievalue', 'favoriteFlyers'));
		}
	}

	public function searchFilterQuery(Request $request, $orderCondition, $orderDirection){
		$searchPriceFromField = $request->searchFilterPriceFrom;
		$searchPriceToField = $request->searchFilterPriceTo;

        // conditions for price from
		if ($searchPriceFromField == '> 20000') {
			$searchPriceFromField = 999999999999999999999;
		}
		if (($searchPriceFromField == '< 2000') || ($searchPriceFromField == null)) {
			$searchPriceFromField = 0;
		}

        // conditions for price to
		if (($searchPriceToField == null) || ($searchPriceToField == '> 20000')) {
			$searchPriceToField = 999999999999999999999;
		}

        // condition to change positions if price-from is higher than price to
		if ($searchPriceFromField > $searchPriceToField) {
			$test = $searchPriceFromField;
			$searchPriceFromField = $searchPriceToField;
			$searchPriceToField = $test;

			return redirect()->route('search.flyers', ['price-from' => $searchPriceFromField, 'price-to' => $searchPriceToField]);
		}

		$searchFlyersResult = Flyer::where(array(
			['price', '>', "$searchPriceFromField"],
			['price', '<', "$searchPriceToField"]
			))->orderBy($orderCondition, $orderDirection)->paginate(12);

		return $searchFlyersResult;
	}

	public function paginationOnPage($searchFlyersResult){
		if (count($searchFlyersResult) < 6) {
			$pagination = Flyer::paginate(0);
		} else {
			$pagination = Flyer::paginate(12);
		}

		return $pagination;
	}  
}