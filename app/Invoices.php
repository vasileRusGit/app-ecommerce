<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoices extends Model
{
    public function invoice()
    {
        return $this->belongsTo('App\User');
    }

    public function invoice_products(){
    	return $this->hasMany('\App\InvoiceProducts');
    }
}
