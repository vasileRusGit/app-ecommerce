<?php

namespace App;

use App\Http\Requests\FlyerRequest;
use Illuminate\Http\Request;
use App\Flyer;
use App\Photo;
use App\User;
use App\Favorite;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\FlyersController;

class RatingReviewsClass extends FlyersController
{
    public function addRatingToFlyer(Request $request)
    {
        $ratingNumber = $request->input('ratingNumber');
        $zip = $request->input('zip');
        $street = $request->input('street');
        $flyer = Flyer::locatedAt($zip, $street)->firstOrFail();
        $user = User::user();

        $ratingFlyers = new Rating();

        if (\Auth::check()) {

            $ratingsToFlyers = DB::table('ratings')->where('flyer_id', $flyer->id)->pluck('user_id');
            $userThatRateTheFlyer = [];
            foreach ($ratingsToFlyers as $userRatings) {
                $userThatRateTheFlyer[] = $userRatings;
            }

            if (in_array($user->id, $userThatRateTheFlyer)) {
                DB::table('ratings')->where('user_id', $user->id)->where('flyer_id', $flyer->id)->update(['rating' => $ratingNumber]);
            } else {
                $ratingFlyers->user_id = $user->id;
                $ratingFlyers->flyer_id = $flyer->id;
                $ratingFlyers->rating = $ratingNumber;
                $ratingFlyers->save();
            }
        }
    }

    public function addReviewToFlyerModal(Request $request)
    {
        $review = $request->input('reviewTextarea');
        $user = User::user();
        $flyerId = $request->input('flyerIdShowPage');

        $reviewClass = new Review();

        if (\Auth::check()) {
            if ($review == '') {
                flash()->info('Attention!', "The body field is required!");
                return redirect()->back();
            } else {
                $reviewClass->user_id = $user->id;
                $reviewClass->flyer_id = $flyerId;
                $reviewClass->review = $review;
                $reviewClass->save();
                return redirect()->back();
            }
        }
    }

    public function editRatingToFlyerModal(Request $request){
        $reviewRatingNumber = $request->input('reviewRatingNumber');
        $user = User::user();
        $flyerId = $request->input('editModalFlyerId');
        $reviewIdEditModal = $request->input('reviewIdEditModal');

        DB::table('review_ratings')->where('review_id', $reviewIdEditModal)->where('flyer_id', $flyerId)->where('user_id', $user->id)
            ->update(['rating_review' => $reviewRatingNumber]);
    }

    public function replyReview(Request $request)
    {
        $reviewId = $request->input('replyReviewId');
        $user = User::user();
        $replyReviewtext = $request->input('replyReviewTextarea');
        $flyerId = DB::table('reviews')->where('id', $reviewId)->pluck('flyer_id');

        $replyReview = new ReviewReply();
        $replyReview->user_id = $user->id;
        $replyReview->review_id = $reviewId;
        $replyReview->flyer_id = $flyerId[0];
        $replyReview->reply_review = $replyReviewtext;
        $replyReview->save();
        return redirect()->back();
    }

    public function editReplyReview(Request $request)
    {
        $replyReviewId = $request->input('editReplyReviewId');
        $replyUserId = $request->input('editReplyUserId');
        $replyUpdatedAt = $request->input('editReplyUpdatedAt');
        $editReplyReviewText = $request->input('editReplyReviewTextarea');

        DB::table('review_replies')->where('user_id', $replyUserId)->where('id', $replyReviewId)->update(['reply_review' => $editReplyReviewText]);
        return redirect()->back();
    }

    public function editReviewToFlyerModal(Request $request)
    {
        $editReviewText = $request->input('editReviewTextarea');
        $editReviewId = $request->input('editReviewId');
        $user = User::user();
        $flyerId = $request->input('flyerIdShowPageForEditReview');

        if (\Auth::check()) {
            if ($editReviewText == '') {
                flash()->info('Attention!', "The body field is required!");
                return redirect()->back();
            } else {
                DB::table('reviews')->where('flyer_id', $flyerId)->where('user_id', $user->id)->where('id', $editReviewId)->update(['review' => $editReviewText]);
                return redirect()->back();
            }
        }
    }


    public function addRatingToFlyerModal(Request $request)
    {
        $reviewsId = DB::table('reviews')->pluck('id');

        $getProperArray = end($reviewsId);
        $reviewId = (int)end($getProperArray) + 1;

        $ratingNumber = $request->input('reviewRatingNumber');
        if ($ratingNumber == '') {
            $ratingNumber = 0;
        }

        $zip = $request->input('zip');
        $street = $request->input('street');
        $flyer = Flyer::locatedAt($zip, $street)->firstOrFail();
        $user = User::user();

        $ratingReviews = new ReviewRating();
        $ratingReviews->user_id = $user->id;
        $ratingReviews->flyer_id = $flyer->id;
        $ratingReviews->review_id = $reviewId;
        $ratingReviews->rating_review = $ratingNumber;
        $ratingReviews->save();
    }


}