<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryAddress extends Model
{
    public function delivery_address()
    {
        return $this->belongsTo('App\User');
    }
}
